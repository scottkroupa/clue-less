package CluelessPackage;

/*
 * The CaseFile class extends CardHolder (b/c it can be an owner of cards).
 * It contains the weapon, player, and room that = the solution to the crime.
 * 
 *  @author: Christine Herlihy
 *	last updated: 11/12/16
 *
 *	Notes: 
 */

import java.util.ArrayList;


public class CaseFile extends CardHolder{
	

	private ArrayList<Card> crime = new ArrayList<Card>();
	
	
	/**
	 * @param crime
	 */
	public CaseFile(ArrayList<Card> crime) {
		super();
		this.crime = crime;
	}

	/**
	 * @return the crime
	 */
	public ArrayList<Card> getCrime() {
		return crime;
	}

	/**
	 * @param crime the crime to set
	 */
	public void setCrime(ArrayList<Card> crime) {
		this.crime = crime;
	}
	
	
	
	/**
	 * @method solutionMatchesAccusation checks to see whether a player's accusation matches the contents of the CaseFile
	 * @param String acWeapon: accusation weapon
	 * @param String acPlayer: accusation player
	 * @param String acRoom: accusation room
	 * @return boolean (matches = T; no match = F)
	 */
	public boolean solutionMatchesAccusation(String acWeapon, String acPlayer, String acRoom){
		
		ArrayList<Card> solution = this.crime;
		boolean weaponMatch = false;
		boolean playerMatch = false;
		boolean roomMatch = false;
		boolean result = false;
		
		for(int i = 0; i <solution.size(); i++){
			
			if(solution.get(i).getCardType().equalsIgnoreCase("weapon") & solution.get(i).getCardValue().equalsIgnoreCase(acWeapon)){
				weaponMatch = true;
			}
			
			else if(solution.get(i).getCardType().equalsIgnoreCase("player") & solution.get(i).getCardValue().equalsIgnoreCase(acPlayer)){
				playerMatch = true;
			}
			
			else if(solution.get(i).getCardType().equalsIgnoreCase("room") & solution.get(i).getCardValue().equalsIgnoreCase(acRoom)){
				roomMatch = true;
			}
		}
		
		// Check to make sure ALL fields match
		if(weaponMatch & playerMatch & roomMatch){
			result = true;
		}
		
		
		// Return result; default is false
		return result;

	}	// close solutionMatchesAccusation method 
	


}
