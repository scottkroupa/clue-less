/*
 *  The GameMenuPanel class 
 *	@author: Ryan Bonisch
 *	last updated: 12/01/16
 *
 *	
 */

/**
 * Package CluelessPackage
 */
package CluelessPackage;

/**
 * import all necessary libraries
 */

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.Rectangle;
import javax.swing.text.DefaultCaret;
import static javax.swing.text.DefaultCaret.ALWAYS_UPDATE;


public class GameMenuPanel extends JPanel {

    //Declare global variables
    private int gameMenuPanelID;
    private String prompt;
    private String username;
    private JTextArea jtaChatWindow;
    private JTextArea jtaMessageWindow;
    private final SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    private ArrayList<Card> hand = new ArrayList();
    private Socket socket;
    private ArrayList<String> playerOrder = new ArrayList();
    private ArrayList<String> charOrder = new ArrayList();
    private final ImageIcon yellowPiece = new ImageIcon(getClass().getResource("/YellowPiece.png"));
    private final ImageIcon redPiece = new ImageIcon(getClass().getResource("/RedPiece.png"));
    private final ImageIcon purplePiece = new ImageIcon(getClass().getResource("/PurplePiece.png"));
    private final ImageIcon greenPiece = new ImageIcon(getClass().getResource("/GreenPiece.png"));
    private final ImageIcon whitePiece = new ImageIcon(getClass().getResource("/WhitePiece.png"));
    private final ImageIcon bluePiece = new ImageIcon(getClass().getResource("/BluePiece.png"));
    private final Image mustard = yellowPiece.getImage();
    private final Image scarlet = redPiece.getImage();
    private final Image plum = purplePiece.getImage();
    private final Image green = greenPiece.getImage();
    private final Image white = whitePiece.getImage();
    private final Image peacock = bluePiece.getImage();
    private ArrayList<ImageIcon> cardIcons = new ArrayList();
    private ArrayList<GUIHand> guiHandList = new ArrayList();
    private ArrayList<GUIRoom> guiRoomList = new ArrayList();
    private ArrayList<GUIHall> guiHallList = new ArrayList();
    private final int playerNum;
    private boolean myTurn;
    private boolean myMove;
    private boolean mySuggest;
    private boolean myAccuse;
    private boolean myEndTurn;
    private NetworkListener nl;
    private DataInputStream from;
    private final String charName;
    private ArrayList<GUIHall> startingLocs = new ArrayList();
    private final String[] suspectList = {"Col. Mustard", "Miss Scarlet", "Professor Plum", "Mr. Green", "Mrs. White", "Mrs. Peacock"};
    private ArrayList<ImageIcon> pieceIcons = new ArrayList();
    private JButton suggestAccuseButton;
    private JButton endTurn;
    private JButton endTurn2;
    private JButton timer; 
    private JRadioButton jrbSuggest;
    private JRadioButton jrbAccuse;
    private String[] murder = {"", "", ""};
    private JComboBox jcbWeapons;
    private JComboBox jcbSuspects;
    private JComboBox jcbRooms;
    private String containerName;
    private JTabbedPane jtpTabbedPane;
    private JButton pickCardButton;
    private String sendCardTo;
    private JRadioButton jrbClear;
    private JLabel suspectImage;
    private JLabel roomImage;
    private JLabel weaponImage;
    private JButton confirmMove;
    private GUIOrder guiOrder;
    private Frame f = null;
    private int minute;
    private Timer t;
    private int timePassed  =0;
    private ClientApplication ca = null;
    //private String template;
    
    
    /**
     * @param charName
     * @method GameMenuPanel constructor
     * @param frame
     * @param username
     * @param socket
     * @param playerNum
     */
    public GameMenuPanel(Frame frame, String username, Socket socket, int playerNum, String charName, String template) {
        gameMenuPanelID = 0; // Intended Purpose?
        this.socket = socket;
        this.playerNum = playerNum;
        myTurn = false;
        myMove = false;
        mySuggest = false;
        myAccuse = false;
        myEndTurn = false;
        this.username = username;
        this.charName = charName;
        yellowPiece.setDescription("Col. Mustard");
        redPiece.setDescription("Miss Scarlet");
        purplePiece.setDescription("Professor Plum");
        greenPiece.setDescription("Mr. Green");
        whitePiece.setDescription("Mrs. White");
        bluePiece.setDescription("Mrs. Peacock");
        pieceIcons.add(yellowPiece);
        pieceIcons.add(redPiece);
        pieceIcons.add(purplePiece);
        pieceIcons.add(greenPiece);
        pieceIcons.add(whitePiece);
        pieceIcons.add(bluePiece);
        ImageIcon mustardIcon = new ImageIcon(getClass().getResource("/ColMustard.png"));
        ImageIcon scarletIcon = new ImageIcon(getClass().getResource("/MissScarlet.png"));
        ImageIcon plumIcon = new ImageIcon(getClass().getResource("/ProfPlum.png"));
        ImageIcon greenIcon = new ImageIcon(getClass().getResource("/MrGreen.png"));
        ImageIcon whiteIcon = new ImageIcon(getClass().getResource("/MrsWhite.png"));
        ImageIcon peacockIcon = new ImageIcon(getClass().getResource("/MrsPeacock.png"));
        ImageIcon knifeIcon = new ImageIcon(getClass().getResource("/Knife.png"));
        ImageIcon wrenchIcon = new ImageIcon(getClass().getResource("/Wrench.png"));
        ImageIcon ropeIcon = new ImageIcon(getClass().getResource("/Rope.png"));
        ImageIcon revolverIcon = new ImageIcon(getClass().getResource("/Revolver.png"));
        ImageIcon pipeIcon = new ImageIcon(getClass().getResource("/LeadPipe.png"));
        ImageIcon candlestickIcon = new ImageIcon(getClass().getResource("/Candlestick.png"));
        ImageIcon studyIcon = new ImageIcon(getClass().getResource("/Study.png"));
        ImageIcon hallIcon = new ImageIcon(getClass().getResource("/Hall.png"));
        ImageIcon loungeIcon = new ImageIcon(getClass().getResource("/Lounge.png"));
        ImageIcon libraryIcon = new ImageIcon(getClass().getResource("/Library.png"));
        ImageIcon billiardIcon = new ImageIcon(getClass().getResource("/BilliardRoom.png"));
        ImageIcon diningIcon = new ImageIcon(getClass().getResource("/DiningRoom.png"));
        ImageIcon conservatoryIcon = new ImageIcon(getClass().getResource("/Conservatory.png"));
        ImageIcon ballroomIcon = new ImageIcon(getClass().getResource("/Ballroom.png"));
        ImageIcon kitchenIcon = new ImageIcon(getClass().getResource("/Kitchen.png"));
        mustardIcon.setDescription("Col. Mustard");
        scarletIcon.setDescription("Miss Scarlet");
        plumIcon.setDescription("Professor Plum");
        greenIcon.setDescription("Mr. Green");
        whiteIcon.setDescription("Mrs. White");
        peacockIcon.setDescription("Mrs. Peacock");
        knifeIcon.setDescription("Knife");
        wrenchIcon.setDescription("Wrench");
        ropeIcon.setDescription("Rope");
        revolverIcon.setDescription("Revolver");
        pipeIcon.setDescription("Lead Pipe");
        candlestickIcon.setDescription("Candlestick");
        studyIcon.setDescription("Study");
        hallIcon.setDescription("Hall");
        loungeIcon.setDescription("Lounge");
        libraryIcon.setDescription("Library");
        billiardIcon.setDescription("Billiard Room");
        diningIcon.setDescription("Dining Room");
        conservatoryIcon.setDescription("Conservatory");
        ballroomIcon.setDescription("Ballroom");
        kitchenIcon.setDescription("Kitchen");
        cardIcons.add(mustardIcon); // 0
        cardIcons.add(scarletIcon);
        cardIcons.add(plumIcon);
        cardIcons.add(greenIcon);
        cardIcons.add(whiteIcon);
        cardIcons.add(peacockIcon);
        cardIcons.add(knifeIcon);   // 6
        cardIcons.add(wrenchIcon);
        cardIcons.add(ropeIcon);
        cardIcons.add(revolverIcon);
        cardIcons.add(pipeIcon);
        cardIcons.add(candlestickIcon);
        cardIcons.add(studyIcon);   // 12
        cardIcons.add(hallIcon);
        cardIcons.add(loungeIcon);
        cardIcons.add(libraryIcon);
        cardIcons.add(billiardIcon);
        cardIcons.add(diningIcon);
        cardIcons.add(conservatoryIcon);
        cardIcons.add(ballroomIcon);
        cardIcons.add(kitchenIcon); // 20
        init();
	    buildGUI(frame, template);
        nl = new NetworkListener(this, from);
        nl.start();
    }
    
    /**
     * @method
     */
    public final void init(){
        try {
            from = new DataInputStream(socket.getInputStream());
            int num = from.readInt();
            for (int i = 0; i < num; i++) {
                ObjectInputStream object = new ObjectInputStream(socket.getInputStream());
                Card card = (Card)object.readObject();
                hand.add(card);
                System.out.println(hand);
                System.out.println(hand.size());
            }
            from = new DataInputStream(socket.getInputStream());
            num = from.readInt();
            for (int i = 0; i < num; i++) {
                String playerName = from.readUTF();
                String charNames = from.readUTF();
                playerOrder.add(playerName);
                charOrder.add(charNames);
                System.out.println(playerName);
                System.out.println(charNames);
                System.out.println(playerOrder);
                System.out.println(playerOrder.size());
                System.out.println(charOrder);
                System.out.println(charOrder.size());
            }
            from = new DataInputStream(socket.getInputStream());
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(GameMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * @method This is the GUI that the player will interface with and be loaded into the client application frame.
     * @param frame
     */
    public final void buildGUI(Frame frame, String template) {
        
        //
        prompt = "[" + username + "]: ";
        double componentWidth = frame.getWidth() - 70;
        double componentHeight = frame.getHeight() / 20;

        //
        JPanel mainPanel = new JPanel(new GridBagLayout());
        GridBagConstraints mainConstraint = new GridBagConstraints();
        LineBorder lineBorder = new LineBorder(Color.BLACK, 1);

        //
        GridLayout gl = new GridLayout(5, 5, 0, 0);
        JPanel mapPanel = new JPanel(gl);
        if(template == "Mysterious") {
            mainPanel.setBackground((Color.PINK));
            mapPanel.setBackground(Color.PINK);
        }
        Dimension tabbedPaneSize = 
            new Dimension(
                    (int)(componentWidth * 1.05), (int)(componentHeight * 7));
        Dimension roomsHalls = new Dimension(1,1);
        FourPoint roomFP = new FourPoint(0,0,(int)(tabbedPaneSize.getWidth()/5)-2,63);
        FourPoint vertHallFP = new FourPoint(58,-1,20,65);
        FourPoint horzHallFP = new FourPoint(-1,20,(int)(tabbedPaneSize.getWidth()/5),20);
        
        confirmMove = new JButton("Confirm Move");
        confirmMove.setEnabled(false);
        confirmMove.setPreferredSize(new Dimension(100,50));
        confirmMove.addActionListener((ActionEvent ae) -> {
            int r = 0;
            int c = 0;
            for (GUIRoom room : guiRoomList) {
                if(room.isSelected()) {
                    r = room.getRow();
                    c = room.getCol();
                    //mySuggest = true;
                }
                room.setSelected(false);
                room.setPossibleLocation(false);
                room.setMyTurn(false);
            }
            for (GUIHall hallway : guiHallList) {
                if(hallway.isSelected()) {
                    r = hallway.getRow();
                    c = hallway.getCol();
                    mySuggest = false;
                    jrbSuggest.setEnabled(false);
                }
                hallway.setSelected(false);
                hallway.setPossibleLocation(false);
                hallway.setMyTurn(false);
            }
            try {
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
                toServer.writeUTF("[0][" + username + "][move][" + charName + "][" + r + "][" + c + "][0]");
            } catch (IOException ex){
                Logger.getLogger(GameMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
            confirmMove.setEnabled(false);
            myMove = false;
        });
        
        GUIRoom study = new GUIRoom("Study", roomFP, 3, 0, 0);
        guiRoomList.add(study);
        study.setPreferredSize(roomsHalls);
        study.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && study.isPossibleLocation()){
                    if(study.isSelected()) {
                        study.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        study.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Study!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Study!");
            }
        });
        
        GUIHall studyLibraryHall = new GUIHall("Hall 3", vertHallFP, 1, 0);
        guiHallList.add(studyLibraryHall);
        studyLibraryHall.setPreferredSize(roomsHalls);
        studyLibraryHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && studyLibraryHall.isPossibleLocation()){
                    if(studyLibraryHall.isSelected()) {
                        studyLibraryHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        studyLibraryHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Study-Library Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Study-Library Hallway!");
            }
        });
        studyLibraryHall.setCharacter1Image(plum);
        
        GUIRoom library = new GUIRoom("Library", roomFP, 0, 2, 0);
        guiRoomList.add(library);
        library.setPreferredSize(roomsHalls);
        library.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && library.isPossibleLocation()){
                    if(library.isSelected()) {
                        library.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        library.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Library!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Library!");
            }
        });
        
        GUIHall libraryConservatoryHall = new GUIHall("Hall 8", vertHallFP, 3, 0);
        guiHallList.add(libraryConservatoryHall);
        libraryConservatoryHall.setPreferredSize(roomsHalls);
        libraryConservatoryHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && libraryConservatoryHall.isPossibleLocation()){
                    if(libraryConservatoryHall.isSelected()) {
                        libraryConservatoryHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        libraryConservatoryHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Library-Conservatory Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Library-Conservatory Hallway!");
            }
        });
        libraryConservatoryHall.setCharacter1Image(peacock);
        
        GUIRoom conservatory = new GUIRoom("Conservatory", roomFP, 2, 4, 0);
        guiRoomList.add(conservatory);
        conservatory.setPreferredSize(roomsHalls);
        conservatory.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && conservatory.isPossibleLocation()){
                    if(conservatory.isSelected()) {
                        conservatory.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        conservatory.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Conservatory!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Conservatory!");
            }
        });
        
        GUIHall studyHallHall = new GUIHall("Hall 1", horzHallFP, 0, 1);
        guiHallList.add(studyHallHall);
        studyHallHall.setPreferredSize(roomsHalls);
        studyHallHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && studyHallHall.isPossibleLocation()){
                    if(studyHallHall.isSelected()) {
                        studyHallHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        studyHallHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Study-Hall Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Study-Hall Hallway!");
            }
        });
        
        JLabel emptySpace1 = new JLabel("");
        emptySpace1.setPreferredSize(roomsHalls);
        
        GUIHall libraryBilliardHall = new GUIHall("Hall 6", horzHallFP, 2, 1);
        guiHallList.add(libraryBilliardHall);
        libraryBilliardHall.setPreferredSize(roomsHalls);
        libraryBilliardHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && libraryBilliardHall.isPossibleLocation()){
                    if(libraryBilliardHall.isSelected()) {
                        libraryBilliardHall.setSelected(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        libraryBilliardHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Library-Billiard Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Library-Billiard Hallway!");
            }
        });
        
        JLabel emptySpace2 = new JLabel("");
        emptySpace2.setPreferredSize(roomsHalls);
        
        GUIHall conservatoryBallroomHall = new GUIHall("Hall 11", horzHallFP, 4, 1);
        guiHallList.add(conservatoryBallroomHall);
        conservatoryBallroomHall.setPreferredSize(roomsHalls);
        conservatoryBallroomHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && conservatoryBallroomHall.isPossibleLocation()){
                    if(conservatoryBallroomHall.isSelected()) {
                        conservatoryBallroomHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        conservatoryBallroomHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Conservatory-Ballroom Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Conservatory-Ballroom Hallway!");
            }
        });
        conservatoryBallroomHall.setCharacter1Image(green);
        
        GUIRoom hall = new GUIRoom("Hall", roomFP, 0, 0, 2);
        guiRoomList.add(hall);
        hall.setPreferredSize(roomsHalls);
        hall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && hall.isPossibleLocation()){
                    if(hall.isSelected()) {
                        hall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        hall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Hall!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Hall!");
            }
        });
        
        GUIHall hallBilliardHall = new GUIHall("Hall 4", vertHallFP, 1, 2);
        guiHallList.add(hallBilliardHall);
        hallBilliardHall.setPreferredSize(roomsHalls);
        hallBilliardHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && hallBilliardHall.isPossibleLocation()){
                    if(hallBilliardHall.isSelected()) {
                        hallBilliardHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        hallBilliardHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Hall-Billiard Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Hall-Billiard Hallway!");
            }
        });
        
        GUIRoom billiard = new GUIRoom("Billiard Room", roomFP, 0, 2, 2);
        guiRoomList.add(billiard);
        billiard.setPreferredSize(roomsHalls);
        billiard.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && billiard.isPossibleLocation()){
                    if(billiard.isSelected()) {
                        billiard.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        billiard.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Billiard!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Billiard!");
            }
        });
        
        GUIHall billiardBallroomHall = new GUIHall("Hall 9", vertHallFP, 3, 2);
        guiHallList.add(billiardBallroomHall);
        billiardBallroomHall.setPreferredSize(roomsHalls);
        billiardBallroomHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && billiardBallroomHall.isPossibleLocation()){
                    if(billiardBallroomHall.isSelected()) {
                        billiardBallroomHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        billiardBallroomHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Billiard-Ballroom Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Billiard-Ballroom Hallway!");
            }
        });
        
        GUIRoom ballroom = new GUIRoom("Ballroom", roomFP, 0, 4, 2);
        guiRoomList.add(ballroom);
        ballroom.setPreferredSize(roomsHalls);
        ballroom.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && ballroom.isPossibleLocation()){
                    if(ballroom.isSelected()) {
                        ballroom.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        ballroom.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Ballroom!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Ballroom!");
            }
        });
        
        GUIHall hallLoungeHall = new GUIHall("Hall 2", horzHallFP, 0, 3);
        guiHallList.add(hallLoungeHall);
        hallLoungeHall.setPreferredSize(roomsHalls);
        hallLoungeHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && hallLoungeHall.isPossibleLocation()){
                    if(hallLoungeHall.isSelected()) {
                        hallLoungeHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        hallLoungeHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Hall-Lounge Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Hall-Lounge Hallway!");
            }
        });
        hallLoungeHall.setCharacter1Image(scarlet);
        
        JLabel emptySpace3 = new JLabel();
        emptySpace3.setPreferredSize(roomsHalls);
        
        GUIHall billiardDiningHall = new GUIHall("Hall 7", horzHallFP, 2, 3);
        guiHallList.add(billiardDiningHall);
        billiardDiningHall.setPreferredSize(roomsHalls);
        billiardDiningHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && billiardDiningHall.isPossibleLocation()){
                    if(billiardDiningHall.isSelected()) {
                        billiardDiningHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        billiardDiningHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Billiard-Dining Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Billiard-Dining Hallway!");
            }
        });
        
        GUIHall ballroomKitchenHall = new GUIHall("Hall 12", horzHallFP, 4, 3);
        guiHallList.add(ballroomKitchenHall);
        ballroomKitchenHall.setPreferredSize(roomsHalls);
        ballroomKitchenHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && ballroomKitchenHall.isPossibleLocation()){
                    if(ballroomKitchenHall.isSelected()) {
                        ballroomKitchenHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        ballroomKitchenHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Ballroom-Kitchen Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Ballroom-Kitchen Hallway!");
            }
        });
        ballroomKitchenHall.setCharacter1Image(white);
        
        GUIRoom lounge = new GUIRoom("Lounge", roomFP, 4, 0, 4);
        guiRoomList.add(lounge);
        lounge.setPreferredSize(roomsHalls);
        lounge.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && lounge.isPossibleLocation()){
                    if(lounge.isSelected()) {
                        lounge.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        lounge.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Lounge!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Lounge!");
            }
        });
        
        GUIHall loungeDiningHall = new GUIHall("Hall 5", vertHallFP, 1, 4);
        guiHallList.add(loungeDiningHall);
        loungeDiningHall.setPreferredSize(roomsHalls);
        loungeDiningHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && loungeDiningHall.isPossibleLocation()){
                    if(loungeDiningHall.isSelected()) {
                        loungeDiningHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        loungeDiningHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Lounge-Dining Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Lounge-Dining Hallway!");
            }
        });
        loungeDiningHall.setCharacter1Image(mustard);
        
        GUIRoom dining = new GUIRoom("Dining Room", roomFP, 0, 2, 4);
        guiRoomList.add(dining);
        dining.setPreferredSize(roomsHalls);
        dining.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && dining.isPossibleLocation()){
                    if(dining.isSelected()) {
                        dining.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        dining.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Dining Room!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Dining Room!");
            }
        });
        
        GUIHall diningKitchenHall = new GUIHall("Hall 10", vertHallFP, 3, 4);
        guiHallList.add(diningKitchenHall);
        diningKitchenHall.setPreferredSize(roomsHalls);
        diningKitchenHall.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && diningKitchenHall.isPossibleLocation()){
                    if(diningKitchenHall.isSelected()) {
                        diningKitchenHall.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        diningKitchenHall.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Dining-Kitchen Hallway!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Dining-Kitchen Hallway!");
            }
        });
        
        GUIRoom kitchen = new GUIRoom("Kitchen", roomFP, 1, 4, 4);
        guiRoomList.add(kitchen);
        kitchen.setPreferredSize(roomsHalls);
        kitchen.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(myTurn && kitchen.isPossibleLocation()){
                    if(kitchen.isSelected()) {
                        kitchen.setSelected(false);
                        confirmMove.setEnabled(false);
                    } else {
                        for(GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                        }
                        for(GUIHall hall : guiHallList) {
                            hall.setSelected(false);
                        }
                        kitchen.setSelected(true);
                        confirmMove.setEnabled(true);
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //System.out.println("ENTERED Kitchen!");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //System.out.println("EXITED Kitchen!");
            }
        });
        
        endTurn = new JButton("End Turn");
        endTurn.setSize(100,100);
        endTurn.setEnabled(false);
        endTurn.addActionListener((ActionEvent e) -> {
            for (GUIRoom room : guiRoomList) {
                room.setSelected(false);
                room.setPossibleLocation(false);
                room.setMyTurn(false);
            }
            for (GUIHall hallway : guiHallList) {
                hallway.setSelected(false);
                hallway.setPossibleLocation(false);
                hallway.setMyTurn(false);
            }
            jtpTabbedPane.setSelectedIndex(0);
            jcbSuspects.setSelectedIndex(-1);
            jcbWeapons.setSelectedIndex(-1);
            jcbRooms.setSelectedIndex(-1);
            endTurn.setEnabled(false);
            endTurn2.setEnabled(false);
            jrbClear.setSelected(true);
            jrbSuggest.setEnabled(false);
            jrbAccuse.setEnabled(false);
            suggestAccuseButton.setEnabled(false);
            myTurn = false;
            myMove = false;
            mySuggest = false;
            myAccuse = false;
            myEndTurn = false;
            if(f != null) {
                f.dispose();
                f = null;
            }
            try {
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
                toServer.writeUTF("[0]["+username+"][end][turn][0][0][0]");
            } catch (IOException ex) {
                Logger.getLogger(GameMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        // CH edits 12/10: Adding timer button
        timer = new JButton("Game starting!");
        timer.setFont(new Font("Arial", Font.PLAIN, 10));
        timer.setMargin(new Insets(0,0,0,0));       
        timer.setSize(100,100);
        timer.setEnabled(false);  
        
        t = new Timer(1000, new ActionListener(){
        	
            @Override
            public void actionPerformed(ActionEvent e){
            	timePassed += 1000;         	
            	long minutes = TimeUnit.MILLISECONDS.toMinutes(timePassed) % 60;
                long seconds = TimeUnit.MILLISECONDS.toSeconds(timePassed) % 60;
            	
            	String output = String.format("%d min, %d sec",
            	minutes, seconds);
          
               // timer.setText("<html>Time Elapsed: <br />" + minutes + " minutes, " + seconds + " seconds</html>");
                timer.setText("<html>Time Elapsed: <br />" + output + "</html>");
            }
        });
        t.start();


        mapPanel.add(study);
        mapPanel.add(studyHallHall);
        mapPanel.add(hall);
        mapPanel.add(hallLoungeHall);
        mapPanel.add(lounge);
        mapPanel.add(studyLibraryHall);
        mapPanel.add(emptySpace1);	
        mapPanel.add(hallBilliardHall);
        mapPanel.add(endTurn);
        mapPanel.add(loungeDiningHall);
        mapPanel.add(library);
        mapPanel.add(libraryBilliardHall);
        mapPanel.add(billiard);
        mapPanel.add(billiardDiningHall);
        mapPanel.add(dining);
        mapPanel.add(libraryConservatoryHall);
        mapPanel.add(timer);//CH edits 12/10 to add timer
        mapPanel.add(billiardBallroomHall);
        mapPanel.add(confirmMove);
        mapPanel.add(diningKitchenHall);
        mapPanel.add(conservatory);
        mapPanel.add(conservatoryBallroomHall);
        mapPanel.add(ballroom);
        mapPanel.add(ballroomKitchenHall);
        mapPanel.add(kitchen);
        startingLocs.add(loungeDiningHall);
        startingLocs.add(hallLoungeHall);
        startingLocs.add(studyLibraryHall);
        startingLocs.add(conservatoryBallroomHall);
        startingLocs.add(ballroomKitchenHall);
        startingLocs.add(libraryConservatoryHall);
        
        String[] suspects = {"Col. Mustard", "Miss Scarlet", "Professor Plum", "Mr. Green", "Mrs. White", "Mrs. Peacock"};
        String[] rooms = {"Study", "Hall", "Lounge", "Library", "Billiard Room", "Dining Room", "Conservatory", "Ballroom", "Kitchen"};
        String[] weapons = {"Knife", "Wrench", "Rope", "Revolver", "Candlestick", "Lead Pipe"};
        
        //
        JPanel suggestAccusePanel = new JPanel(new BorderLayout());
        suggestAccuseButton = new JButton("Accuse");
        suggestAccuseButton.setSize(100,100);
        suggestAccuseButton.setEnabled(false);
        
        endTurn2 = new JButton("End Turn");
        endTurn2.setSize(100,100);
        endTurn2.setEnabled(false);
        
        jrbClear = new JRadioButton("Clear");
        jrbClear.setEnabled(true);
        
        jrbSuggest = new JRadioButton("Suggest");
        jrbSuggest.setEnabled(false);
        jrbSuggest.addActionListener((ActionEvent e) -> {
            int index = 0;
            for (int i = 0; i < rooms.length; i++) {
                if(rooms[i].matches(containerName)) {
                    index = i;
                }
            }
            jcbRooms.setSelectedIndex(index);
            jcbRooms.setEnabled(false);
            suggestAccuseButton.setText("Suggest");
        });
        
        jrbAccuse = new JRadioButton("Accuse");
        jrbAccuse.addActionListener((ActionEvent e) -> {
            jcbRooms.setSelectedIndex(-1);
            jcbRooms.setEnabled(true);
            suggestAccuseButton.setText("Accuse");
        });
        ButtonGroup group = new ButtonGroup();
        
        suggestAccuseButton.addActionListener((ActionEvent e) -> {
            if(myTurn) {
                if (jrbSuggest.isSelected()) {
                    jrbAccuse.setEnabled(false);
                    endTurn.setEnabled(false);
                    endTurn2.setEnabled(false);
                    jrbClear.setSelected(true);
                    suggestAccuseButton.setText("");
                    jcbSuspects.setSelectedIndex(-1);
                    suspectImage.setIcon(null);
                    jcbWeapons.setSelectedIndex(-1);
                    weaponImage.setIcon(null);
                    jcbRooms.setSelectedIndex(-1);
                    roomImage.setIcon(null);
                    
                    if(!myMove) {
                        for (GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                            room.setPossibleLocation(false);
                            room.setMyTurn(false);
                        }
                        for (GUIHall hallway : guiHallList) {
                            hallway.setSelected(false);
                            hallway.setPossibleLocation(false);
                            hallway.setMyTurn(false);
                        }
                    } else {
                        for (GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                            room.setMyTurn(false);
                        }
                        for (GUIHall hallway : guiHallList) {
                            hallway.setSelected(false);
                            hallway.setMyTurn(false);
                        }
                        confirmMove.setEnabled(false);
                    }
                    
                    suggest(murder[0], murder[2], murder[1]);
                } else if (jrbAccuse.isSelected()) {
                    jrbClear.setSelected(true);
                    suggestAccuseButton.setEnabled(false);
                    myMove = false;
                    mySuggest = false;
                    myEndTurn = false;
                    for (GUIRoom room : guiRoomList) {
                        room.setSelected(false);
                        room.setPossibleLocation(false);
                        room.setMyTurn(false);
                    }
                    for (GUIHall hallway : guiHallList) {
                        hallway.setSelected(false);
                        hallway.setPossibleLocation(false);
                        hallway.setMyTurn(false);
                    }
                    myTurn = false;
                    confirmMove.setEnabled(false);
                    jrbSuggest.setEnabled(false);
                    jrbAccuse.setEnabled(false);
                    endTurn.setEnabled(false);
                    endTurn2.setEnabled(false);
                    accuse(murder[0], murder[2], murder[1]);
                }
            }
        });
        endTurn2.addActionListener((ActionEvent e) -> {
            for (GUIRoom room : guiRoomList) {
                room.setSelected(false);
                room.setPossibleLocation(false);
                room.setMyTurn(false);
            }
            for (GUIHall hallway : guiHallList) {
                hallway.setSelected(false);
                hallway.setPossibleLocation(false);
                hallway.setMyTurn(false);
            }
            jtpTabbedPane.setSelectedIndex(0);
            endTurn2.setEnabled(false);
            endTurn.setEnabled(false);
            suggestAccuseButton.setEnabled(false);
            jrbClear.setSelected(true);
            jrbSuggest.setEnabled(false);
            jrbAccuse.setEnabled(false);
            myTurn = false;
            myMove = false;
            mySuggest = false;
            myAccuse = false;
            myEndTurn = false;
            if(f != null) {
                f.dispose();
                f = null;
            }
            try {
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
                toServer.writeUTF("[0]["+username+"][end][turn][0][0][0]");
            } catch (IOException ex) {
                Logger.getLogger(GameMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        group.add(jrbSuggest);
        group.add(jrbAccuse);
        group.add(jrbClear);
        JPanel topPanel = new JPanel(new BorderLayout());
        JPanel radioButtonsPanel = new JPanel(new GridLayout(1,3,0,0));
        radioButtonsPanel.add(jrbSuggest);
        radioButtonsPanel.add(jrbAccuse);
        radioButtonsPanel.add(suggestAccuseButton);
        topPanel.add(radioButtonsPanel, BorderLayout.WEST);
        topPanel.add(endTurn2, BorderLayout.EAST);
        
        suspectImage = new JLabel();
        roomImage = new JLabel();
        weaponImage = new JLabel();
        
        JPanel murderTriadPanel = new JPanel(new GridLayout(1,3,0,0));
        JPanel suspectsPanel = new JPanel(new BorderLayout());
        jcbSuspects = new JComboBox(suspects);
        jcbSuspects.setSelectedIndex(-1);
        jcbSuspects.setEditable(false);
        jcbSuspects.addItemListener((ItemEvent ie) -> {
            if (ie.getStateChange() == ItemEvent.SELECTED) {
                String selected = Arrays.toString(ie.getItemSelectable().getSelectedObjects());
                if (selected.contains("Col. Mustard")) {
                    suspectImage.setIcon(cardIcons.get(0));
                } else if (selected.contains("Miss Scarlet")) {
                    suspectImage.setIcon(cardIcons.get(1));
                } else if (selected.contains("Professor Plum")) {
                    suspectImage.setIcon(cardIcons.get(2));
                } else if (selected.contains("Mr. Green")) {
                    suspectImage.setIcon(cardIcons.get(3));
                } else if (selected.contains("Mrs. White")) {
                    suspectImage.setIcon(cardIcons.get(4));
                } else if (selected.contains("Mrs. Peacock")) {
                    suspectImage.setIcon(cardIcons.get(5));
                }
                murder[0] = selected.substring(1, selected.length()-1);
                suggestAccuseButton.setEnabled(canSuggestOrAccuse());
            }
        });
        suspectsPanel.add(jcbSuspects, BorderLayout.NORTH);
        suspectsPanel.add(suspectImage, BorderLayout.CENTER);
        JPanel roomsPanel = new JPanel(new BorderLayout());
        jcbRooms = new JComboBox(rooms);
        jcbRooms.setSelectedIndex(-1);
        jcbRooms.setEditable(false);
        jcbRooms.addItemListener((ItemEvent ie) -> {
            if(ie.getStateChange() == ItemEvent.SELECTED) {
                String selected = Arrays.toString(ie.getItemSelectable().getSelectedObjects());
                if(selected.contains("Study")) {
                    roomImage.setIcon(cardIcons.get(12));
                } else if(selected.contains("Hall")) {
                    roomImage.setIcon(cardIcons.get(13));
                } else if(selected.contains("Lounge")) {
                    roomImage.setIcon(cardIcons.get(14));
                } else if(selected.contains("Library")) {
                    roomImage.setIcon(cardIcons.get(15));
                } else if(selected.contains("Billiard")) {
                    roomImage.setIcon(cardIcons.get(16));
                } else if(selected.contains("Dining")) {
                    roomImage.setIcon(cardIcons.get(17));
                } else if(selected.contains("Conservatory")) {
                    roomImage.setIcon(cardIcons.get(18));
                } else if(selected.contains("Ballroom")) {
                    roomImage.setIcon(cardIcons.get(19));
                } else if(selected.contains("Kitchen")) {
                    roomImage.setIcon(cardIcons.get(20));
                }
                murder[1] = selected.substring(1, selected.length()-1);
                suggestAccuseButton.setEnabled(canSuggestOrAccuse());
            }
        });
        roomsPanel.add(jcbRooms, BorderLayout.NORTH);
        roomsPanel.add(roomImage, BorderLayout.CENTER);
        JPanel weaponsPanel = new JPanel(new BorderLayout());
        jcbWeapons = new JComboBox(weapons);
        jcbWeapons.setSelectedIndex(-1);
        jcbWeapons.setEditable(false);
        jcbWeapons.addItemListener((ItemEvent ie) -> {
            if(ie.getStateChange() == ItemEvent.SELECTED) {
                String selected = Arrays.toString(ie.getItemSelectable().getSelectedObjects());
                if(selected.contains("Knife")) {
                    weaponImage.setIcon(cardIcons.get(6));
                } else if (selected.contains("Wrench")) {
                    weaponImage.setIcon(cardIcons.get(7));
                } else if (selected.contains("Rope")) {
                    weaponImage.setIcon(cardIcons.get(8));
                } else if (selected.contains("Revolver")) {
                    weaponImage.setIcon(cardIcons.get(9));
                } else if (selected.contains("Lead Pipe")) {
                    weaponImage.setIcon(cardIcons.get(10));
                } else if (selected.contains("Candlestick")) {
                    weaponImage.setIcon(cardIcons.get(11));
                }
                murder[2] = selected.substring(1, selected.length()-1);
                suggestAccuseButton.setEnabled(canSuggestOrAccuse());
            }
        });
        weaponsPanel.add(jcbWeapons, BorderLayout.NORTH);
        weaponsPanel.add(weaponImage, BorderLayout.CENTER);
        murderTriadPanel.add(suspectsPanel);
        murderTriadPanel.add(roomsPanel);
        murderTriadPanel.add(weaponsPanel);
        
        suggestAccusePanel.add(topPanel, BorderLayout.NORTH);
        suggestAccusePanel.add(murderTriadPanel, BorderLayout.CENTER);
        
        
        // Hand Panel
        GridLayout handGL = new GridLayout(1,hand.size()+1,0,0);
        JPanel myHandPanel = new JPanel(handGL);
        JScrollPane jspHandScrollPane = new JScrollPane(myHandPanel);
        Dimension handPSize = new Dimension(160, 300);
        for(Card card : hand) {
            for (ImageIcon icon : cardIcons) {
                if (card.getCardValue().matches(icon.getDescription())){
                    GUIHand guiHand = new GUIHand(icon, card);
                    guiHand.setPreferredSize(handPSize);
                    guiHandList.add(guiHand);
                    guiHand.addMouseListener(new MouseListener() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                        }

                        @Override
                        public void mousePressed(MouseEvent e) {
                        }

                        @Override
                        public void mouseReleased(MouseEvent e) {
                            if(guiHand.getSelectable()){
                                for (GUIHand guiHand1 : guiHandList) {
                                    guiHand1.setSelected(false);
                                }
                                guiHand.fill();
                                pickCardButton.setEnabled(true);
                            }
                        }

                        @Override
                        public void mouseEntered(MouseEvent e) {
                        }

                        @Override
                        public void mouseExited(MouseEvent e) {
                        }
                    });
                    myHandPanel.add(guiHand);
                }
            }
        }
        pickCardButton = new JButton("pickCard");
        pickCardButton.setEnabled(false);
        pickCardButton.addActionListener((ActionEvent e) -> {
            pickCardButton.setEnabled(false);
            for (GUIHand guiHand1 : guiHandList) {
                if(guiHand1.getSelected()){
                    pickCard(guiHand1.getCard().getCardType(), guiHand1.getCard().getCardValue());
                    guiHand1.setSelected(false);
                }
            }
            for (GUIHand guiHand : guiHandList) {
                guiHand.setSelectable(false);
                guiHand.setSelected(false);
            }
            jtpTabbedPane.setSelectedIndex(0);
        });
        myHandPanel.add(pickCardButton);
        
        //
        guiOrder = new GUIOrder(playerOrder, charOrder);
        
        
        //
        JPanel notebookPanel = new JPanel(new BorderLayout());
        Notebook notebook = new Notebook();
        // CH added 12/10
        GUINotebook guiNotebook = new GUINotebook(notebook, notebookPanel, this.guiHandList);
        
        JPanel rules = new JPanel(new BorderLayout());
        String message = 
            "Game Rules\n\n" +
            "*General*\n\n" + 
            "-The Map tab will show you the current location of each player.  It is also used to move around Mr. Boddy's mansion.\n" +
            "-The Notebook tab will allow you to check off cards as they are shown to you, or if you know another player has a card in their hand from others' suggestions.\n" +
            "-The Suggest/Accuse tab is where you will perform suggestions and Accusations.  See *Suggestion* and *Accusation* for more details\n" +
            "-The Hand tab lists all of your cards in your hand.  It also has a \"Pick Card\" button which is used when you have a card as part of a suggestion. See *Suggestion*\n" +
            "-The Player Order tab lists all players in the game and their respective turn order.\n" +
            "-The Game Rules = this.gameRules;\n\n" +
            "*Movement*\n\n" +
            "-You will notice, throughout the game, some rooms/hallways will have a red square and X on them.  These may not be selected for movement to.\n" +
            "-You may only move to areas that have a green border around them.  To select the possible move location, left click inside of the green square.\n" +
            "-Then left-click on the Confirm Move button located on the Map itself.  This will signify that this is where you would like to go.\n\n" +
            "*Suggestion*\n\n" +
            "-When you enter a room, you may not end your turn until you make a suggestion.\n" +
            "-Simply switch to the Suggest/Accuse tab, select the Suggest radio button and then choose the suspect and weapon that you believe maybe the crime.\n" +
            "-When ready, press the suggest button located to the right of the Accuse radio button.\n" +
            "-The game will look through the next player(s) to see if they have a card in their hand as part of your suggestion.\n" +
            "-If they do, they will show it to you so you may mark it down in your notebook.  The Notebook tab will automatically be selected when a card is returned to you.\n" +
            "-If no player has a card you suggested, then these cards are most likely a part of the Case File.\n" +
            "-NOTE: You may not perform any other actions while another player is picking the card to show you.\n" +
            "-If you are the player that is chosen to show a card, your game will immediately switch to your Hand tab where you must select a card highlighted in Cyan.\n" +
            "-Once you do this, the \"Pick Card\" button will be enabled. When you are satisfied with the card you have chosen to show, simply press that button and you will be returned to the Map tab.\n\n" +
            "*Accusation*\n\n" +
            "-An Accusation may be made at anytime, DURING YOUR TURN, whether you are in a room or hallway.\n" +
            "-Making an accusation is almost the same as a suggestion in terms of selecting cards however you are not bound by the room you are in.\n" +
            "-The accusation is the final portion of the game.  You assert that you know where the murder was committed, which what weapon and by whom.\n" +
            "-If your accusation proves incorrect, you must remain in the game only to show other players a card in your hand as part of suggestions.\n" +
            "-If your accusation proves correct, you win the game and increase in Detective Level.  Perhaps one day rivaling The Greatest Detective of all time!";
        Font font = new Font("Arial", Font.PLAIN, 9);
        JTextArea jtaMessage = new JTextArea();
        JScrollPane jspMessage = new JScrollPane(jtaMessage);
        Dimension rulesPaneSize = 
            new Dimension(
                    (int)(componentWidth * 1.05), (int)(componentHeight * 10.9));
        jspMessage.setPreferredSize(rulesPaneSize);
        jspMessage.setVerticalScrollBarPolicy(
            ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jtaMessage.setWrapStyleWord(true);
        jtaMessage.setLineWrap(true);
        jtaMessage.setEditable(false);
        jtaMessage.setFont(font);
        jtaMessage.setText(message);
        rules.add(jspMessage, BorderLayout.NORTH);
        
        
        //
        jtpTabbedPane = new JTabbedPane();
        jtpTabbedPane.addTab("Map", mapPanel);
        jtpTabbedPane.addTab("Notebook", notebookPanel);
        jtpTabbedPane.addTab("Suggest/Accuse", suggestAccusePanel);
        jtpTabbedPane.addTab("Hand", jspHandScrollPane);
        jtpTabbedPane.addTab("Player Order", guiOrder);
        jtpTabbedPane.addTab("Game Rules", rules);
        jtpTabbedPane.setPreferredSize(tabbedPaneSize);
        mainConstraint.anchor = GridBagConstraints.PAGE_START;
        mainConstraint.gridy = 0;
        mainConstraint.gridx = 0;
        mainConstraint.ipady = 140;
        mainPanel.add(jtpTabbedPane, mainConstraint);
        

        
        // Chat panel area
        JPanel chatPanel = new JPanel();
        chatPanel.setBorder(lineBorder);
        Dimension chatWindowPSize =
            new Dimension(
                    (int)(componentWidth * 0.51), (int)(componentHeight * 4.8));
        jtaChatWindow = new JTextArea();
        jtaChatWindow.setEditable(false);
        jtaChatWindow.setLineWrap(true);
        jtaChatWindow.setWrapStyleWord(true);
        JScrollPane jspChatWindow = new JScrollPane(jtaChatWindow);
        jspChatWindow.setVerticalScrollBarPolicy(
            ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jspChatWindow.setHorizontalScrollBarPolicy(
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jspChatWindow.setPreferredSize(chatWindowPSize);
        mainConstraint.anchor = GridBagConstraints.WEST;
        mainConstraint.gridy = 1;
        mainConstraint.gridx = 0;
        mainConstraint.ipady = 0;
        mainConstraint.insets = new Insets(5,0,0,0);
        chatPanel.add(jspChatWindow, BorderLayout.CENTER);
        mainPanel.add(chatPanel, mainConstraint);

        // Message panel area
        JPanel messagePanel = new JPanel();
        messagePanel.setBorder(lineBorder);
        Dimension messageWindowPSize =
            new Dimension(
                    (int)(componentWidth * 0.51), (int)(componentHeight * 4.8));
        jtaMessageWindow = new JTextArea();
        jtaMessageWindow.setCaretPosition(jtaMessageWindow.getDocument().getLength());
        jtaMessageWindow.setEditable(false);
        jtaMessageWindow.setLineWrap(true);
        jtaMessageWindow.setWrapStyleWord(true);
        DefaultCaret caret = (DefaultCaret) jtaMessageWindow.getCaret();
        caret.setUpdatePolicy(ALWAYS_UPDATE);
        JScrollPane jspMessageWindow = new JScrollPane(jtaMessageWindow);
        jspMessageWindow.setVerticalScrollBarPolicy(
            ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jspMessageWindow.setHorizontalScrollBarPolicy(
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jspMessageWindow.setPreferredSize(messageWindowPSize);
        mainConstraint.anchor = GridBagConstraints.EAST;
        mainConstraint.gridy = 1;
        mainConstraint.ipady = 0;
        messagePanel.add(jspMessageWindow, BorderLayout.CENTER);
        mainPanel.add(messagePanel, mainConstraint);

        // Text entry area
        JPanel textEntryPanel = new JPanel();
        textEntryPanel.setBorder(lineBorder);
        mainConstraint.anchor = GridBagConstraints.PAGE_END;
        mainConstraint.gridy = 2;
        JTextField jtfChatEntry = new JTextField();
        jtfChatEntry.setForeground(Color.lightGray);
        jtfChatEntry.setText(prompt);
        Dimension chatEntryPSize =
            new Dimension(
                    (int)(componentWidth * 1.05), (int)(componentHeight * 0.9));
        jtfChatEntry.setPreferredSize(chatEntryPSize);
        textEntryPanel.add(jtfChatEntry, BorderLayout.WEST);
        mainPanel.add(textEntryPanel, mainConstraint);
        jtfChatEntry.addKeyListener(new KeyListenerImpl(jtfChatEntry));
        jtfChatEntry.addFocusListener(new FocusListenerImpl(jtfChatEntry));
        
        this.add(mainPanel);
    }
    
    /**
     * @method The appendChat method is used to add chat to the chat window
     * @param message 
     */
    public void appendChat(String message) {
        this.jtaChatWindow.append(message + "\n");
    }
    
    /**
     * @method The appendSystemMessage method is used to add system messages to
     *  the message window
     * @param message 
     */
    public void appendSystemMessage(String message) {
        this.jtaMessageWindow.append(message + "\n");
    }
    
    /**
     * @method The stringParser method is used to receive in String responses 
     *  from the network listener and then parse it.  It will then call the 
     *  appropriate method from the parsed string.
     * @param response 
     * @throws IOException
     */
    public void stringParser(String response) throws IOException {
        DataOutputStream toClient;
        int[] index = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        String dest, src, cmd, field, info1, info2, info3;
        int countLeft = 0;
        int countRight = 0;
        for (int i = 0; i < response.length(); i++) {
            if (response.charAt(i) == '['){
                countLeft++;
            }
            if(response.charAt(i) == ']'){
                countRight++;
            }
        }
        
        index[0] = response.indexOf("[") + 1;
        index[1] = response.indexOf("]");
        index[2] = response.indexOf("[", index[1]) + 1;
        index[3] = response.indexOf("]", index[2]);
        index[4] = response.indexOf("[", index[3]) + 1;
        index[5] = response.indexOf("]", index[4]);
        index[6] = response.indexOf("[", index[5]) + 1;
        index[7] = response.indexOf("]", index[6]);
        index[8] = response.indexOf("[", index[7]) + 1;
        if(countLeft > 7 && countRight > 7) {
            System.out.println("countLeft and countRight are greater than 7");
            int colon1 = response.indexOf(":", index[8]);
            int colon2 = response.indexOf(":", colon1 + 1);
            System.out.println("position of : " + colon2);
            index[9] = response.indexOf("]", colon2);
            System.out.println("index[9] is " + index[9]);
            System.out.println(response);
        } else if (countLeft == 7 && countRight == 7) {
            index[9] = response.indexOf("]", index[8]);
        }
        index[10] = response.indexOf("[", index[9]) + 1;
        index[11] = response.indexOf("]", index[10]);
        index[12] = response.indexOf("[", index[11]) + 1;
        index[13] = response.lastIndexOf("]");
        
        
        dest = response.substring(index[0], index[1]);
        src = response.substring(index[2], index[3]);
        cmd = response.substring(index[4], index[5]);
        field = response.substring(index[6], index[7]);
        info1 = response.substring(index[8], index[9]);
        info2 = response.substring(index[10], index[11]);
        info3 = response.substring(index[12], index[13]);
        
        System.out.println(dest + " " + src + " " + cmd + " " + field + " "
                + info1 + " " + info2 + " " + info3);
        
        boolean condition = false;
        
        if (!src.matches(username) || cmd.matches("chat")) {
            condition = true;
        }
        
        if(condition) {
            if(dest.matches("all")) {
                if(cmd.matches("chat")) {
                    if(field.matches("chat")) {
                        appendChat(info1);
                    }
                } else if(cmd.matches("system")) {
                    if(field.matches("message")) {
                        appendSystemMessage(info1);
                    }
                }
            } else if(dest.matches(username)) {
                if(cmd.matches("your")) {
                    if(field.matches("turn")) {
                        String message =
                                "[Server " + df.format(new Date()) + "]: It is "
                                + "YOUR turn, please select from the "
                                + "possible locations highlighted in green";
                        appendSystemMessage(message);
                        myTurn = true;
                        int i = playerOrder.indexOf(username);
                        int j = charOrder.indexOf(charName);
                        if(i == j) {
                            guiOrder.setTurn(i);
                        }
                    }
                } else if (cmd.matches("possible")) {
                    if(field.matches("move")) {
                        if(info1.matches("locations")) {
                            for(GUIRoom room : guiRoomList) {
                                room.setMyTurn(myTurn);
                                if(room.getCol() == Integer.parseInt(info3) && 
                                   room.getRow() == Integer.parseInt(info2)) {
                                    room.setPossibleLocation(myTurn);
                                    System.out.println(room.getName());
                                }
                            }
                            for(GUIHall hallway : guiHallList) {
                                hallway.setMyTurn(myTurn);
                                if(hallway.getCol() == Integer.parseInt(info3) && 
                                   hallway.getRow() == Integer.parseInt(info2)) {
                                    hallway.setPossibleLocation(myTurn);
                                    System.out.println(hallway.getName());
                                }
                            }
                        }
                    }
                } else if (cmd.matches("not")) {
                    if(field.matches("turn")) {
                        String message =
                                "[Server " + df.format(new Date()) + "]: It is "
                                + "NOT your turn, please standby while another"
                                + " player takes their turn";
                        appendSystemMessage(message);
                        myTurn = false;
                        int i = playerOrder.indexOf(info1);
                        int j = charOrder.indexOf(info2);
                        System.out.println(info1+" with "+info2+" character's turn");
                        System.out.println(i+" "+j);
                        if(i == j) {
                            guiOrder.setTurn(i);
                        } else {
                            guiOrder.setTurn(0);
                        }
                    }
                } else if (cmd.matches("remove")) {
                    if(info1.matches("from")) {
                        if(info3.matches("location")) {
                            if(field.matches(info2)) { // Starting location
                                int a = 0;
                                for(int i = 0; i < suspectList.length; i++) {
                                    if(field.matches(suspectList[i])) {
                                        a = i;
                                    }
                                }
                                startingLocs.get(a).setCharacter1Image(null);
                            } else {
                                //info2 is cellName
                                //field is charName
                                if(info2.startsWith("Hall ")){
                                    for (GUIHall hallway : guiHallList) {
                                        if(hallway.getName().matches(info2)) {
                                            hallway.setCharacter2Image(null);
                                        }
                                    }
                                } else {
                                    for (GUIRoom room : guiRoomList) {
                                        if(room.getName().matches(info2)) {
                                            for(int i = 0; i < suspectList.length; i++) {
                                                if(suspectList[i].matches(field)) {
                                                    room.setCharacter(i, null);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (cmd.matches("move")) {
                    if(info1.matches("to")) {
                        //field is the character's name
                        int row = Integer.parseInt(info2);
                        int col = Integer.parseInt(info3);
                        GUIRoom r = null;
                        GUIHall h = null;
                        Image image = null;
                        for(ImageIcon piece : pieceIcons) {
                            if(field.matches(piece.getDescription())) {
                                image = piece.getImage();
                            }
                        }
                        for(GUIRoom room : guiRoomList){
                            if(room.getRow() == row && room.getCol() == col) {
                                r = room;
                                if(field.matches(charName)) {
                                    containerName = room.getName();
                                }
                            }
                        }
                        for(GUIHall hall : guiHallList) {
                            if(hall.getRow() == row && hall.getCol() == col) {
                                h = hall;
                                if(field.matches(charName)) {
                                    containerName = hall.getName();
                                }
                            }
                        }
                        if(h != null && image != null) {
                            h.setCharacter2Image(image);
                        } else if (r != null && image != null) {
                            for(int i = 0; i < suspectList.length; i++) {
                                if(suspectList[i].matches(field)) {
                                    r.setCharacter(i, image);
                                    break;
                                }
                            }
                        }
                    }
                } else if (cmd.matches("availableActions")) {
                    if(response.contains("Move")) {
                        System.out.println("avail: MOVE!");
                        myMove = true;
                    } else {
                        System.out.println("CANNOT MOVE!");
                        myMove = false;
                    }
                    
                    if(response.contains("Suggest")) {
                        System.out.println("avail: Suggest!");
                        mySuggest = true;
                        jrbSuggest.setEnabled(mySuggest);
                        jrbClear.setSelected(true);
                    } else {
                        System.out.println("CANNOT SUGGEST!");
                        mySuggest = false;
                        jrbSuggest.setEnabled(mySuggest);
                        jrbSuggest.setSelected(mySuggest);
                    }
                    
                    if(response.contains("Accuse")) {
                        System.out.println("avail: Accuse!");
                        myAccuse = true;
                        jrbAccuse.setEnabled(myAccuse);
                        jrbClear.setSelected(true);
                    } else {
                        System.out.println("CANNOT ACCUSE!");
                        myAccuse = false;
                        jrbAccuse.setEnabled(myAccuse);
                        jrbAccuse.setSelected(myAccuse);
                    }
                    
                    if(response.contains("EndTurn")) {
                        System.out.println("avail: EndTurn");
                        myEndTurn = true;
                        endTurn.setEnabled(myEndTurn);
                        endTurn2.setEnabled(myEndTurn);
                    } else {
                        System.out.println("CANNOT ENDTURN!");
                        myEndTurn = false;
                        endTurn.setEnabled(myEndTurn);
                        endTurn2.setEnabled(myEndTurn);
                    }
                } else if (cmd.matches("pickCard")) {
                    //dest src cmd field info1 info2 info3
                    jtpTabbedPane.setSelectedIndex(3);
                    String message = "[Server " + df.format(new Date())
                            + "]: You must select a card from your HAND"
                            + " to send back to " + field;
                    appendSystemMessage(message);
                    for (GUIHand guiHand1 : guiHandList) {
                        if(guiHand1.getCard().getCardValue().matches(info1)
                                || guiHand1.getCard().getCardValue().matches(info2) 
                                || guiHand1.getCard().getCardValue().matches(info3)) {
                            guiHand1.setSelectable(true);
                        }
                    }
                    sendCardTo = field;
                } else if (cmd.matches("showingCard")) {
                    //add in new frame???
                    f = new Frame();
                    f.setTitle(info3+" "+info1);
                    f.setSize(180,270);
                    for (ImageIcon icon : cardIcons) {
                        if (info3.matches(icon.getDescription())){
                            f.add(new JLabel(icon));
                        }
                    }
                    f.setVisible(true);
                    f.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent e) {
                            f.dispose();
                            f = null;
                        }
                    });                    
                    jtpTabbedPane.setSelectedIndex(1);
                    String message = "[Server "+df.format(new Date())+"]: "+src+" showed you the "+info1+" card of "+info3+", quick write it down on your notebook!";
                    appendSystemMessage(message);
                    jrbAccuse.setEnabled(myAccuse);
                    endTurn.setEnabled(myEndTurn);
                    endTurn2.setEnabled(myEndTurn);
                    if(!myMove) {
                        for (GUIRoom room : guiRoomList) {
                            room.setSelected(false);
                            room.setPossibleLocation(false);
                            room.setMyTurn(false);
                        }
                        for (GUIHall hallway : guiHallList) {
                            hallway.setSelected(false);
                            hallway.setPossibleLocation(false);
                            hallway.setMyTurn(false);
                        }
                    } else {
                        for (GUIRoom room : guiRoomList) {
                            room.setMyTurn(myMove);
                        }
                        for (GUIHall hallway : guiHallList) {
                            hallway.setMyTurn(myMove);
                        }
                        confirmMove.setEnabled(false);
                    }
                } else if(cmd.matches("system")) {
                    if(field.matches("message")) {
                        appendSystemMessage(info1);
                    }
                } else if(cmd.matches("initiate")) {
                    if(field.matches("countdown")) {
                        if(info1.matches("to")) {
                            if(info2.matches("end")) {
                                if(info3.matches("game")) {
                                    String message = "[Server "+df.format(new Date())+"]: Game Over.";
                                    appendSystemMessage(message);
                                    message = "[Server "+df.format(new Date())+"]: in 30 seconds the session will close and you will be returned to the Home screen.";
                                    appendSystemMessage(message);
                                    forceExit(30000);
                                }
                            }
                        }
                    }
                } else if (cmd.matches("set")) {
                    if (field.matches("disconn")) {
                        nl.stopMe();
                    }
                }
            }
        }
    }
    
    /**
     * This is the getter method to return the gameMenuPanelID
     * @return 
     */
    public int getGameMenuPanelID() {
        return gameMenuPanelID;
    }
    
    /**
     * This is the setter method for the gameMenuPanelID
     * @param name
     */
    public void setGameMenuPanelID(int name) {
        gameMenuPanelID = name;
    }
    
    /**
     * 
     * @param ca 
     */
    public void setClientApplication(ClientApplication ca) {
        this.ca = ca;
    }    
    
    /**
     * The movePiece method will be called when the player attempts to move 
     * their character on the board
     * @param row
     * @param col
     */
    public void movePiece(int row, int col) {
        System.out.println("The player is attempting to move to "
            + row + " " + col);
    }
    
    /**
     * The suggest method will be called when the player is attempting
     * a suggestion and will accept the arguments of who they are suggesting, what weapon they used
     * and in which room the murder took place
     * @param suspect
     * @param weapon
     * @param room
     */
    public void suggest(String suspect, String weapon, String room) {
        System.out.println("The player suggests "
			+ suspect + " in the "
			+ room + " with the "
			+ weapon);
        DataOutputStream toServer;
        try {
            toServer = new DataOutputStream(socket.getOutputStream());
            toServer.writeUTF("[0]["+username+"][suggest]["+suspect+"]["+room+"]["+weapon+"][0]");
        } catch (IOException ex) {
            Logger.getLogger(GameMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * @return 
     */
    public boolean canSuggestOrAccuse() {
        boolean condition = false;
        
        if(jcbWeapons.getSelectedIndex() != -1 &&
            jcbSuspects.getSelectedIndex() != -1 &&
            jcbRooms.getSelectedIndex() != -1) {
            condition = true;
        }
        
        return condition;
    }
    
    /**
     * The accuse method is called when the player makes their final accusation for the game.
     * @param suspect
     * @param weapon
     * @param room
     */
    public void accuse(String suspect, String weapon, String room) {
        System.out.println("The player accuses "
			+ suspect + " in the "
			+ room + " with the "
			+ weapon);
        DataOutputStream toServer;
        try {
            toServer = new DataOutputStream(socket.getOutputStream());
            //                dest    src        cmd       field      info1     info2
            toServer.writeUTF("[0]["+username+"][accuse]["+suspect+"]["+room+"]["+weapon+"][0]");
        } catch (IOException ex) {
            Logger.getLogger(GameMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * The pickCard() method is called when another player has made a suggestion.  This will in turn force the
     * the next player in order to show one of the cards that is in their hand that was part of the suggestion.
     * @param type
     * @param value
     */
    public void pickCard(String type, String value) {
        try {
            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
            System.out.println("["+sendCardTo+"]["+username+"][showingCard][cardType]["+type+"][value]["+value+"]");
            toServer.writeUTF("["+sendCardTo+"]["+username+"][showingCard][cardType]["+type+"][value]["+value+"]");
        } catch (IOException ex) {
            Logger.getLogger(GameMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    /**
     * @return
     */
    public boolean isActiveGame() {
        return true;
    }
    
    /**
     * The playerExitGame() method is called when the player exits the game intentionally
     */
    public void playerExitGame() {
        System.out.println("The player has exited the game");
    }
    
    /** 
     * 
     * @param sleep
     */
    public void forceExit(long sleep) {
        Runnable task = () -> {
            try {
                Thread.sleep(sleep);
                System.out.println("Thread ended");
                DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
                toServer.writeUTF("[0]["+username+"][set][disconn][0][0][0]");
                //ca.beginAgain();
                System.exit(0);
            } catch (InterruptedException | IOException ex) {
                Logger.getLogger(GameMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        Thread thread = new Thread(task);
        thread.start();
    }
    
  
    /**
     * 
     */
    private class KeyListenerImpl implements KeyListener {

        private final JTextField jtfChatEntry;

        public KeyListenerImpl(JTextField jtfChatEntry) {
            this.jtfChatEntry = jtfChatEntry;
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                if(!jtfChatEntry.getText().isEmpty()) {
                    String returnString = "";
                    
                    returnString +=
                            "[" + username + " " 
                            + df.format(new Date()) + "]: " +
                            jtfChatEntry.getText() + "]";
                    try {
                        DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
                        toServer.writeUTF("[all][" + username + "][chat][chat][" + returnString + "[0][0]");
                    } catch (IOException ex) {
                        System.err.println(ex);
                    }
                    jtfChatEntry.setText("");
                }
            }
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

    /**
     * 
     */
    private class FocusListenerImpl implements FocusListener {

        private final JTextField jtfChatEntry;

        public FocusListenerImpl(JTextField jtfChatEntry) {
            this.jtfChatEntry = jtfChatEntry;
        }

        @Override
        public void focusGained(FocusEvent e) {
            jtfChatEntry.setText("");
            jtfChatEntry.setForeground(Color.black);
        }

        @Override
        public void focusLost(FocusEvent e) {
            jtfChatEntry.setText(prompt);
            jtfChatEntry.setForeground(Color.lightGray);
        }
    }

}