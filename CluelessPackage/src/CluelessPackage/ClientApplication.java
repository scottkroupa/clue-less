/*
 *  The ClientApplication class 
 *	@author: Ryan Bonisch
 *	last updated: 12/1/16
 *
 *	
 */

package CluelessPackage;

/**
 * Import necessary libraries
 */
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 * This is the ClientApplication class that will create a frame to add
 * all of the other client side applications to it.  
 */
public class ClientApplication extends JFrame
        implements Runnable, CustomListener {
    
    // Declare global variables
    private String username;
    private Socket socket;
    private int playerNum;
    private Player player;
    private HomePanel homePanel;
    private GUITemplate templatePanel;
    private LobbyPanel lobbyPanel;
    private GameMenuPanel gameMenuPanel;
    private int sessionID;
    public ArrayList<Player> activePlayersInSession;
    private String detectiveLevel;
    private String template;
    
    /**
     * Here is the main method where when the ClientApplication is run
     * this method will kick off all subsequent methods.
     * @param args
     */
    public static void main(String[] args) {
        ClientApplication frame = new ClientApplication();
    }
    
    /**
     * This is the constructor for the ClientApplication
     */
    public ClientApplication() {
        /** Load the homePanel for connection
         * information (IP Address, port, name, password)
         */
        loadHomePanel();
        
        /**
         * Here is the Window Listener.  Essentially when the action of close
         * the window happens, we will issue a System.exit(0); but not before 
         * performing other actions.
         */
        addWindowListener(new WindowAdapter()
        {
            /**
             * Here is the windowClosing method for when the window is preparing
             * to close.
             * @param e 
             */
            @Override
            public void windowClosing(WindowEvent e)
            {
                /**
                 * 
                 */
                if (socket != null) {
                    if (socket.isConnected() && !socket.isClosed()) {
                        try {
                            System.out.println("socket.isConnected() = " + socket.isConnected());
                            System.out.println("socket.isClosed() = " + socket.isClosed());
                            System.out.println("socket.isOutputShutdown() = " + socket.isOutputShutdown());
                            System.out.println("socket.isInputShutdown() = " + socket.isInputShutdown());
                            DataOutputStream toServer = new DataOutputStream(socket.getOutputStream());
                            toServer.writeUTF("[0][" + playerNum + "][set][disconn][0]");
                            if(lobbyPanel != null && lobbyPanel.getNetworkListener() == null) {
                                lobbyPanel.stopNetworkListener();
                            }
                            socket.shutdownInput();
                            socket.shutdownOutput();
                            socket.close();
                        } catch (IOException ex) {
                            Logger.getLogger(ClientApplication.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else if (!socket.isClosed() && (!socket.isOutputShutdown() && !socket.isInputShutdown())) {
                        try {
                            socket.shutdownInput();
                            socket.shutdownOutput();
                        } catch (IOException ex) {
                            Logger.getLogger(ClientApplication.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                
                /**
                 * exit the system
                 */
                System.exit(0);
            }
        });
    }
    
    /**
     * @method The homePanel is where all of the connection information 
     *      exists such as username, password, ipaddress and port
     */
    public final void loadHomePanel() {
        
        // create the homePanel object
        // add a custom listener to the object
        // attach the homePanel to the frame
        homePanel = new HomePanel(this);
        homePanel.addListeners(this);
        
        // set some parameter options for the frame
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300,300);
        setTitle("ClueLess Client");
        setVisible(true);
    }
    
    /**
     * Once connected, display the lobbyPanel
     * @param username
     * @param socket
     * @param playerNum
     */
    public final void loadTemplate(){

        this.getContentPane().removeAll();
        //TODO: This is just a test size and we will need to return to this
        this.setSize(400,250);
        this.setMinimumSize(this.getSize());
        templatePanel = new GUITemplate(this);
        templatePanel.addListeners(this);
        this.revalidate();
        this.repaint();

    }


    public final void loadLobby(String template, String username, Socket socket, int playerNum) {
        
        // remove the homePanel from the ClientApplication
        this.getContentPane().removeAll();
        
        // reset the ClientApplication to appropriate size 
        // for the new lobby panel
        this.setSize(700,580);
        this.setMinimumSize(this.getSize());
        
        // Create the lobby panel
        //lobby = new LobbyPanel();
        //this.add(lobby);
        lobbyPanel = new LobbyPanel(this, template, username, socket, playerNum);
        lobbyPanel.addListeners(this);
        this.template = template;
        // refresh the ClientApplication frame
        this.revalidate();
        this.repaint();
    }
    
    /**
     * @param charName
     * @method after game start has begun, display the gameMenuPanel
     */
    public final void loadGameMenuPanel(String charName) {
        
        // remove the lobby from the ClientApplication
        this.getContentPane().removeAll();
        
        // reset the ClientApplication to appropriate size
        // for the new GameMenuPanel
        this.setSize(700, 600);
        this.setMinimumSize(this.getSize());

        
        // Create the gameMenuPanel
        gameMenuPanel = new GameMenuPanel(this, username, socket, playerNum, charName, template);
        gameMenuPanel.setClientApplication(this);
        this.add(gameMenuPanel);
        
        // refresh the ClientApplication frame
        this.revalidate();
        this.repaint();
        run();
    }

    /**
     * This is a required method for the listener to be added
     * to the homePanel object
     */
    @Override
    public void notifyListeners() {
    }
    
    /**
     * Once homePanel is done connecting, it will notify the
     * ClientApplication through this method.  Which will launch
     * the loadLobby() method
     * @param username
     * @param socket
     * @param playerNum
     */
    public void notified(String template){
       //TODO add template here
        loadLobby(template, username,socket, playerNum);
    }
    public void notified(String username, Socket socket, int playerNum, String detectiveLevel) {
        this.username = username;
        this.socket = socket;
        this.playerNum = playerNum;
        this.detectiveLevel = detectiveLevel;
        loadTemplate();
        // loadLobby(username, socket, playerNum);
    }
    
    /**
     * This is the getter method to return the username
     * @return string
     */
    public String getUsername() {
        return username;
    }
    
    /**
     * This is the setter method for the String username variable
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }
    
    /**
     * This is the getter method to return the player object
     * @return 
     */
    public Player getPlayer() {
        return player;
    }
    
    /**
     * This is the setter method for the Player object
     * @param player
     */
    public void setPlayer(Player player) {
        this.player = player;
    }
    
    /**
     * This is the getter method to return the lobby object
     * @return 
     *
    public LobbyPanel getLobby() {
        return lobby;
    }*/
    
    /**
     * This is the setter method for the lobby object
     * @param lobby
     *
    public void setLobby(LobbyPanel lobby) {
        this.lobby = lobby;
    }*/
    
    /**
     * This is the getter method to return the sessiondID integer
     * @return 
     */
    public int getSessionID() {
        return sessionID;
    }
    
    /**
     * This is the setter method for the sessionID integer variable
     * @param sessionID
     */
    public void setSessionID(int sessionID) {
        this.sessionID = sessionID;
    }
    
    /**
     * This is the getter method to return the activePlayersInSession arraylist
     * @return 
     */
    public ArrayList<Player> getActivePlayersInSession() {
        return activePlayersInSession;
    }
    
    /**
     * This is the setter method to set playerReady status
     */
    public void setPlayerReady() {
        System.out.println("Player is ready");
    }
    
    /**
     * This is the method to enable the start game button on the lobbyPanel
     * @param charName
     */
    public void enableStartGameButton(String charName) {
        loadGameMenuPanel(charName);
    }
    
    /**
     * 
     */
    public void beginAgain() {
        
        // remove the homePanel from the ClientApplication
        getContentPane().removeAll();
        setSize(300,300);
        setTitle("ClueLess Client");
        
        loadHomePanel();
        gameMenuPanel = null;
        lobbyPanel = null;
    }
    
    /**
     * This method that will tell everyone we are waiting for more players
     */
    public void tellWaitingForMorePlayers() {
        System.out.println("Waiting for more players");
    }
    
    /**
     * Here is the run method which will handle the logic when
     * in the gameMenuPanel.
     */
    @Override
    public void run() {
        System.out.println("Running");
    }
}