package CluelessPackage;
import java.util.ArrayList;
import java.util.List;

public class Notebook {

	private final String [] notebookItems = {"Col. Mustard", "Miss Scarlet", "Professor Plum", "Mr. Green", "Mrs. White", "Mrs. Peacock", "Knife", "Candlestick", "Revolver", "Rope", "Lead Pipe", "Wrench", "Hall", "Lounge", "Dining Room", "Kitchen", "Ballroom", "Conservatory", "Billiard Room", "Library", "Study"};
	private List<NotebookItem> items;
	
	//Constructor
	public Notebook(){
		items = new ArrayList<>();
		//for loop to set all to false
		for (int i=0; i < notebookItems.length; i++){
			items.add(new NotebookItem(notebookItems[i]));
		}		
	}
	
	public void updateNotebookItems(int index){
		//I do not think we need a status here depending on the intent
		NotebookItem n = items.get(index);
		n.setStatus();
	}
	public List<NotebookItem> getItems(){
		return items;
	}
	

	
	
	
	
	
}
