/**
 * Contributors: Ryan D Bonisch
 * Course: EN.605.401 Foundations of Software Engineering
 * Start: 04-Oct-16 14:15
 * End: 
 */
package CluelessPackage;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 */
public class NetworkListener extends Thread {

    // Initialize and declare Global variables
    private DataInputStream fromNetworkObject;
    private ArrayList<Player> players;
    private LobbyPanel lobby = null;
    private ServerApplication server = null;
    private HandleASession has = null;
    private GameMenuPanel gmp = null;
    volatile boolean complete = false;
     
    /**
     * 
     * @param lobby
     * @param fromNetworkObject
     */
    public NetworkListener(
            LobbyPanel lobby, DataInputStream fromNetworkObject) {
        this.fromNetworkObject = fromNetworkObject;
        this.lobby = lobby;
    }
    
    /**
     * 
     * @param server
     * @param fromNetworkObject
     */
    public NetworkListener(
            ServerApplication server, DataInputStream fromNetworkObject) {
        this.server = server;
        this.fromNetworkObject = fromNetworkObject;
    }
    
    /**
     * 
     * @param has
     * @param fromNetworkObject 
     */
    public NetworkListener(
            HandleASession has, DataInputStream fromNetworkObject) {
        this.has = has;
        this.fromNetworkObject = fromNetworkObject;
        System.out.println("Staring network listener");
    }
    
    /**
     * 
     * @param gmp
     * @param fromNetworkObject 
     */
    public NetworkListener(
            GameMenuPanel gmp, DataInputStream fromNetworkObject) {
        this.gmp = gmp;
        this.fromNetworkObject = fromNetworkObject;
        System.out.println("Staring network listener");
    }
    
    /**
     * 
     */
    @Override
    public void run() {
        while(!complete) {
            String response = "";
            
            try {
                response = fromNetworkObject.readUTF();
                System.out.println("Network Listener received " + response);
            } catch (IOException ex) {
                Logger.getLogger(NetworkListener.class.getName()).log(Level.SEVERE, null, ex);
                break;
            }
            
            int[] index = {0,0,0,0,0,0,0,0,0,0};
            String dest = "", src = "", cmd = "", field = "", info = "";

            index[0] = response.indexOf("[") + 1;
            index[1] = response.indexOf("]", index[0]);
            index[2] = response.indexOf("[", index[1]) + 1;
            index[3] = response.indexOf("]", index[2]);
            index[4] = response.indexOf("[", index[3]) + 1;
            index[5] = response.indexOf("]", index[4]);
            index[6] = response.indexOf("[", index[5]) + 1;
            index[7] = response.indexOf("]", index[6]);
            index[8] = response.indexOf("[", index[7]) + 1;
            index[9] = response.indexOf("]", index[8]);

            /*
            boolean condition = true;
            
            for (int i = 0; i < index.length; i++) {
                if(index[i] <= 0) {
                    condition = false;
                }
            }*/
            //if(condition) {
                dest = response.substring(index[0], index[1]);
                src = response.substring(index[2], index[3]);
                cmd = response.substring(index[4], index[5]);
                field = response.substring(index[6], index[7]);
                info = response.substring(index[8], index[9]);
            //}
            
            if(lobby != null)
            {
                try {
                    lobby.stringParser(response);
                } catch (IOException ex) {
                    Logger.getLogger(NetworkListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if(server != null) {
                try {
                    server.stringParser(response);
                } catch (IOException ex) {
                    Logger.getLogger(NetworkListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if(has != null) {
                try {
                    has.stringParser(response);
                } catch (IOException ex) {
                    Logger.getLogger(NetworkListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if(gmp != null) {
                try {
                    gmp.stringParser(response);
                } catch (IOException ex) {
                    Logger.getLogger(NetworkListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(field.matches("disconn")){
                stopMe();
            }
        }
    }
    
    /**
     * 
     */
    public void stopMe() {
        complete = true;
        /*
        try {
            fromNetworkObject.close();
        } catch (IOException ex) {
            Logger.getLogger(NetworkListener.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        System.out.println("Stopping network listener");
    }
}
