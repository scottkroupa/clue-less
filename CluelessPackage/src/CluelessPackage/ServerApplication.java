/*
 *  The ServerApplication class 
 *	@author: Ryan Bonisch
 *	last updated: 12/3/16
 *
 *	
 */

package CluelessPackage;

/** 
* Import all necessary libraries 
*/ 
import java.awt.*; 
import java.io.*;
import java.net.*; 
import java.text.SimpleDateFormat;
import java.util.ArrayList; 
import java.util.Date; 
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*; 

/** 
 *  This is the ServerApplication class that will be the centralized
 *  point for all game information
 */ 
public class ServerApplication extends JFrame {
    
    //Declare global variables
    private ArrayList<User> userSessionMap = new ArrayList();
    private ArrayList<ArrayList<Player>> list = new ArrayList();
    private ArrayList<Player> players = new ArrayList();
    private ArrayList<NetworkListener> networkListeners = new ArrayList();
    private SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    private int sessionNo = 1; 
    private int position = 0;
    private JTextArea jtaLog;
    private ExecutorService ss;
    private ArrayList<String> detLevel = new ArrayList();
    
    /** 
     * Here is the main method for the ServerApplication.
     * When the application is run, this method creates
     * a new ServerApplication object
     * @param args  
    */ 
    public static void main(String[] args) { 
        ServerApplication frame = new ServerApplication(); 
    }
    
    /**
     * Here is the constructor method for the ServerApplication class
     */
    public ServerApplication(){
        
        jtaLog = new JTextArea(); // Create a text area to add system messages to
        JScrollPane scrollPane = new JScrollPane(jtaLog); // Create a scroll pane to hold text area 
        add(scrollPane, BorderLayout.CENTER); // Add the scroll pane to the frame 
    
        // Set the frame parameters
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        setSize(400,400); 
        setTitle("ClueLess ServerApplication"); 
        setVisible(true); 
          
        try { 
            // Create a local variable for the port number and session number
            int serverSocketPort = 8000; 
            
            File det = new File("detectives.txt");
            Scanner inFile = new Scanner(det);
            while (inFile.hasNextLine()){
                String line = inFile.nextLine();
                detLevel.add(line);
            }
            
            File file = new File("users.csv");
            inFile = new Scanner(file);
            while (inFile.hasNextLine()){
                String line = inFile.nextLine();
                String[] s = line.split(",");
                User newUser = new User(s[0],s[1],Integer.parseInt(s[2]),s[3],0);
                userSessionMap.add(newUser);
            }
            inFile.close();

            // Create a server socket 
            ServerSocket serverSocket = new ServerSocket(serverSocketPort); 
            jtaLog.append(new Date() + 
                ": Server started at socket " + serverSocketPort + "\n"); 
            
            ss = Executors.newFixedThreadPool(6);

            while(true){
                Socket socket = serverSocket.accept();
                
                Runnable clientHandler = () -> {
                    try {
                        DataInputStream fromPlayer =
                                new DataInputStream(socket.getInputStream());
                        DataOutputStream toPlayer =
                                new DataOutputStream(socket.getOutputStream());
                        
                        //This is the name for the user not the player name/character name
                        String playerName = fromPlayer.readUTF();
                        String password = fromPlayer.readUTF();
                        
                        //
                        System.out.println("Received\nUsername: " + playerName);
                        System.out.println("Password: " + password);
                        
                        boolean matches = false;
                        boolean alreadyInGame = false;
                        
                        for (User userSessionMap1 : userSessionMap) {
                            if (playerName.matches(userSessionMap1.getUsername())
                                && password.matches(userSessionMap1.getPassword())) {
                                for (Player player : players) {
                                    if(player.getPlayerName().matches(playerName)){
                                        alreadyInGame = true;
                                    }
                                }
                                matches = true;
                            }
                        }
                        
                        if(matches && !alreadyInGame) {
                            System.out.println("correct login");
                            toPlayer.write(1);
                            Player player = new Player(playerName);
                            System.out.println("players.size() = " + players.size());
                            for(int i = 0; i < players.size(); i++) {
                                if(players.get(i) == null) {
                                    players.set(i, player);
                                    players.get(i).setSocket(socket);
                                    position = i + 1;
                                    System.out.println("position = " + position);
                                    break;
                                } else if (i == (players.size() - 1)) {
                                    position = players.size();
                                    players.add(player);
                                    players.get(position).setSocket(socket);
                                    position++;
                                    break;
                                }
                            }
                            System.out.println("players.size() = " + players.size());
                            if (players.isEmpty()) {
                                players.add(player);
                                players.get(position).setSocket(socket);
                                position++;
                            }
                            players.get(position-1).setSessionID(sessionNo);
                            players.get(position-1).setPlayerStatus(0);
                            players.get(position-1).setPlayerInGame(true);
                            System.out.println("position = " + position);
                            toPlayer.write(position);
                            for (User user : userSessionMap) {
                                if(user.getUsername().matches(playerName)){
                                    toPlayer.writeUTF(detLevel.get(user.getDetLevel()));
                                }
                            }
                            jtaLog.append(new Date() + ": Player "
                                    + (position) + " joined session "
                                    + sessionNo + '\n');
                            jtaLog.append("Player " + (position)
                                    + "'s IP address" +
                                    socket.getInetAddress().getHostAddress()
                                    + '\n');
                           
                            //add thread to thread manager arraylist
                            NetworkListener thread = 
                                    new NetworkListener(this, fromPlayer);
                            networkListeners.add(thread);
                            thread.start();
                            position = 0;
                        } else if (!matches) {
                            System.out.println(
                                    "incorrect username or password");
                            toPlayer.write(0);
                            socket.close();
                        } else if (alreadyInGame) {
                            System.out.println("already in the game");
                            toPlayer.write(2);
                            socket.close();
                        }
                    }catch (IOException ex) {
                        Logger.getLogger(LobbyPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                };
                
                ss.submit(clientHandler);
                
            }
        } catch (IOException ex) {
            Logger.getLogger(ServerApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void stringParser(String response) throws IOException { 
        DataOutputStream toClient;
        int[] index = {0,0,0,0,0,0,0,0,0,0};
        String dest = "", src = "", cmd = "", field = "", info = "";
        
        index[0] = response.indexOf("[") + 1;
        index[1] = response.indexOf("]");
        index[2] = response.indexOf("[", index[1]) + 1;
        index[3] = response.indexOf("]", index[2]);
        index[4] = response.indexOf("[", index[3]) + 1;
        index[5] = response.indexOf("]", index[4]);
        index[6] = response.indexOf("[", index[5]) + 1;
        index[7] = response.indexOf("]", index[6]);
        index[8] = response.indexOf("[", index[7]) + 1;
        index[9] = response.indexOf("]", index[8]);
        /*
        boolean condition = true;

        for (int i = 0; i < index.length; i++) {
            if(index[i] <= 0) {
                condition = false;
            }
        }*/
        //if(condition) {
            dest = response.substring(index[0], index[1]);
            src = response.substring(index[2], index[3]);
            cmd = response.substring(index[4], index[5]);
            field = response.substring(index[6], index[7]);
            info = response.substring(index[8], index[9]);
        //}
        
        System.out.println(response);
        //System.out.println(dest + " " + src + " " + cmd + " " + field + " " + info);
        
        if(dest.matches("all")) {
            for(int i = 0; i < players.size(); i++)
            {
                if (players.get(i) == null) { continue;}
                if((i+1) != Integer.parseInt(src) || cmd.matches("chat")){
                    toClient = new DataOutputStream(
                            players.get(i).getSocket().getOutputStream());
                    toClient.writeUTF(response);
                }
            }
        } else if(dest.matches("0")) {
            if(cmd.matches("get")) {
                if(field.matches("players")) {
                    if(info.matches("count")) {
                        int dst = Integer.parseInt(src)-1;
                        toClient = new DataOutputStream(players.get(dst).getSocket().getOutputStream());
                        toClient.writeUTF("[" + dst + "][0][set][players][" + players.size() + "]");
                    }
                }
            } else if(cmd.matches("set")) {
                if(field.matches("disconn")) {
                    int p = Integer.parseInt(src);
                    
                    // notify all players above
                    for (int i = 0; i < players.size(); i++) {
                        if (players.get(i) == null) { continue;}
                        if ((i+1) != p){
                            toClient = new DataOutputStream(
                                    players.get(i).getSocket().getOutputStream());
                            toClient.writeUTF(
                                    "[" + i + "][0][disconn][player][" + p + "]");
                            toClient.writeUTF(
                                    "[" + i + "][0][chat][all][[Server " 
                                    + df.format(new Date()) + "]: Player" 
                                    + p + " disconnected]");
                        }
                    }
                    players.set((p-1), null);
                    System.out.println("players.size() = " + players.size());
                } else if (field.matches("game")) {
                    if(info.matches("start")) {
                        networkListeners.stream().forEach((nls) -> {
                            nls.stopMe();
                        });
                        for (int i = 5; i > 0; i--) {
                            for(int j = 0; j < players.size(); j++) {
                                if (players.get(j) == null) { continue;}
                                toClient = new DataOutputStream(players.get(j).getSocket().getOutputStream());
                                toClient.writeUTF("[" + (j+1) + "][0][chat][all][[Server " + df.format(new Date()) + "]: Game starting in " + i + "...]");
                            }
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(ServerApplication.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        for(int j = 0; j < players.size(); j++) {
                            if (players.get(j) == null) { continue;}
                            toClient = new DataOutputStream(players.get(j).getSocket().getOutputStream());
                            toClient.writeUTF("[" + (j+1) + "][0][set][start][game]");
                        }
                        list.add(players);
                        createNewSession(sessionNo, players);
                    }
                } else if (field.matches("charName")) {
                    players.get((Integer.parseInt(src)-1)).setCharacterName(info);
                } else if (field.matches("color")) {
                    int r;
                    int g;
                    int b;
                    String sub = info.substring(15);
                    System.out.println(sub);
                    int r1 = sub.indexOf("=") + 1;
                    int r2 = sub.indexOf(",");
                    r = Integer.parseInt(sub.substring(r1, r2));
                    int g1 = sub.indexOf("=", r2) + 1;
                    int g2 = sub.indexOf(",", g1);
                    g = Integer.parseInt(sub.substring(g1, g2));
                    int b1 = sub.indexOf("=", g2) + 1;
                    int b2 = sub.length();
                    b = Integer.parseInt(sub.substring(b1, b2));
                    players.get(Integer.parseInt(src) - 1).setCaracterColor(new Color(r, g, b));
                }
            }
        } else if(dest.matches("1")) {
            if(players.size() >= 1){
                if(players.get(0) != null) {
                    toClient = new DataOutputStream(players.get(0).getSocket().getOutputStream());
                    toClient.writeUTF(response);
                }
            }
        } else if(dest.matches("2")) {
            if(players.size() >= 2){
                if(players.get(1) != null) {
                    toClient = new DataOutputStream(players.get(1).getSocket().getOutputStream());
                    toClient.writeUTF(response);
                }
            }
        } else if(dest.matches("3")) {
            if(players.size() >= 3){
                if(players.get(2) != null) {
                    toClient = new DataOutputStream(players.get(2).getSocket().getOutputStream());
                    toClient.writeUTF(response);
                }
            }
        } else if(dest.matches("4")) {
            if(players.size() >= 4){
                if(players.get(3) != null) {
                    toClient = new DataOutputStream(players.get(3).getSocket().getOutputStream());
                    toClient.writeUTF(response);
                }
            }
        } else if(dest.matches("5")) {
            if(players.size() >= 5){
                if(players.get(4) != null) {
                    toClient = new DataOutputStream(players.get(4).getSocket().getOutputStream());
                    toClient.writeUTF(response);
                }
            }
        } else if(dest.matches("6")) {
            if(players.size() >= 6){
                if(players.get(5) != null) {
                    toClient = new DataOutputStream(players.get(5).getSocket().getOutputStream());
                    toClient.writeUTF(response);
                }
            }
        }
    }
    
    /**
     * This is the method that will create a new session
     * @param sessionID
     * @param players
     */
    public void createNewSession(int sessionID, ArrayList<Player> players) {
        System.out.println("Creating a new Session");
        jtaLog.append("Session" + sessionNo + " has begun\n");
        sessionNo++;
        jtaLog.append("Session" + sessionNo + " is now active\n");
        
        HandleASession game = new HandleASession(sessionID, players);
        Thread gameThread = new Thread(game);
        gameThread.start();
        
        this.players = new ArrayList();
        this.ss = Executors.newFixedThreadPool(6);
        this.networkListeners = new ArrayList();
    }
    
    /**
     * This is the method that will receive PlayerReady Updates
     */
    public void receivePlayerReadyUpdates() {
        System.out.println("received Player ready ");
    }
}


class HandleASession implements Runnable {

    private Deck deck;
    private CaseFile caseFile;
    private GameController gameController;
    private Map map;
    private final ArrayList<Player> players;
    private final int sessionID;
    private ArrayList<NetworkListener> networkListeners;
    private SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    private ExecutorService ss = Executors.newFixedThreadPool(6);
    private boolean onGoing = true;
    
    public HandleASession(int sessionID, ArrayList<Player> players){
        this.players = players;
        this.sessionID = sessionID;
        init();
    }
    
    public final void init(){
        
        // initialize deck
        deck = new Deck();
        ArrayList<String> weapons = new ArrayList();
        ArrayList<String> suspects = new ArrayList();
        ArrayList<String> rooms = new ArrayList();
        weapons.add("Revolver");
        weapons.add("Knife");
        weapons.add("Lead Pipe");
        weapons.add("Rope");
        weapons.add("Candlestick");
        weapons.add("Wrench");
        suspects.add("Col. Mustard");
        suspects.add("Miss Scarlet");
        suspects.add("Professor Plum");
        suspects.add("Mr. Green");
        suspects.add("Mrs. White");
        suspects.add("Mrs. Peacock");
        rooms.add("Study");
        rooms.add("Hall");
        rooms.add("Lounge");
        rooms.add("Library");
        rooms.add("Billiard Room");
        rooms.add("Dining Room");
        rooms.add("Conservatory");
        rooms.add("Ballroom");
        rooms.add("Kitchen");
        deck.initDeckPiles(weapons, suspects, rooms);
        
        // Setup the crime
        caseFile = new CaseFile(deck.dealToCaseFile());
        System.out.println("caseFile = " + caseFile.getCrime());
        map = new Map();//players);
        gameController = new GameController(map);
        gameController.setCrime(caseFile);
        //This is error from 12/7/16 - Game controller needs to call broadcast message but cannot get a reference to handle a session
        gameController.setHandleASession(this);
        // Shuffle the deck for the game and deal to players
        System.out.println("shuffling and dealing cards to players");
        deck.shuffle();
        ArrayList<Card> hand = new ArrayList(deck.getCardsContained());
        ArrayList<String> charNames = new ArrayList(suspects);
        int num = 0;
        int rem;
        for (Player player : players) {
            if (player != null) {
                num+=1;
                player.resetActions();
                player.removeAction("EndTurn");
                charNames.remove(player.getCharacterName());
            }
        }
        rem = 18%num;
        System.out.println(rem);
        for (Player player : players) {
            if (player == null) {continue;}
            if (rem != 0) {
                rem--;
                try {
                    DataOutputStream toClient = new DataOutputStream(player.getSocket().getOutputStream());
                    toClient.writeInt((18/num) + 1);
                    System.out.println("Player " + player.getCharacterName() + " will received " + ((18/num) + 1) + " cards");
                } catch (IOException ex) {
                    Logger.getLogger(HandleASession.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    DataOutputStream toClient = new DataOutputStream(player.getSocket().getOutputStream());
                    toClient.writeInt((18/num));
                    System.out.println("Player " + player.getCharacterName() + " will received " + ((18/num)) + " cards");
                } catch (IOException ex) {
                    Logger.getLogger(HandleASession.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        while (!hand.isEmpty()) {
            for (Player player : players) {
                if(hand.isEmpty()) {break;}
                if(player == null) {continue;}
                try {
                    Card card = hand.remove(0);
                    Card card2 = new Card(card.getCardType(), card.getCardValue(), card.getPositionInDeck());
                    ObjectOutputStream toClient = new ObjectOutputStream(player.getSocket().getOutputStream());
                    toClient.writeObject(card);
                    card2.setOwner(player);
                    player.addCard(card2);
                } catch (IOException ex) {
                    Logger.getLogger(HandleASession.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        for (Player player : players) {
            System.out.println(player.getCards());
        }
        System.out.println("done!");
        
        // Determine turn order
        System.out.println("Determining turn order");
        int index = 0;
        for (int i = 0; i < players.size(); i++) {
            if (players.get(i).getCharacterName().contains("Scarlet")){
                index = i;
            }
        }
        ArrayList<Player> temp = new ArrayList();
        temp.add(players.get(index));
        for (int i = 0; i < players.size(); i++) {
            if(i == index || players.get(i) == null) {
                continue;
            } else {
                temp.add(players.get(i));
            }
        }
        temp.get(0).setMyTurn(true);
        gameController.setTurnOrder(temp);
        gameController.setActivePlayers(temp);
        for (Player player : players) {
            try {
                DataOutputStream toClient = new DataOutputStream(player.getSocket().getOutputStream());
                toClient.writeInt(temp.size());
                for (Player temp1 : temp) {
                    toClient.writeUTF(temp1.getPlayerName());
                    toClient.writeUTF(temp1.getCharacterName());
                }
            }catch (IOException ex) {
                Logger.getLogger(HandleASession.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Done!");
        
        //Generate NPC's
        System.out.println("Validating all players and adding NPCs");
        int count = 0;
        while(!charNames.isEmpty()) {
            if (players.get(count) == null) {
                players.set(count, new Player("NPC",charNames.remove(0)));
            } else {
                players.add(new Player("NPC", charNames.remove(0)));
            }
            count++;
        }
        for (Player player : players) {
            if (player.getPlayerName().matches("NPC")){
                player.setPlayerInGame(false);
                player.setGameController(gameController);
                player.setSessionID(sessionID);
                player.setPlayerStatus(0);
            }
        }
        map.setPlayerList(players);
        map.setStartingPoint();
        System.out.println("Done!");
        
        try {
            // Tell the first player they have their turn
            String name = gameController.getPlayerTurn().getPlayerName();
            //gameController.getPlayerTurn().resetActions();
            DataOutputStream toClient = new DataOutputStream(gameController.getPlayerTurn().getSocket().getOutputStream());
            toClient.writeUTF("[" + name + "][0][your][turn][0][0][0]");
            ArrayList<MapCell> locs = new ArrayList(gameController.getPossibleMoves(gameController.getPlayerTurn()));
            for (MapCell loc : locs) {
                toClient.writeUTF("[" + name + "][0][possible][move][locations][" + loc.getRow() + "][" + loc.getCol() + "]");
            }
            sendAvailActions(gameController.getPlayerTurn());
            
            for (int i = 1; i < gameController.getTurnOrder().size(); i++) {
                Player p = gameController.getTurnOrder().get(i);
                toClient = new DataOutputStream(p.getSocket().getOutputStream());
                toClient.writeUTF("[" + p.getPlayerName() + "][0][not][turn]["+name+"]["+gameController.getPlayerTurn().getCharacterName()+"][0]");
            }
        } catch (IOException ex) {
            Logger.getLogger(HandleASession.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("Printing all players in the players arrayList and appropriate info");
        for (Player player : players) {
            System.out.println("playerName = " + player.getPlayerName());
            System.out.println("characterName = " + player.getCharacterName());
            System.out.println("playerInGame = " + player.getPlayerInGame());
            System.out.println("myTurn = " + player.getMyTurn());
            System.out.println("currentRoom = " + player.getCurrentRoom().getCellName());
            System.out.println();
        }
        System.out.println("Done!");
        
        
        
        // initialize Network Listener
        networkListeners = new ArrayList();
        for (Player player : players) {
            if(player.getPlayerInGame()) {
                try {
                    player.setGameController(gameController);
                    DataInputStream fromClient = new DataInputStream(player.getSocket().getInputStream());
                    NetworkListener thread = new NetworkListener(this, fromClient);
                    player.setNetworkListener(thread);
                    networkListeners.add(thread);
                    thread.start();
                } catch (IOException ex) {
                    Logger.getLogger(HandleASession.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        System.out.println(networkListeners.size());
    }
    
    @Override
    public void run() {
        while(onGoing) {
            
        }
    }
    
    /**
     * 
     * @param player
     * @throws IOException 
     */
    public void sendAvailActions(Player player) throws IOException {
        DataOutputStream toClient = new DataOutputStream(player.getSocket().getOutputStream());
        String name = player.getPlayerName();
        ArrayList<String> availActions = new ArrayList(player.getAvailActions());
            String temp1 = "["+name+"][0][availableActions]";
            for (String avail : availActions) {
                temp1+= "[" + avail + "]";
            }
            if(availActions.size() < 4) {
                for(int i = 0; i < (4 - availActions.size()); i++) {
                    temp1+="[0]";
                }
            }
            System.out.println("temp1 = " + temp1);
            toClient.writeUTF(temp1);
    }
    
    public void endPlayersTurn() throws IOException {
        DataOutputStream toClient;
        
        Player p2 = gameController.getPlayerTurn();
        for (Player player : players) {
            if(player.getPlayerInGame() && !player.equals(p2)){
                toClient = new DataOutputStream(player.getSocket().getOutputStream());
                toClient.writeUTF("[" + player.getPlayerName() + "][0][not][turn]["+p2.getPlayerName()+"]["+p2.getCharacterName()+"][turn]");
            } else if (player.equals(p2)) {
                //p2.resetActions();
                String name = p2.getPlayerName();
                toClient = new DataOutputStream(p2.getSocket().getOutputStream());
                toClient.writeUTF("[" + name + "][0][your][turn][0][0][0]");
                ArrayList<MapCell> locs = new ArrayList(gameController.getPossibleMoves(p2));
                ArrayList<MapCell> locations = new ArrayList();
                if(locs.size() > 0) {
                    Iterator<MapCell> iter = locs.iterator();
                    while (iter.hasNext()) {
                        MapCell loc = iter.next();

                        if(!loc.isRoom()) {
                            System.out.println(loc.getCellName() + " is not a room");
                            Hall hallway = (Hall)loc;
                            if(!hallway.isOccupied()) {
                                locations.add(loc);
                            }
                        } else {
                            locations.add(loc);
                        }
                    }
                }
                System.out.println("printing possible move locations");
                System.out.println(locations);
                if(locations.size() > 0) {
                    for (MapCell loc : locations) {

                        toClient.writeUTF("[" + name + "][0][possible][move][locations][" + loc.getRow() + "][" + loc.getCol() + "]");
                    }
                } else {
                    toClient.writeUTF("[" + name + "][0][no possible][move][locations][0][0]");
                }
                sendAvailActions(p2);
            }
        }
    }
    
    public void incrementDetLevel(String playerName) {
        ArrayList<User> users = new ArrayList();
        File file = new File("users.csv");
        try (Scanner inFile = new Scanner(file)) {
            while (inFile.hasNextLine()){
                String line = inFile.nextLine();
                String[] s = line.split(",");
                User newUser = new User(s[0],s[1],Integer.parseInt(s[2]),s[3],Integer.parseInt(s[4]));
                users.add(newUser);
            }
            inFile.close();
            for (User user : users) {
                if(user.getUsername().matches(playerName)){
                    if(user.getDetLevel() < 19) {
                        user.setDetLevel(user.getDetLevel()+1);
                    }
                }
            }
            try (FileWriter fwriter = new FileWriter("users.csv")) {
                for (User user : users) {
                    fwriter.append(user.getUsername()+","+user.getPassword()+","+user.getSessionID()+","+user.getCharName()+","+user.getDetLevel()+"\n");
                }
                fwriter.close();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(HandleASession.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HandleASession.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * @param response
     * @throws IOException
     */
    public synchronized void stringParser(String response) throws IOException {
        System.out.println("stringParser was called");
        DataOutputStream toClient;
        int[] index = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        String dest = "", src = "", cmd = "", field = "", info1 = "", info2 = "", info3 = "";
        
        index[0] = response.indexOf("[") + 1;
        index[1] = response.indexOf("]");
        index[2] = response.indexOf("[", index[1]) + 1;
        index[3] = response.indexOf("]", index[2]);
        index[4] = response.indexOf("[", index[3]) + 1;
        index[5] = response.indexOf("]", index[4]);
        index[6] = response.indexOf("[", index[5]) + 1;
        index[7] = response.indexOf("]", index[6]);
        index[8] = response.indexOf("[", index[7]) + 1;
        index[9] = response.indexOf("]", index[8]);
        index[10] = response.indexOf("[", index[9]) + 1;
        index[11] = response.indexOf("]", index[10]);
        index[12] = response.indexOf("[", index[11]) + 1;
        index[13] = response.indexOf("]", index[12]);
        
        dest = response.substring(index[0], index[1]);
        src = response.substring(index[2], index[3]);
        cmd = response.substring(index[4], index[5]);
        field = response.substring(index[6], index[7]);
        info1 = response.substring(index[8], index[9]);
        info2 = response.substring(index[10], index[11]);
        info3 = response.substring(index[12], index[13]);
        
        System.out.println(response);
        
        if(dest.matches("all")) {
            for (Player player : players) {
                if(player.getPlayerInGame()) {
                    toClient = new DataOutputStream(player.getSocket().getOutputStream());
                    toClient.writeUTF(response);
                }
            }
        } else if (dest.matches("0")) {
            if(cmd.matches("move")) {
                for (Player player : players) {
                    if(player == gameController.getPlayerTurn()
                            && player.getPlayerName().matches(src) 
                            && player.getCharacterName().matches(field)) {
                        String from = player.getCurrentRoom().getCellName();
                        if(!from.matches(field)){
                            MapCell cell = map.find(from);
                            if(!cell.isRoom()) {
                                Hall hallway = (Hall)cell;
                                hallway.setOccupied(false);
                            }
                        }
                        System.out.println("from = " + from);
                        int row = Integer.parseInt(info1);
                        int col = Integer.parseInt(info2);
                        String ret = player.move(player, row, col);
                        System.out.println("ret = " + ret);
                        MapCell cell = map.find(ret);
                        System.out.println("move to "+cell.getCellName());
                        if(!cell.isRoom()) {
                            System.out.println(cell.getCellName()+" is a hallway");
                            Hall hallway = (Hall)cell;
                            System.out.println(cell.getCellName()+" setting to occupied");
                            hallway.setOccupied(true);
                            System.out.println("is "+cell.getCellName()+" occupied? "+hallway.isOccupied());
                        }
                        sendAvailActions(player);
                        for (Player player1: players) {
                            if(player1.getPlayerInGame()) {
                                toClient = new DataOutputStream(player1.getSocket().getOutputStream());
                                toClient.writeUTF("["+player1.getPlayerName()+"][0][remove]["+player.getCharacterName()+"][from]["+from+"][location]");
                                toClient.writeUTF("["+player1.getPlayerName()+"][0][move]["+player.getCharacterName()+"][to]["+row+"]["+col+"]");
                            }
                        }
                    }
                }
            } else if (cmd.matches("end")) {
                if (field.matches("turn")) {
                    Player p = null;
                    for (Player player : players) {
                        if(player.getPlayerName().matches(src)){
                            p = player;
                        }
                    }
                    System.out.println(p.endTurn(p));
                    endPlayersTurn();
                }
            } else if (cmd.matches("suggest")) {
                if(!field.matches(src)) { //suggesting someone else
                    Player p = null; // suggester
                    Player suspect = null;
                    //field is charName
                    //info1 is room
                    //info2 is weapon
                    //info3 is 0
                    String from = "";
                    for (Player player : players) {
                        if(player.getCharacterName().matches(field)) {
                            suspect = player;
                            from = player.getCurrentRoom().getCellName();
                        }
                        if(player.getPlayerName().matches(src)) {
                            p = player;
                        }
                    }
                    int row = map.find(info1).getRow();
                    int col = map.find(info1).getCol();
                    for (Player player : players) {
                        if(player.getPlayerInGame()){
                            toClient = new DataOutputStream(player.getSocket().getOutputStream());
                            System.out.println("["+player.getPlayerName()+"][0][remove]["+field+"][from]["+from+"][location]");
                            System.out.println("["+player.getPlayerName()+"][0][move]["+field+"][to]["+row+"]["+col+"]");
                            toClient.writeUTF("["+player.getPlayerName()+"][0][remove]["+field+"][from]["+from+"][location]");
                            toClient.writeUTF("["+player.getPlayerName()+"][0][move]["+field+"][to]["+row+"]["+col+"]");
                        }
                    }
                    Player cardHolder = p.suggest(suspect, info2);
                    System.out.println("suspects current room is " + suspect.getCurrentRoom().getCellName());
                    if(suspect.getCurrentRoom().getCellName().matches(from)){
                        if(!from.matches(field)){
                            MapCell cell = map.find(from);
                            if(!cell.isRoom()) {
                                Hall hallway = (Hall)cell;
                                hallway.setOccupied(false);
                            }
                        }
                        
                        suspect.setCurrentRoom(map.find(col, row));
                    }
                    System.out.println("suspects current room is " + suspect.getCurrentRoom().getCellName());
                    if(cardHolder!=null){
                        if(p.equals(cardHolder)) {
                            System.out.println("ERROR! cardHolder and suggesting player are the same!");
                        }
                        System.out.println(cardHolder.getPlayerName() + " has a card in your suggestion");
                        toClient = new DataOutputStream(p.getSocket().getOutputStream());
                        toClient.writeUTF("["+p.getPlayerName()+"][0][system][message][[Server " + df.format(new Date()) + "]: " + cardHolder.getPlayerName() + " has a card in your suggestion][0][0]");
                        toClient = new DataOutputStream(cardHolder.getSocket().getOutputStream());
                        toClient.writeUTF("["+cardHolder.getPlayerName()+"][0][pickCard]["+p.getPlayerName()+"]["+field+"]["+info1+"]["+info2+"]");
                    } else { // all 3 cards are in case file
                        String ret = gameController.annouceNoMatchingCard();
                        toClient = new DataOutputStream(p.getSocket().getOutputStream());
                        toClient.writeUTF("[all][0][system][message][[Server " + df.format(new Date())+"]: "+p.getPlayerName()+", No one has these three cards... there may be a reason for this...][0][0]");
                        sendAvailActions(p);
                    }
                } else { // suggesting self
                    Player p = null; // suggester & suspect
                    //field is charName
                    //info1 is room
                    //info2 is weapon
                    //info3 is 0
                    for (Player player : players) {
                        if(player.getPlayerName().matches(src)) {
                            p = player;
                        }
                    }
                    Player cardHolder = p.suggest(p, info2);
                    if(cardHolder!=null){
                        if(p.equals(cardHolder)) {
                            System.out.println("ERROR! cardHolder and suggesting player are the same!");
                        }
                        System.out.println(cardHolder.getPlayerName() + " has a card in your suggestion");
                        toClient = new DataOutputStream(p.getSocket().getOutputStream());
                        toClient.writeUTF("["+p.getPlayerName()+"][0][system][message][[Server " + df.format(new Date()) + "]: " + cardHolder.getPlayerName() + " has a card in your suggestion][0][0]");
                        toClient = new DataOutputStream(cardHolder.getSocket().getOutputStream());
                        toClient.writeUTF("["+cardHolder.getPlayerName()+"][0][pickCard][sendTo]["+p.getPlayerName()+"][0][0]");
                    } else { // all 3 cards are in case file
                        String ret = gameController.annouceNoMatchingCard();
                        toClient = new DataOutputStream(p.getSocket().getOutputStream());
                        toClient.writeUTF("[all][0][system][message][[Server " + df.format(new Date())+"]: "+p.getPlayerName()+", No one has these three cards... there may be a reason for this...][0][0]");
                        sendAvailActions(p);
                    }
                }
            } else if (cmd.matches("accuse")){
                String ret = "";
                for (Player player : players) {
                    if(player.getPlayerName().matches(src)) {
                        ret = player.accuse(field, info2, info1);
                        if (ret.contains("Lost")) {
                            endPlayersTurn();
                        } else if (ret.contains("Won")) {
                            incrementDetLevel(src);
                            for (Player player1 : gameController.getTurnOrder()) {
                                toClient = new DataOutputStream(player1.getSocket().getOutputStream());
                                toClient.writeUTF("["+player1.getPlayerName()+"][0][initiate][countdown][to][end][game]");
                            }
                        }
                    }
                }
                for (Player player : players) {
                    if(player.getPlayerInGame()) {
                        toClient = new DataOutputStream(player.getSocket().getOutputStream());
                        toClient.writeUTF("[all][0][system][message][[Server "+df.format(new Date())+"]: "+ret+"][0][0]");
                        System.out.println("[all][0][system][message][[Server "+df.format(new Date())+"]: "+ret+"][0][0]");
                    }
                }
                //12/11/16: JQ add check if all players have lost QQQQ                  
                //if (true){ //debug
                if (gameController.allPlayersLost()== true){
                    broadcastMessage("All players have lost!  How depressing... please play again!");
                    for (Player player1 : gameController.getTurnOrder()) {
                        toClient = new DataOutputStream(player1.getSocket().getOutputStream());
                        toClient.writeUTF("["+player1.getPlayerName()+"][0][initiate][countdown][to][end][game]");
                    }
                    System.out.println("Debug all players lost");
                }
            } else if (cmd.matches("set")) {
                if (field.matches("disconn")) {
                    for (Player player : players) {
                        if(player.getPlayerInGame()) {
                            player.getNetworkListener().stopMe();
                            toClient = new DataOutputStream(player.getSocket().getOutputStream());
                            toClient.writeUTF("[0]["+player.getPlayerName()+"][set][disconn][0][0][0]");
                        }
                    }
                }
            }
        } else { // did not receive message destined for server or all
            if(cmd.matches("showingCard")) {
                Player r = null; // receiving card
                Player s = null; // showing card
                for (Player player : players) {
                    if(player.getPlayerName().matches(dest)){
                        r = player;
                    } else if (player.getPlayerName().matches(src)) {
                        s = player;
                    }
                }

                for (Player player : players) {
                    if(player.getPlayerInGame()) {
                        if(player.equals(r)) {
                            toClient = new DataOutputStream(player.getSocket().getOutputStream());
                            toClient.writeUTF(response);
                        } else {
                            String ret = gameController.annouceCardShown(r, s);
                            toClient = new DataOutputStream(player.getSocket().getOutputStream());
                            toClient.writeUTF("[all][0][system][message][[Server " + df.format(new Date()) + "]: " + ret + "][0][0]");
                        }
                    }
                }
                sendAvailActions(r);
            } else {
                System.out.println("UNKNOWN RESPONSE line 974: " + response);
            }
        }
    }

    /**
     * This is the method that will broadcast out messages
     * @param message
     */
    public void broadcastMessage(String message) {
        DataOutputStream toClient;
        
        for (Player player : players) {
            if (player == null || !player.getPlayerInGame()) {
                continue;
            } else {
                try {
                    toClient = new DataOutputStream(player.getSocket().getOutputStream());
                    toClient.writeUTF("[all][0][system][message]["+message+"][0][0]");
                }catch (IOException ex) {
                    Logger.getLogger(HandleASession.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}