package CluelessPackage;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by scott on 12/9/2016.
 */
public class GUITemplate implements ActionListener{

    private DataOutputStream toServer = null;
    private DataInputStream fromServer = null;
    private JComboBox template;
    private JLabel templateText;
    private JPanel guiTemplate;
    private Socket socket;
    private JButton selectButton;
    private JLabel title;
    private ArrayList<ClientApplication> listeners;
    private String chosenTemplate;





    public GUITemplate(Frame frame){

        /*try {
            toServer = new DataOutputStream(socket.getOutputStream());
            fromServer = new DataInputStream(socket.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(LobbyPanel.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        //double componentWidth = frame.getWidth() - 10;
        //double componentHeight = frame.getHeight() / 10;
        listeners = new ArrayList();
        GridBagConstraints templateConstraints = new GridBagConstraints();
        JPanel templatePanel = new JPanel(new GridBagLayout());
        templateConstraints = new GridBagConstraints();
        templateConstraints.gridx = 0;
        templateConstraints.gridy = 7;
        templateConstraints.weighty = 1;
        String [] templates = {"Default", "Mysterious"};
        template = new JComboBox(templates);
        template.setSelectedIndex(0);
        //template.addActionListener(this);
        templateConstraints.gridwidth=2;
        templatePanel.add(template, templateConstraints);
        LineBorder lineBorder = new LineBorder(Color.BLACK, 1);
        templatePanel.setBorder(lineBorder);
        selectButton = new JButton();
        selectButton.setText("Select");
        templateConstraints.gridx = 1;
        templateConstraints.gridy = 8;
        templateConstraints.weighty = 2;
        templatePanel.add(selectButton, templateConstraints);
        title = new JLabel();
        title.setText("<html>" + "<p>Please choose which template for</p>" + "<p>characters you would like to use:</p>");
        templateConstraints.gridx = 0;
        templateConstraints.gridy = 3;
        templateConstraints.weighty = 0;
        templatePanel.add(title, templateConstraints);
        /*template.addItemListener((ItemEvent ie) -> {
                    if (ie.getStateChange() == ItemEvent.SELECTED) {
                        String change = Arrays.toString(ie.getItemSelectable().getSelectedObjects());
                        if (change.contains("Default"))
                            chosenTemplate = "Default";
                        else if (change.contains("Mysterious"))
                            chosenTemplate = "Mysterious";
                    }
                    ;
                    });
*/

        //TODO implement listener that tells you to move to the next page





        //chosenTemplate = (String) template.getSelectedItem();
        selectButton.addActionListener(this);
        frame.add(templatePanel);
        notifyListeners();
    }



    @Override
    public void actionPerformed(ActionEvent e) {
        chosenTemplate = (String)template.getSelectedItem();
        notifyListeners();
    }
    public void addListeners(ClientApplication client) {
        listeners.add(client);
    }



    public void notifyListeners() {
        listeners.stream().forEach((client) -> {
            client.notified(chosenTemplate);
        });
    }



}
