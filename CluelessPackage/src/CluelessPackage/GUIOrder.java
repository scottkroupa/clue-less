/*
 *  The GUIOrder class 
 *	@author: Ryan Bonisch
 *	last updated: 12/3/16
 *
 *	
 */

package CluelessPackage;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;
import java.lang.Math.*;

/**
 * 
 */
public class GUIOrder extends JPanel {
    
    // Declare global variables
    private final ArrayList<String> playerOrder;
    private final ArrayList<String> charOrder;
    private int playerTurn;
    
    /**
     * @method The constructor method for the GUIOrder class
     * @param playerOrder
     * @param charOrder 
     */
    public GUIOrder(ArrayList<String> playerOrder, ArrayList<String> charOrder) {
        this.playerOrder = playerOrder;
        this.charOrder = charOrder;
        playerTurn = 0;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        //g.setColor(Color.black);
        //g.drawOval(225, 65, 200, 200);
        
        for (int i = 0; i < playerOrder.size(); i++) {
            if(i == playerTurn) {
                g.setColor(Color.red);
            } else {
                g.setColor(Color.black);
            }
            g.drawString(playerOrder.get(i), 300, (i*50)+50);
            g.drawString(charOrder.get(i), 300, (i*50)+70);
        }
    }
    
    /**
     * 
     * @param num 
     */
    public void setTurn(int num) {
        this.playerTurn = num;
        repaint();
    }
}
