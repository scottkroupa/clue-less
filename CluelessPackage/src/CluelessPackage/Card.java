package CluelessPackage;

import java.io.Serializable;

/*
 *  The card class is for representing individual cards. It is used by the 
 *  Deck, Player, and CaseFile classes.
 *	@author: Christine Herlihy
 *	last updated: 11/12/16
 *
 *	NOTE: We may need to add additional constructors that require a subset of these fields
 *		Clarify whether PlayerLoc should be its own class, or a method/attribute of Player and/or Map.
 */

public class Card implements Serializable{
	
	private String cardType = new String(); // weapon, person, room
	private CardHolder owner = new CardHolder(); // can be either a Player object or a CaseFile object (both inherit from CardHolder) 
	private String cardValue = new String(); // type of weapon | name of person | name of room 
	private int positionInDeck = -99;
	

	/**
	 * @method: constructor without owner field set (for initDeck function) 
	 * @param cardType
	 * @param cardValue
	 * @param positionInDeck
	 */
	public Card(String cardType, String cardValue, int positionInDeck) {
		super();
		this.cardType = cardType;
		this.cardValue = cardValue;
		this.positionInDeck = positionInDeck;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}
	
	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	} 
	
	
	/**
	 * @return the owner
	 */
	public CardHolder getOwner() {
		return owner;
	}
	
	
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(CardHolder owner) {
		this.owner = owner;
	}
	
	
	/**
	 * @return the cardValue
	 */
	public String getCardValue() {
		return cardValue;
	}

	/**
	 * @param cardValue the cardValue to set
	 */
	public void setCardValue(String cardValue) {
		this.cardValue = cardValue;
	}

	/**
	 * @return the positionInDeck
	 */
	public int getPositionInDeck() {
		return positionInDeck;
	}

	/**
	 * @param positionInDeck the positionInDeck to set
	 */
	public void setPositionInDeck(int positionInDeck) {
		this.positionInDeck = positionInDeck;
	}
		

	@Override // added for testing purposes (re: shuffle function of deck) 
	public String toString() {
		return "Card [cardType=" + cardType + ", owner=" + owner
				+ ", cardValue=" + cardValue + ", positionInDeck="
				+ positionInDeck + "]";
	}

}
