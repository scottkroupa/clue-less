package CluelessPackage;
public class NotebookItem {

	private String item;
	private boolean status;
	
	//Constructor
	public NotebookItem(String item){
		this.item = item;
		this.status = false;
	}
	
	public String getItem(){
		return item;
	}
	public boolean getStatus(){
		return status;
	}
	
	/**
	 * explain
	 */
	public void setStatus(){
		status=!status;
	}
	
	
}
