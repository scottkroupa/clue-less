package CluelessPackage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/*
 *  The deck class holds the Card objects, and can be shuffled. The GameController 
 *  uses the Deck object to deal cards to CardHolder objects (-> subclasses = Player and CaseFile)
 *	@author: Christine Herlihy
 *	last updated: 11/12/16
 *
 */

public class Deck {
	
	private boolean shuffled = false;
	private ArrayList<Card> cardsContained = new ArrayList<Card> ();
	private ArrayList<Card> weaponCards = new ArrayList<Card> ();
	private ArrayList<Card> playerCards = new ArrayList<Card> ();
	private ArrayList<Card> roomCards = new ArrayList<Card> ();
	private int size = 0;

	
	// Empty constructor (for our purposes, deck gets "created" by the initDeckPiles method)
	
	public Deck() {
		super();
		
	}

	/**
	 * @return shuffled (T/F)
	 */
	public boolean isShuffled() {
		return shuffled;
	}
	
	/**
	 * @param shuffled the shuffled to set
	 */
	public void setShuffled(boolean shuffled) {
		this.shuffled = shuffled;
	}
	
	/**
	 * @return the weaponCards
	 */
	public ArrayList<Card> getWeaponCards() {
		return weaponCards;
	}

	/**
	 * @param weaponCards the weaponCards to set
	 */
	public void setWeaponCards(ArrayList<Card> weaponCards) {
		this.weaponCards = weaponCards;
	}

	/**
	 * @return the playerCards
	 */
	public ArrayList<Card> getPlayerCards() {
		return playerCards;
	}

	/**
	 * @param playerCards the playerCards to set
	 */
	public void setPlayerCards(ArrayList<Card> playerCards) {
		this.playerCards = playerCards;
	}

	/**
	 * @return the roomCards
	 */
	public ArrayList<Card> getRoomCards() {
		return roomCards;
	}

	/**
	 * @param roomCards the roomCards to set
	 */
	public void setRoomCards(ArrayList<Card> roomCards) {
		this.roomCards = roomCards;
	}
	

	public ArrayList<Card> getCardsContained() {
		return cardsContained;
	}

	public void setCardsContained(ArrayList<Card> cardsContained) {
		this.cardsContained = cardsContained;
	}

	/**
	 *	@method initDeckPiles adds a card for each item (i.e., weapon, player, room) into its respective pile (weapons, players
	 *			rooms)
	 *	@param ArrayList<String> weapons: the array list containing all possible weapons to use in game
	 *  @param ArrayList<String> players: the array list containing all possible players to use in game
	 *  @param ArrayList<String> rooms: the array list containing all possible rooms to use in game
	 *  @return void // can alter this to either return a deck object, or a true/false for init process complete 
	 */	
	public void initDeckPiles(ArrayList<String> weapons, ArrayList<String> players, ArrayList<String> rooms){
		
		// Instantiate a weapon card for each weapon in the list, and add it to the deck
		for(int i = 0; i < weapons.size(); i++){
			Card tempWeaponCard = new Card("weapon", weapons.get(i).toString(), i); // type: weapon; value: weapons[i]; placeInDeck: i
			this.getWeaponCards().add(tempWeaponCard);
		}
		
		// Instantiate a player card for each player in the list, and add it to the deck
		for(int i = 0; i < players.size(); i++){
			Card tempPlayerCard= new Card("player", players.get(i).toString(), i + weapons.size()); // type: player; value: players[i]; placeInDeck: i+weapons.size()
			this.getPlayerCards().add(tempPlayerCard);
		}
		
		// Instantiate a room card for each room in the list, and add it to the deck
		for(int i = 0; i < rooms.size(); i++){
			Card tempRoomCard= new Card("room", rooms.get(i).toString(), i + weapons.size() + players.size()); // type: room; value: rooms[i]; placeInDeck: i+weapons.size() + players.size()
			this.getRoomCards().add(tempRoomCard);
		}
		
		this.cardsContained.addAll(this.getWeaponCards());
		this.cardsContained.addAll(this.getPlayerCards());
		this.cardsContained.addAll(this.getRoomCards());

	} // end initDeck method 
	
	
	/**
	 *	@method dealToCaseFile shuffles each pile of cards (weapons, players rooms) separately, randomly selects a card 
	 *		from each pile, gives each card to the case file, and removes each card from the deck
	 *	@return ArrayList<Card> weapons: the array list containing all possible weapons to use in game
	 */		
	public ArrayList<Card> dealToCaseFile(){
		
		// Randomly select one weapon from the possible weapons
		Random randomGenerator = new Random();
		int low = 0;
		int high = this.getWeaponCards().size();
		int index = randomGenerator.nextInt(high-low) + low;
		
		//The selected weapon is the crime weapon; set this, and remove it from weapon cards
		Card crimeWeapon = this.getWeaponCards().get(index);
		this.getWeaponCards().remove(index);
	
		// Randomly select one player from the possible players
		Random randomGenerator2 = new Random();
		int low2 = 0;
		int high2 = this.getPlayerCards().size();
		int index2 = randomGenerator2.nextInt(high2-low2) + low2;
		
		//The selected player is the crime player; set this, and remove it from player cards
		Card crimePlayer = this.getPlayerCards().get(index2);
		this.getPlayerCards().remove(index2);
		
		// Randomly select one room from the possible rooms
		Random randomGenerator3 = new Random();
		int low3 = 0;
		int high3 = this.getRoomCards().size();
		int index3 = randomGenerator3.nextInt(high3-low3) + low3;
		
		//The selected room is the crime room; set this, and remove it from room cards
		Card crimeRoom = this.getRoomCards().get(index3);
		this.getRoomCards().remove(index3);
		
		this.cardsContained.clear();
		this.cardsContained.addAll(this.getWeaponCards());
		this.cardsContained.addAll(this.getPlayerCards());
		this.cardsContained.addAll(this.getRoomCards());
		
		// Initialize ArrayList<Card> to contain solution cards; add each solution item in 
		ArrayList<Card> solution = new ArrayList<Card>();
		solution.add(crimeWeapon);
		solution.add(crimePlayer);
		solution.add(crimeRoom);
		
		//Return solution (type: ArrayList<Card>) 
		return solution; 
		
	}	// end dealToCaseFile method 

	
	/**
	 *	@method shuffle combines all card piles into a single deck, shuffles this deck, flips the "shuffled" boolean to T,
	 *			and returns a shuffled deck
	 *	@return Deck object (shuffled == TRUE)
	 */		
	public Deck shuffle(){
		
		this.getCardsContained().clear();
		
		// Put all remaining weapon cards into the deck; reset positionInDeck value
		for(int i=0; i < this.getWeaponCards().size(); i++){
			this.getCardsContained().add(this.getWeaponCards().get(i));
			this.getCardsContained().get(i).setPositionInDeck(i);
		}
		
		// Put all remaining player cards into the deck; reset positionInDeck value
		for(int i=0; i < this.getPlayerCards().size(); i++){
			this.getCardsContained().add(this.getPlayerCards().get(i));
			this.getCardsContained().get(i).setPositionInDeck(i + this.getWeaponCards().size());
		}
		
		// Put all remaining room cards into the deck; reset positionInDeck value
		for(int i=0; i < this.getRoomCards().size(); i++){
			this.getCardsContained().add(this.getRoomCards().get(i));
			this.getCardsContained().get(i).setPositionInDeck(i + this.getWeaponCards().size() + this.getPlayerCards().size());
		}
		
		// Shuffle cards; this is an in-place operation, so "return this" returns a shuffled deck		
		Collections.shuffle(getCardsContained());
		
		for(int i = 0; i< this.getCardsContained().size(); i++){
			this.getCardsContained().get(i).setPositionInDeck(i);
		}
		
		this.setShuffled(true); // flip boolean switch to true 
		
		return this; // Right now, returns deck object; can be modified to return ArrayList<Card> cardsContained
		
	}	// end shuffle method 



}
