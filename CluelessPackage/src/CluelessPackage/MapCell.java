package CluelessPackage;
import java.util.ArrayList;

/**
 * Created by scott.kroupa on 11/29/2016.
 */
public abstract class MapCell {

    private String cellName;
    private int col;
    private int row;
    private ArrayList<MapCell> possibleLocation = new ArrayList<>();


    public MapCell(String cellName, int col, int row){
        this(cellName);
        this.col = col;
        this.row = row;
    }

    protected MapCell(String cellName){
        this.cellName = cellName;
        this.col = 0;
        this.row = 0;
        possibleLocation = new ArrayList<>();
    }

    public MapCell(){}

    public String getCellName(){
        return cellName;
    }
    public int getCol(){
        return col;
    }
    public int getRow(){
        return row;
    }
    public ArrayList<MapCell> getPossibleMoves(){
        return possibleLocation;
    }
    public void addPossibleMoves(MapCell cell){
        possibleLocation.add(cell);
        //We are only adding Halls to rooms and this is where the rooms will be added to the halls
        //cell.addPossibleMoves(this);
    }
    public void addDoor(MapCell cell){
        possibleLocation.add(cell);
    }
    public boolean isRoom(){
        return false;
    }


}
