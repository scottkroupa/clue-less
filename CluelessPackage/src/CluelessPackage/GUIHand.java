/*
 *  The GUIHand class 
 *	@author: Ryan Bonisch
 *	last updated: 12/3/16
 *
 *	
 */

/**
 * Package information
 */
package CluelessPackage;

/**
 * Import all necessary libraries
 */
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * @class 
 */
public class GUIHand extends JPanel {
    
    //Decleare global variables
    private boolean selectable;
    private boolean selected;
    private final ImageIcon icon;
    private final Card card;
    
    /**
     * @param icon
     * @param card
     * @method The constructor for the GUIHand object
     */
    public GUIHand(ImageIcon icon, Card card) {
        this.icon = icon;
        this.card = card;
        selectable = false;
    }
    
    /**
     * @method The overriden paintComponent method for the GUIHand object
     * @param g 
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        if(selectable) {
            g.setColor(Color.cyan);
            g.fillRect(0, 30, 164, 248);
        }
        if(selected) {
            g.setColor(Color.green);
            g.fillRect(0, 30, 164, 248);
        }
        
        g.drawImage(icon.getImage(), 10, 40, 154, 268, 0, 0, 160, 250, this);
    }
    
    /**
     * 
     * @return 
     */
    public Card getCard() {
        return card;
    }
    
    /**
     * @method The getter method for the selectable variable
     * @return 
     */
    public boolean getSelectable() {
        return selectable;
    }
    
    /**
     * @method The setter method for the selectable variable
     * @param selectable 
     */
    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }
    
    /**
     * @method The getter method for the selected variable
     * @return 
     */
    public boolean getSelected() {
        return selected;
    }
    
    /**
     * @param selected
     * @method The setter method for the selected variable 
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
        repaint();
    }
    
    /**
     * @method
     */
    public void fill() {
        if (selectable) {
            selected = !selected;
            repaint();
        }
    }
}
