package CluelessPackage;

import javax.swing.table.DefaultTableModel;

/**
 * Created by scott on 12/7/2016.
 */
public class NoteBookTableModel extends DefaultTableModel {



    public NoteBookTableModel(Object [][] data, Object [] col){
        super(data, col);

    }

    public Class<?> getColumnClass(int columnIndex)
    {
        return getValueAt(0, columnIndex).getClass();
    }

    @Override
    public boolean isCellEditable(int row, int col){
        if(col == 0)
            return false;
        else
            return true;
    }







}
