package CluelessPackage;
import java.util.ArrayList;

/**
 * Created by scott.kroupa on 11/29/2016.
 */
public class Room extends MapCell {


    public Room(String cellName, int col, int row){
        super(cellName, col, row);
    }
    @Override
    public boolean isRoom(){
        return true;
    }

}
