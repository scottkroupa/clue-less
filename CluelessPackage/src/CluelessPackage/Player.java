// Player Class for ClueLess application
// James Quaranta
// 11/29/16 Edited Move flow to include commented out if branch using MAP which is not ready yet
// all conditional branches mapped
// 11/30/16:  Added Playerloc location instance variable and
// setPlayerLoc(), getPlayerloc() methods
package CluelessPackage;
//  12/3/16:  updated to use gameController in constructor
//  12/4/16:  edited to use MapCell instead of Room
//   12/4/16 R2:  Added reference to GameMenuPanel, removed all stubs
//  12/5/16:  Implemented
//  12/7/16:  Cleanup

import java.awt.Color;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author jrquaran
 */
public class Player extends CardHolder{
    
    private Socket socket;
    private int sessionID;
    private String playerName;
    private int playerStatus;
    private boolean playerInGame;
    private ArrayList<Card> cards = new ArrayList<Card>();
    private boolean myTurn;
    private MapCell currentRoom;
    //private String username;//redundant with playerName
    private Color characterColor;
    private String characterIcon;
    private String characterName;
    private Template template;
    private String notebookItemToUpdate;
    private boolean playerReady;
    private int gameMenuPanelID;// is this needed?
    private NetworkListener nl = null;
    //need a reference to gameMenuPanel
    private GameMenuPanel gameMenuPanel;
    private ArrayList<String> availActions = new ArrayList<String>(); //has specialized sets
    private PlayerLoc location = new PlayerLoc(); //note, playerLoc method should have a player object to tell the map which icon to move...
    private GameController g1; //to access gameController methods

    
    //constructor.  May need more initialization
    public Player (String p_playerName){
        playerName = p_playerName;

    }
    public Player (String p_playerName, String p_characterName){
        playerName = p_playerName;
        characterName = p_characterName;
        
        
    }
    
    public Player (GameController gController, String p_playerName, String p_characterName){
        playerName = p_playerName;
        characterName = p_characterName;
        g1 = gController;
        
    }
    
    //set and get methods
    
    public void setGameController(GameController gController){
        g1 = gController;
        System.out.println("GameController g1 set to: " + gController);
    }
    
    public GameController getGameController(){
        return g1;
    }
    
    public void setNetworkListener(NetworkListener nl) {
        this.nl = nl;
    }
    
    public NetworkListener getNetworkListener() {
        return nl;
    }
    
    public void setSocket(Socket p_socket){
        socket = p_socket;
        System.out.println("socket set to: " +p_socket);
    }
    
    public Socket getSocket(){
        return socket;
    }

    public void setSessionID(int id){
        sessionID = id;
        System.out.println("sessionID set to:" + id);
    }
    
    public int getSessionID(){
        return sessionID;
    }
    
    public void setPlayerName(String p_playerName){
        playerName=p_playerName;
        System.out.println("playerName set to: " + p_playerName);
    }
    
    public String getPlayerName(){
        return playerName;
    }
    
    public void setPlayerStatus(int p_status){
        playerStatus = p_status;
        System.out.println("playerStatus set to: " + p_status);
    }
    
    public int getPlayerStatus(){
        return playerStatus;
    }
    
    public void setPlayerInGame(boolean p_playerInGame){
        playerInGame = p_playerInGame;
        System.out.println("playerInGame set to: " + p_playerInGame);
    }
    
    public boolean getPlayerInGame(){
       return playerInGame;
    }
    
       
    public void setCards(ArrayList<Card> p_cards){
        cards = p_cards;
        System.out.println("cards is set to: " + p_cards);
    }
   
    public ArrayList<Card> getCards(){
        return cards;
    }

    public void setMyTurn(boolean p_myTurn){
        
        //myTurn = true;  //stub
        myTurn=p_myTurn;
        System.out.println ("myTurn set to: " +myTurn);
    }
    
    public boolean getMyTurn(){
        return myTurn;
    }
    
    public void setCurrentRoom(MapCell p_currentRoom){
        currentRoom = p_currentRoom;
        System.out.println("currentRoom set to: " + p_currentRoom.getCellName());
    }
    
    public MapCell getCurrentRoom(){
        return currentRoom;  //sidebar, can extract room name from calling method if needed
    }
    
    public void setCaracterColor(Color p_charColor){
        characterColor = p_charColor;
        System.out.println("characterColor set to: " + p_charColor);
    }
    
    public Color getCharacterColor(){
        return characterColor;
    }
    
    public void setCharacterIcon(String p_charIcon){
        characterIcon = p_charIcon;
        System.out.println("characterIcon set to: " + p_charIcon);
    }
    
    public String getCharacterIcon(){
        return characterIcon;
    }
    
    public void setCharacterName(String p_charName){
        characterName = p_charName;
        System.out.println("characterName set to: " + p_charName);
    }
    
    public String getCharacterName(){
        return characterName;
    }
    
    public void setTemplate(Template p_template){
        template = p_template;
        System.out.println("template set to: "+ p_template);
    }
    
    public Template getTemplate(){
        return template;
    }
    
    public void setNotebookItemToUpdate(String p_notebookItem){
        notebookItemToUpdate = p_notebookItem;
        System.out.println("notebookItemToUpdate set to: " + p_notebookItem);
    }
    
    public String getNotebookItemToUpdate (){
        return notebookItemToUpdate;
    }
    
    public void setPlayerReady(boolean isReady){
        playerReady = isReady;
        System.out.println("playerReady set to: " + playerReady);
    }
    
    public boolean getPlayerReady(){
        return playerReady;
    }
    
    
    //not sure if we need gameMenuPanelID.
    public void setGameMenuPanelID(int gameMenuID){
        gameMenuPanelID = gameMenuID;
        System.out.println("gameMenuPanelID set to: " + gameMenuID);
    }
    
    
    //not sure if we need gameMenuPanelID.
    public int getGameMenuPanelID(){
        return gameMenuPanelID;
    }
    
    //12/4 added get/set for an actual GameMenuPanel.  Need to access PickCard
    public void setGameMenuPanel(GameMenuPanel p_gameMenuPanel){
        gameMenuPanel= p_gameMenuPanel;
        System.out.println("gameMenuPanel set to: " +gameMenuPanel);
    }
    
    public GameMenuPanel getGameMenuPanel(){
        return gameMenuPanel;
    }
    
    
    public ArrayList<String> getAvailActions(){
        return availActions;
    }

    
    public void setPlayerLoc(PlayerLoc p_location){
        location = p_location;
        System.out.println("location set to: " + location);
    }
    
    public PlayerLoc getPlayerLoc(){
        return location;
    }
    
    //Other useful methods
    
    /**
     * @method Adds a card to the players hand
     * @param card 
     * added per Ryan
     */
    public void addCard(Card card) {
        cards.add(card);
    }
    
    public String move (Player player, int row, int col){
        // 11/28/16: modified method to integrate with gameConroller.
        //11/29/16: if branch commented out since Map is not ready yet
        //full move UC mapped except communication with GameMenuPanel
        // 12/4/16: use MapCell instead of Room, and remove stubs
        // 12/5/16: Implemented
        
        
        //catch the room return from GameController.movePlayer()
        //Room aRoom = g1.movePlayer(this, row, col); //was Room type, now MapCell
        
        //debug
        //System.out.println("Debug Player.move called with: " + row+ col);//debug
        
        
        
        MapCell aMapCell = g1.movePlayer(player, row, col);
        
        
        //Debug, this works
        //Room aRoom = new Room("TestRoom",0,0);
        //MapCell aMapCell = aRoom;
        
        System.out.println("Debug Player.move():  MapCell: "+  aMapCell);
        
        //This section branches depending on if the move is valid, as tested by
        //Map.checkValidMove.   
        //Code structure here but commented out as Map is not working yet
        //Map m1 = new Map();//stub, should be in Main   
        //if (m1.checkValidMove(player, row, col) == true {
        
        //Changed 08Dec16
        //***********************************************************
        // error handling if room returned is null
        if (aMapCell != null ){//&& aMapCell.isRoom() ){ 
            //call Player.setCurrentRoom
            //this.setCurrentRoom(aMapCell);
            player.setCurrentRoom(aMapCell);
            //call Player.removeAction("Move")
            //this.removeAction("Move");
            player.removeAction("Move");
            if(aMapCell.isRoom()) {
                //call Player.addAction("Suggest")
                //this.addAction("Suggest");
                player.addAction("Suggest");
            } else if(!aMapCell.isRoom()){// && player.getAvailActions().contains("Suggest")) {
                player.removeAction("Suggest");
                player.addAction("EndTurn");
                
            }
            return aMapCell.getCellName();
        }else{
            return null; //  return a null MapCell. If can't do that, make a "fail" MapCell
        } //end if 
        //**************************************************************
    }
    
    public Player suggest (Player suspect, String weapon){
        //no room needed as parameter since must be the currentRoom
        // 11/28/16: modified method to integrate with gameConroller
        //  11/29/16:  all flows present including conditionals
        // 12/4/16: Removed stubs
        // 12/5/16: Implemented
        
        //debug
        System.out.println("Debug Player.suggest: currentRoom:"  + currentRoom);
        
        //moveSuspect() returns boolean , for test just print if return "true"
        boolean b = g1.moveSuspect(suspect, currentRoom);
        System.out.println("Debug Player.suggest:  Did Move return True? "+ b);//debug. remove when implemented
        
        //announceSuggestion() returns Card
        //if GameController.hasSuggestedCard() returns true, then a Card will be returned.
        //if returns false, null will be returned.
        //message back to GameMenuPanel (the return of this method) will either be
            //if true, message should be: ("The card is " +Card aCard.getCardValue()); 
            //if false, message should be: ("No card found");
        Player p = g1.announceSuggestion(this, suspect, weapon, currentRoom);
        //either if branch has removeAction("Suggest"), so put this first
        this.removeAction("Suggest");
        this.removeAction("Move");
        this.addAction("EndTurn");
        
        return p;
        /*
        if (aCard != null){
            return ("The card is: " + aCard.getCardValue());
        }else{
            return ("No card found");
        }//end if*/
    }
    
    public String accuse (String suspect, String weapon, String room){
        this.removeAllActions();
        // 11/28/16: modified method to integrate with gameController
        // 11/29/16:  flow structure in place with conditionals
        // 12/4/16: Removed stubs
        // 12/5/16: implemented
        
        //per flow, this method calls gameController annouceAccusation() and returns boolean
        
        boolean b = g1.announceAccusation(suspect, weapon, room);
        
        
        //if Accusation is correct, then setPlayerStatus(3) 3=won, 
        //and return a "player won" message
        //if Accusation is incorrect, then setPlayerStatus(2), 2=lost,
        //and removeAllActions, and return "player lost message"
       
            if (b == true){
                this.setPlayerStatus(3);//3=won
                return (this.playerName+ " Won! Congratulations!");
            }else{
                this.setPlayerStatus(2); //2=lost
                this.endTurn(this);
                return (this.playerName+ " Lost! Very Sorry!");
                
            }//end if
    }
    
    public String endTurn(Player player){
        // 11/28/16: modified method to integrate with gameController
        // 12/5/16 implemented
        //pass the CURRENT player turn to the setPlayerturn method, and allow
        //the method to figure out the next player.
        
        //per flow, this method calls gameController setPlayerTurn() with no return type
        //pass the CURRENT player turn to the setPlayerturn method, and allow
        //the method to figure out the next player.
        //GameController g1 = new GameController();
        
        //Added 08Dec16
        //**********************************************
        this.myTurn = false;
        if(player.getPlayerStatus() == 0) {
            this.addAction("Move");
            this.removeAction("EndTurn");
        }
        //**********************************************
        
               
        if(g1.incrementPlayerTurn(player) == null){ //pass CURRENT player and let method figure out next player
            return "Game Over!";
            
            
        } else {
            return ("Player Ended turn, next Player set to:" +g1.getPlayerTurn().getPlayerName()); //this should link to an s1. broadcast message
        }
    }
    
    public void checkMessages (String message){
        //12/5/16 implemented
        System.out.println(message);
    }
    
    public void addAction(String action){
        //for setting availActions Array
        //12/5/16 implemented
        //Changed 08Dec16
        //*********************************************
        if(!availActions.contains(action)){
            availActions.add(action);
        }
        //*********************************************
        System.out.println("Debug: Action added: " + action);//debug output
        System.out.println("Debug: Current Actions: " + availActions.toString());//debug output
    }
    
    public void removeAction(String action){
        //for setting availActions Array
        //12/5/16 implemented
        availActions.remove(action);
        System.out.println("Debug: Current Actions: " + availActions.toString());//debug output
    }
    
    public void removeAllActions(){
        //for setting availActions Array
        //12/5/16  Implemented
        availActions.clear();
        System.out.println("Debug: Current Actions: " + availActions.toString());//debug output
    }
    
    public void resetActions(){
        //for setting availActions Array
        //12/5/16 implemented
        availActions.clear();
        availActions.add("Move");
        availActions.add("Accuse");
        availActions.add("EndTurn");
        //"Suggest" is NOT a default action, must be in a Room for this to show up
        System.out.println("Debug: Current Actions: " + availActions.toString());// debug output
    }
    
    public Card pickCard(Player suspect, String weapon, MapCell room){
        //called by GameController.  It needs a card returned back.
        //  11/29/16  need to integrate with GameMeuPanel.pickCard()
        //  12/4/16: integrated with GameMeuPanel.pickCard(), removed stubs
        //  12/5/16: implemented
        
        
        //call gameMenuPanel.pickCard to ask actual player to pick a card
        //catch the card and pass the card value back to the GameController which
        //called this Player method.  Note that gameMenuPanel.pickCard takes Strings as arguments
        //Card aCard=gameMenuPanel.pickCard(suspect.characterName, weapon, room.getCellName());
        //System.out.println("Card picked is "+ aCard.getCardValue());//debug output
        //return aCard;
        return null;
    }
    
    public void selectTemplate(){
        // 12/5/16 unclear if this is being used
        System.out.println("Method 'selectTemplate' has no parameters nor return...");
    } 
    
    
}//close Player class
