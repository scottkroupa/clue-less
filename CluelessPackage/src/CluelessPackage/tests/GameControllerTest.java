//James Quaranta
//updated as of 12/3/16
package CluelessPackage;

import CluelessPackage.Room;
import CluelessPackage.CaseFile;
import CluelessPackage.Deck;
import CluelessPackage.GameController;
import CluelessPackage.Card;
import CluelessPackage.Player;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrquaran
 */
public class GameControllerTest {
    
    public GameControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTurnOrder method, of class GameController.
     */
    @Test
    public void testGetTurnOrder() {
        System.out.println("getTurnOrder");
        
        //needed to make a Player
        Map m1 = new Map();
        
        GameController instance = new GameController(m1);
        
        //jq added
        Player playerT = new Player (instance, "Christine","Miss Scarlet");
        Player playerC = new Player (instance, "Sally","The Librarian");
        Player playerZ = new Player (instance, "Jose","The Cook");
        ArrayList<Player> gamePlayers = new ArrayList<Player>();
            gamePlayers.add(playerT);
            gamePlayers.add(playerC);
            gamePlayers.add(playerZ);
        instance.setTurnOrder(gamePlayers);
        
        ArrayList expResult = gamePlayers;// jq was null
        ArrayList result = instance.getTurnOrder();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setTurnOrder method, of class GameController.
     */
    @Test
    public void testSetTurnOrder() {
        System.out.println("setTurnOrder");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
        
        
        //jq added
        Player playerT = new Player (instance, "Christine","Miss Scarlet");
        Player playerC = new Player (instance, "Sally","The Librarian");
        Player playerZ = new Player (instance, "Jose","The Cook");
        ArrayList<Player> activePlayers = new ArrayList<Player>();
            activePlayers.add(playerT);
            activePlayers.add(playerC);
            activePlayers.add(playerZ);
        
        
        instance.setTurnOrder(activePlayers);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getPlayerTurn method, of class GameController.
     */
    @Test
    public void testGetPlayerTurn() {
        System.out.println("getPlayerTurn");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
        
        
        
        //jq added
        Player playerT = new Player (instance, "ImNextPlayer","Miss Scarlet");
        instance.incrementPlayerTurn(playerT);
        
        
        //test for Strings instead of Player objects
        
        String expResult = playerT.getPlayerName();
        String result = instance.getPlayerTurn().getPlayerName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setPlayerTurn method, of class GameController.
     */
    @Test
    public void testIncrementPlayerTurn() {
        System.out.println("setPlayerTurn");
        
        //jq added
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
        
        Player playerT = new Player (instance, "Christine","Miss Scarlet");
  
        Player p_playerTurn = playerT;
        
        Player player1 = new Player(instance,"JQ", "Butler");
        Player player2 = new Player (instance,"Ryan", "Cook");
        Player player3 = new Player (instance,"Christine", "Miss Scarlet");
        ArrayList playerArray = new ArrayList();
            playerArray.add(player1);
            playerArray.add(player2);
            playerArray.add(player3);
        
        instance.incrementPlayerTurn(p_playerTurn);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCrime method, of class GameController.
     */
    @Test
    public void testGetCrime() {
        System.out.println("getCrime");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
        
        
        Player player1 = new Player(instance, "JQ", "Butler");
        Player player2 = new Player (instance, "Ryan", "Cook");
        Player player3 = new Player (instance,"Christine", "Miss Scarlet");
        ArrayList playerArray = new ArrayList();
            playerArray.add(player1);
            playerArray.add(player2);
            playerArray.add(player3);
        
        //to build casefile
        Card card1=new Card("Suspect", "Scarlet", 1);
        Card card2=new Card("Weapon", "handgrenade", 2);
        Card card3=new Card("Room", "Library", 3);
        ArrayList<Card> cards4CaseFile = new ArrayList<Card>(); 
          cards4CaseFile.add(card1);
          cards4CaseFile.add(card2);
          cards4CaseFile.add(card3);
        CaseFile case1 = new CaseFile(cards4CaseFile);
        instance.setCrime(case1);
        
        
        CaseFile expResult = case1;
        CaseFile result = instance.getCrime();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCrime method, of class GameController.
     */
    @Test
    public void testSetCrime() {
        System.out.println("setCrime");
        
         //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        //to build casefile
        Card card1=new Card("Suspect", "Scarlet", 1);
        Card card2=new Card("Weapon", "handgrenade", 2);
        Card card3=new Card("Room", "Library", 3);
        ArrayList<Card> cards4CaseFile = new ArrayList<Card>(); 
          cards4CaseFile.add(card1);
          cards4CaseFile.add(card2);
          cards4CaseFile.add(card3);
        CaseFile case1 = new CaseFile(cards4CaseFile);
        
        
        
        CaseFile p_crime = case1;
       
        
        instance.setCrime(p_crime);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getActivePlayers method, of class GameController.
     */
    @Test
    public void testGetActivePlayers() {
        System.out.println("getActivePlayers");
        
         //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        
        
        
        //jq added
        Player playerT = new Player (instance, "Christine","Miss Scarlet");
        Player playerC = new Player (instance, "Sally","The Librarian");
        Player playerZ = new Player (instance, "Jose","The Cook");
        ArrayList<Player> gamePlayers = new ArrayList<Player>();
            gamePlayers.add(playerT);
            gamePlayers.add(playerC);
            gamePlayers.add(playerZ);
        instance.setActivePlayers(gamePlayers);
        
        
        ArrayList expResult = gamePlayers;
        ArrayList result = instance.getActivePlayers();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setActivePlayers method, of class GameController.
     */
    @Test
    public void testSetActivePlayers() {
        System.out.println("setActivePlayers");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        
        //jq added
        Player playerT = new Player (instance, "Christine","Miss Scarlet");
        Player playerC = new Player (instance,"Sally","The Librarian");
        Player playerZ = new Player (instance,"Jose","The Cook");
        ArrayList<Player> gamePlayers = new ArrayList<Player>();
            gamePlayers.add(playerT);
            gamePlayers.add(playerC);
            gamePlayers.add(playerZ);
        
        
        ArrayList p_activePlayers = gamePlayers;
        instance.setActivePlayers(p_activePlayers);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of movePlayer method, of class GameController.
     */
    @Test
    public void testMovePlayer() {
        
        //  11/28/16  updated to integrate with Map
        
        System.out.println("movePlayer");
        
        //jq added
       
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        Player playerT = new Player (instance, "Christine","Miss Scarlet");
        Room roomX = new Room("Library", 0,0);
        
        Player player = playerT;
        int row = 1;
        int col = 2;
        
        
        //Note, can't directly compare objects so change the test to see
        //if the room names (which are strings) are the same
        
        //Room expResult = null;
        String expResult = roomX.getCellName();
        
        //Room result = instance.movePlayer(player, row, col);
        String result = instance.movePlayer(player, row, col).getCellName();
        
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of announceMoved method, of class GameController.
     */
    @Test
    public void testAnnounceMoved() {
        System.out.println("announceMoved");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        
        Player playerT = new Player (instance, "Scott","The Butler");
        
        
        Player player = playerT;
        
        instance.announceMoved(player);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of moveSuspect method, of class GameController.
     */
    @Test
    public void testMoveSuspect() {
        System.out.println("moveSuspect");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        
        Player playerS = new Player (instance, "Christine","Miss Scarlet");
        
        
        Player suspect = playerS;
        Room roomX = new Room("Library", 0,0);
        
        
        Room room = roomX;
        
        instance.moveSuspect(suspect, room);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of announceSuggestion method, of class GameController.
     */
    @Test
    public void testAnnounceSuggestion() {
        //JQ 11/28/16 edited to integrate with SeverApplication to broadcast message
        // and Player to return a Card
        
        System.out.println("announceSuggestion");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        
        Player playerT = new Player (instance, "PlayerWithTurn","Miss Scarlet");
        Player playerS = new Player (instance, "Christine","Miss Scarlet");
        Room roomX = new Room("Library",0,0);
        Card card1 = new Card ("Weapon", "MiniGun",1);//this is the card 
        
        Player suspect = playerS;
        String weapon = "Pipe";
        Room room = roomX;
        
        //test for card value(type is String) vs. Card object since object<>object
        String expResult = card1.getCardValue();
        String result = instance.announceSuggestion(playerT, suspect, weapon, room).getCardValue();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of announceAccusation method, of class GameController.
     */
    @Test
    public void testAnnounceAccusation() {
        System.out.println("announceAccusation");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        
        Player playerS = new Player (instance, "Christine","Miss Scarlet");
        Room roomX = new Room("Library",0,0);
        
        Player suspect = playerS;
        String weapon = "Pipe";
        Room room = roomX;
        
        boolean expResult = true;
        boolean result = instance.announceAccusation(suspect, weapon, room);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of hasSuggestedCard method, of class GameController.
     */
    @Test
    public void testHasSuggestedCard() {
        System.out.println("hasSuggestedCard");
        
    //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        
        Player playerS = new Player (instance, "PlayerWithCard", "CharacterWithCard");
        Room roomX = new Room("Library",0,0);
        
        
        
        Player suspect = playerS;
        String weapon = "pipe";
        Room room = roomX;
        
        //Test for player name vs. player object since object<>object
        
        String expResult = playerS.getCharacterName();
        String result = instance.hasSuggestedCard(suspect, weapon, room).getCharacterName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of annouceCardShown method, of class GameController.
     */
    @Test
    public void testAnnouceCardShown() {
        System.out.println("annouceCardShown");
       
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        
        
        Player playerT = new Player (instance, "Christine","Miss Scarlet");
        Player playerC = new Player (instance, "Sally","The Librarian");
        
        
        Player p_playerT = playerT;
        Player p_playerC = playerC;
        
        instance.annouceCardShown(p_playerT, p_playerC);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of annouceNoMatchingCard method, of class GameController.
     */
    @Test
    public void testAnnouceNoMatchingCard() {
        System.out.println("annouceNoMatchingCard");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        instance.annouceNoMatchingCard();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkAccusation method, of class GameController.
     */
    @Test
    public void testCheckAccusation() {
        System.out.println("checkAccusation");
       
//needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
        
        
        Player playerS = new Player (instance, "Christine","Miss Scarlet");
        Room roomX = new Room("Library",0,0);
        
        
        Player suspect = playerS;
        String weapon = "pipe";
        Room room = roomX;
        
        boolean expResult = true;
        boolean result = instance.checkAccusation(suspect, weapon, room);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of announceAccusationResult method, of class GameController.
     */
    @Test
    public void testAnnounceAccusationResult() {
        System.out.println("announceAccusationResult");
        boolean result_2 = false;
        
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        
        instance.announceAccusationResult(result_2);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of dealCardsToPlayer method, of class GameController.
     */
    @Test
    public void testDealCardsToPlayer() {
        System.out.println("dealCardsToPlayer");
        
        Deck deck1 = new Deck();
       
        //needed to make a Player
        Map m1 = new Map();
        GameController instance = new GameController(m1);
       
        
        Player playerT = new Player (instance, "Christine","Miss Scarlet");
        Player playerC = new Player (instance, "Sally","The Librarian");
        Player playerZ = new Player (instance, "Jose","The Cook");
        ArrayList<Player> activePlayers = new ArrayList<Player>();
            activePlayers.add(playerT);
            activePlayers.add(playerC);
            activePlayers.add(playerZ);
       
        Deck deck = deck1;
        
        boolean expResult = true;
        boolean result = instance.dealCardsToPlayer(deck, activePlayers);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
