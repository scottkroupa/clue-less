package CluelessPackage.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CardTest.class, CaseFileTest.class, DeckTest.class,
		GameControllerTest.class, HallTest.class, HomePanelTest.class,
		LobbyPanelTest.class, MapTest.class, NotebookItemTest.class,
		NotebookTest.class, PlayerLocTest.class, PlayerTest.class,
		RoomTest.class, UserTest.class })
public class AllTests {

}
