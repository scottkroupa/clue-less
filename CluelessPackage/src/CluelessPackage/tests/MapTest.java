package CluelessPackage.tests;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by scott.kroupa on 11/30/2016.
 */
public class MapTest {
    @Test
    public void association() throws Exception {
        Map m = new Map();
        MapCell cell = m.find("Study");
        List<MapCell> l = cell.getPossibleMoves();
        MapCell c = l.get(0);
        assertEquals("Hall 1", c.getCellName());
        }



    @Test
    public void find() throws Exception {
        Map m = new Map();
        MapCell cell = m.find("Library");
        assertEquals("Library", cell.getCellName());
    }

    @Test
    public void find1() throws Exception {
        Map m = new Map();
        MapCell cell = m.find(0,0);
        assertEquals("Study", cell.getCellName());
    }

    @Test
    public void addPlayer() throws Exception {
        ArrayList<Player> players = new ArrayList<>();
        Player p = new Player("Sara");
        players.add(p);
        assertEquals(1,players.size());

    }

    @Test
    public void movePlayer() throws Exception {
        /*ArrayList<Player> players = new ArrayList<>();
        Player p = new Player("Mona Lisa");
        Map m = new Map();
        MapCell c = m.find("Study");
        Room r = new Room("Test", 5,5);
        p.setCurrentRoom((Room) r);
        r.addPossibleMoves(c);
        movePlayer();
        */

    }
    @Test
    public void addDoor() throws Exception {
        Map m = new Map();
        Door d = new Door("test");
        MapCell c = m.find("Study");
        c.addDoor(d);
        MapCell e = c.getPossibleMoves().get(3);
        assertEquals("test",e.getCellName());
    }

}