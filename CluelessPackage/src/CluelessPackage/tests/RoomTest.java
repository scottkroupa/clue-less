package CluelessPackage.tests;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by scott.kroupa on 11/30/2016.
 */
public class RoomTest {
    @Test
    public void getCol() throws Exception {
        Map m = new Map();
        MapCell a = m.find("Lounge");
        assertEquals(4,a.getCol());
    }

    @Test
    public void getRow() throws Exception {
        Map m = new Map();
        MapCell a = m.find("Lounge");
        assertEquals(0,a.getRow());
    }

    @Test
    public void PossibleMoves() throws Exception {
        MapCell b = null;
        Map test = new Map();
        MapCell a = test.find("Study");
        a.addPossibleMoves(test.find("Hall"));
        List <MapCell> l = a.getPossibleMoves();
        for(MapCell cell: l) {
            if (cell.getCellName().equals("Hall"))
                b = cell;
        }
        assertEquals("Hall", b.getCellName());
    }


    @Test
    public void getCellName() throws Exception {
        Map m = new Map();
        MapCell a = m.find("Lounge");
        assertEquals("Lounge",a.getCellName());
    }

    @Test
    public void isRoom() throws Exception {
        Map test = new Map();
        MapCell a = test.find("Kitchen");
        assertEquals(true, a.isRoom());
    }

}