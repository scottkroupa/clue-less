//James Quaranta
//updated as of 12/3/16


package CluelessPackage;

import CluelessPackage.Room;
import CluelessPackage.Template;
import CluelessPackage.Card;
import CluelessPackage.Player;
import java.awt.Color;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrquaran
 */
public class PlayerTest {
    
    public PlayerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setSocket method, of class Player.
     */
    @Test
    public void testSetSocket() {
        System.out.println("setSocket");
        
        Socket socket1 = new Socket();
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        
        Socket p_socket = socket1;
        Player instance = playerTest;
        instance.setSocket(p_socket);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSocket method, of class Player.
     */
    @Test
    public void testGetSocket() {
        System.out.println("getSocket");
        
        //can't test for any socket attributes so test 
        Socket socket1 = null;
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        
        Player instance = playerTest;
        Socket expResult = socket1;
        
        
        Socket result = instance.getSocket();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSessionID method, of class Player.
     */
    @Test
    public void testSetSessionID() {
        System.out.println("setSessionID");
        int id = 0;
        
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        instance.setSessionID(id);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSessionID method, of class Player.
     */
    @Test
    public void testGetSessionID() {
        System.out.println("getSessionID");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        int expResult = 0;
        int result = instance.getSessionID();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setPlayerName method, of class Player.
     */
    @Test
    public void testSetPlayerName() {
        System.out.println("setPlayerName");
        
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1,"defaultName", "defaultCharacter");
        
        
        String p_playerName = "CoolName";
        Player instance = playerTest;
        instance.setPlayerName(p_playerName);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getPlayerName method, of class Player.
     */
    @Test
    public void testGetPlayerName() {
        System.out.println("getPlayerName");
        
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        String expResult = "defaultName";
        String result = instance.getPlayerName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setStatus method, of class Player.
     */
    @Test
    public void testSetStatus() {
        System.out.println("setStatus");
        
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        
        int p_status = 0;
        Player instance = playerTest;
        instance.setPlayerStatus(p_status);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getStatus method, of class Player.
     */
    @Test
    public void testGetStatus() {
        System.out.println("getStatus");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        int expResult = 0;
        int result = instance.getPlayerStatus();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setPlayerInGame method, of class Player.
     */
    @Test
    public void testSetPlayerInGame() {
        System.out.println("setPlayerInGame");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        boolean p_playerInGame = false;
        Player instance = playerTest;
        instance.setPlayerInGame(p_playerInGame);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getPlayerInGame method, of class Player.
     */
    @Test
    public void testGetPlayerInGame() {
        System.out.println("getPlayerInGame");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        boolean expResult = false;
        boolean result = instance.getPlayerInGame();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCards method, of class Player.
     */
    @Test
    public void testSetCards() {
        System.out.println("setCards");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Card cardMustard= new Card("Suspect","Mustard",1);
        Card cardPipe = new Card ("Weapon","Pipe",2);
        Card cardStudy = new Card ("Room", "Study",3);
        ArrayList<Card> myCards = new ArrayList<Card>();
            myCards.add(cardMustard);
            myCards.add(cardPipe);
            myCards.add(cardStudy);
        
        
        
        ArrayList p_cards = myCards;
        Player instance = playerTest;
        instance.setCards(p_cards);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCards method, of class Player.
     */
    @Test
    public void testGetCards() {
        System.out.println("getCards");
        
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        
        
        
        //keep cards as null since object<>object
        
        ArrayList expResult = null;
        ArrayList result = instance.getCards();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setMyTurn method, of class Player.
     */
    @Test
    public void testSetMyTurn() {
        System.out.println("setMyTurn");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1,"defaultName", "defaultCharacter");
        
        
        boolean p_myTurn = false;
        Player instance = playerTest;
        instance.setMyTurn(p_myTurn);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getMyTurn method, of class Player.
     */
    @Test
    public void testGetMyTurn() {
        System.out.println("getMyTurn");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        
        Player instance = playerTest;
        boolean expResult = false;
        boolean result = instance.getMyTurn();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCurrentRoom method, of class Player.
     */
    @Test
    public void testSetCurrentRoom() {
        System.out.println("setCurrentRoom");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1,"defaultName", "defaultCharacter");
        Room room1 = new Room("kitchen",0,0);
        
        
        Room p_currentRoom = room1;
        Player instance = playerTest;
        instance.setCurrentRoom(p_currentRoom);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCurrentRoom method, of class Player.
     */
    @Test
    public void testGetCurrentRoom() {
        System.out.println("getCurrentRoom");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        Room room1 = new Room("kitchen",0,0);
        
        
        Player instance = playerTest;
        
        //test for String roomName since object<>object
        String expResult = room1.getCellName();
        
        instance.setCurrentRoom(room1);
        
        String result = instance.getCurrentRoom().getCellName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCaracterColor method, of class Player.
     */
    @Test
    public void testSetCaracterColor() {
        System.out.println("setCaracterColor");
        Color p_charColor = null;
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        instance.setCaracterColor(p_charColor);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCharacterColor method, of class Player.
     */
    @Test
    public void testGetCharacterColor() {
        System.out.println("getCharacterColor");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        instance.setCaracterColor(Color.BLUE);
        
        Color expResult = Color.BLUE;
        Color result = instance.getCharacterColor();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCharacterIcon method, of class Player.
     */
    @Test
    public void testSetCharacterIcon() {
        System.out.println("setCharacterIcon");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        String p_charIcon = "MyIcon";
        Player instance = playerTest;
        instance.setCharacterIcon(p_charIcon);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCharacterIcon method, of class Player.
     */
    @Test
    public void testGetCharacterIcon() {
        System.out.println("getCharacterIcon");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
               
        
        Player instance = playerTest;
        
        instance.setCharacterIcon("myIcon");
                
        String expResult = "myIcon";
        String result = instance.getCharacterIcon();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCharacterName method, of class Player.
     */
    @Test
    public void testSetCharacterName() {
        System.out.println("setCharacterName");
        
      
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        
        String p_charName = "newCharName";
        Player instance = playerTest;
        instance.setCharacterName(p_charName);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCharacterName method, of class Player.
     */
    @Test
    public void testGetCharacterName() {
        System.out.println("getCharacterName");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
            
        
        Player instance = playerTest;
        
        instance.setCharacterName("newCharacterName");
        
        String expResult = "newCharacterName";
        String result = instance.getCharacterName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setTemplate method, of class Player.
     */
    @Test
    public void testSetTemplate() {
        System.out.println("setTemplate");
        
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        Template myTemplate = new Template ();     
        
        Template p_template = myTemplate;
        Player instance = playerTest;
        instance.setTemplate(p_template);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getTemplate method, of class Player.
     */
    @Test
    public void testGetTemplate() {
        System.out.println("getTemplate");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        Template myTemplate = new Template();
        
        Player instance = playerTest;
        
        instance.setTemplate(myTemplate);
        
        Template expResult = myTemplate;
        Template result = instance.getTemplate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setNotebookItemToUpdate method, of class Player.
     */
    @Test
    public void testSetNotebookItemToUpdate() {
        System.out.println("setNotebookItemToUpdate");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        String p_notebookItem = "myNotebookItem";
        Player instance = playerTest;
        instance.setNotebookItemToUpdate(p_notebookItem);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getNotebookItemToUpdate method, of class Player.
     */
    @Test
    public void testGetNotebookItemToUpdate() {
        System.out.println("getNotebookItemToUpdate");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        
        Player instance = playerTest;
        
        instance.setNotebookItemToUpdate("myNotebookItem");
        
        String expResult = "myNotebookItem";
        String result = instance.getNotebookItemToUpdate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setPlayerReady method, of class Player.
     */
    @Test
    public void testSetPlayerReady() {
        System.out.println("setPlayerReady");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        boolean isReady = false;
        Player instance = playerTest;
        instance.setPlayerReady(isReady);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getPlayerReady method, of class Player.
     */
    @Test
    public void testGetPlayerReady() {
        System.out.println("getPlayerReady");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        boolean expResult = false;
        boolean result = instance.getPlayerReady();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setGameMenuPanelID method, of class Player.
     */
    @Test
    public void testSetGameMenuPanelID() {
        System.out.println("setGameMenuPanelID");
        int gameMenuID = 0;
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        instance.setGameMenuPanelID(gameMenuID);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getGameMenuPanelID method, of class Player.
     */
    @Test
    public void testGetGameMenuPanelID() {
        System.out.println("getGameMenuPanelID");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        Player instance = playerTest;
        
        instance.setGameMenuPanelID(1);
        
        int expResult = 1;
        int result = instance.getGameMenuPanelID();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testSetPlayerLoc() {
        System.out.println("setPlayerLoc");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1,"defaultName", "defaultCharacter");
        PlayerLoc location1 = new PlayerLoc(1,2);
        
        PlayerLoc p_loc = location1;
        Player instance = playerTest;
        instance.setPlayerLoc(p_loc);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testGetPlayerLoc() {
        System.out.println("getPlayerLoc");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        //Template myTemplate = new Template ("TemplateABC");
        PlayerLoc location1 = new PlayerLoc(1,2);
        
        Player instance = playerTest;
        
        instance.setPlayerLoc(location1);
        
        PlayerLoc expResult = location1;
        PlayerLoc result = instance.getPlayerLoc();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
    
    /**
     * Test of move method, of class Player.
     */
    @Test
    public void testMove() {
        // 11/28/16: this test is integrated with gameController
        
        System.out.println("move");
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");
        
        int row = 1;
        int col = 2;
        Player instance = playerTest;
        String expResult = "Library" ;
        String result = instance.move(row, col);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of suggest method, of class Player.
     */
    @Test
    public void testSuggest() {
        // 11/28/16: this test is integrated with gameController
        System.out.println("suggest");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "playerName", "playerCharacter");
        Player suspect1 = new Player(g1, "playerNameOfSuspect", "CharacterNameOfSuspect");
      
        
        Player suspect = suspect1;
        String weapon = "Pipe";//note, for this test weapon can be anything.  Method stub returns "Crowbar"
        Player instance = playerTest;
        String expResult ="The card is: MiniGun";
        String result = instance.suggest(suspect, weapon);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of accuse method, of class Player.
     */
    @Test
    public void testAccuse() {
        // 11/28/16: this test is integrated with gameController
        System.out.println("accuse");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");  
        Player suspect1 = new Player(g1, "defaultName", "defaultCharacter");
        Room room1 = new Room("kitchen",0,0);
        
        Player suspect = suspect1;
        String weapon = "pipe";
        Room room = room1;
        Player instance = playerTest;
        String expResult = (instance.getPlayerName() + " Won! Congratulations!");
        String result = instance.accuse(suspect, weapon, room);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of endTurn method, of class Player.
     */
    @Test
    public void testEndTurn() {
        System.out.println("endTurn");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");  
        
        Player instance = playerTest;
        String expResult = ("Player Ended turn, next Player set to:" + "ImNextPlayer");
        
        String result = instance.endTurn();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkMessages method, of class Player.
     */
    @Test
    public void testCheckMessages() {
        System.out.println("checkMessages");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");  
        
        
        String message = "Test Message";
        Player instance = playerTest;
        instance.checkMessages(message);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addAction method, of class Player.
     */
    @Test
    public void testAddAction() {
        System.out.println("addAction");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");  
        
        
        String action = "NewAction";
        Player instance = playerTest;
        instance.addAction(action);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeAction method, of class Player.
     */
    @Test
    public void testRemoveAction() {
        System.out.println("removeAction");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");  
        
        
        String action = "Move";
        Player instance = playerTest;
        instance.removeAction(action);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeAllActions method, of class Player.
     */
    @Test
    public void testRemoveAllActions() {
        System.out.println("removeAllActions");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");  
        
        
        Player instance = playerTest;
        instance.removeAllActions();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of resetActions method, of class Player.
     */
    @Test
    public void testResetActions() {
        System.out.println("resetActions");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");  
        
        
        Player instance = playerTest;
        instance.resetActions();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of pickCard method, of class Player.
     */
    @Test
    public void testPickCard() {
        System.out.println("pickCard");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");  
        Player suspect1 = new Player(g1, "Jose", "Clerk");  
        Room room1 = new Room("kitchen",0,0);
        Card card2 = new Card ("Weapon", "MiniGun", 0);//this is the card, stub.
        
        Player suspect = suspect1;
        String weapon = "pipe";
        Room room = room1;
        Player instance = playerTest;
        
        //test for cardValue Strings since object<>object
        String expResult = card2.getCardValue();
        String result = instance.pickCard(suspect, weapon, room).getCardValue();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of selectTemplate method, of class Player.
     */
    @Test
    public void testSelectTemplate() {
        System.out.println("selectTemplate");
        
        //needed to make a Player
        Map m1 = new Map();
        GameController g1 = new GameController(m1);
       
        Player playerTest = new Player(g1, "defaultName", "defaultCharacter");  
        
        Player instance = playerTest;
        instance.selectTemplate();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
