package CluelessPackage.tests;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import CluelessPackage.CaseFile;
import CluelessPackage.ClientApplication;
import CluelessPackage.Deck;
import CluelessPackage.Player;

public class ChristineIntegrationTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void playerEditsInfo(){
		
		Player tempPlayer = new Player("Christine");
		tempPlayer.setCharacterName("Miss Scarlett");
		tempPlayer.setCaracterColor(Color.magenta);
		tempPlayer.setCharacterIcon("Users/christine/Dropbox/IMG_9711.jpg"); // Is this the correct input parameter?
		
	
		assertEquals("Miss Scarlett", tempPlayer.getCharacterName());
		assertEquals(Color.magenta, tempPlayer.getCharacterColor());
		assertEquals("Users/christine/Dropbox/IMG_9711.jpg", tempPlayer.getCharacterIcon());
		

		tempPlayer.setCaracterColor(Color.magenta);
		tempPlayer.setCharacterIcon("Users/christine/Dropbox/IMG_9711.jpg"); // Is this the correct input parameter?

		// Next, player needs to be able to choose a template. Need template class to implement this.
		
		// Error handling: need to make sure we can handle user input (esp. integers); suggest that we cast int to string 
		
	}
	
	@Test
	public void dealCardsToPlayer(){
	
	ClientApplication clientApp = new ClientApplication();
	
	Player player1 = new Player("Scott");
	Player player2  = new Player("Ryan");
	Player player3 = new Player("James");
	Player player4 = new Player("Christine");
	
	ArrayList<Player> players = new ArrayList<Player>();
	
	int sessionID = 7;
	
	clientApp.setSessionID(sessionID);

	assertEquals(7, clientApp.getSessionID());
	
	// Need a setter for active players in session to test assignment
	
	// Set up the deck
	
	//Make ArrayList<String> of weapons to include
	ArrayList<String> weapons = new ArrayList<String>();
	weapons.add("Knife");
	weapons.add("Rope");
	weapons.add("Pipe");
	
	//Make ArrayList<String> of players to include
	ArrayList<String> playerCards = new ArrayList<String>();
	playerCards.add("Mrs. White");
	playerCards.add("Colonel Mustard");
	playerCards.add("Miss Scarlett");
	
	//Make ArrayList<String> of rooms to include
	ArrayList<String> rooms = new ArrayList<String>();
	rooms.add("Library");
	rooms.add("Ballroom");
	rooms.add("Study");
	rooms.add("Kitchen");
	rooms.add("Dining Room");
	
	
	Deck tempDeck = new Deck();
	tempDeck.initDeckPiles(weapons, playerCards, rooms);			
	for(int i = 0; i < tempDeck.getPlayerCards().size(); i++){
		assertEquals(players.get(i), tempDeck.getPlayerCards().get(i).getCardValue());
	}
	
	// clientApp has no pointer or way to interact with the deck object 
	// Deck should be an attribute of Client App, so that the client app can leverage the deck's shuffle method

	
	CaseFile solution = new CaseFile(tempDeck.dealToCaseFile());
	// The 3 cards that are dealt to the case file should have been removed in the previous method call
	assertEquals(8, tempDeck.getCardsContained().size());
	
	Deck shuffledDeck = tempDeck.shuffle();
	
	// Client app now has the shuffled deck, but there is no 'deal cards to players' method 
	
	
		
		
		
	}

}
