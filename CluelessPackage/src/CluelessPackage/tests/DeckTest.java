/**
 * 
 */

package CluelessPackage.tests;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import CluelessPackage.Card;
import CluelessPackage.Deck;


/**
 * @author christine
 *
 */
public class DeckTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link ch_clueless.Deck#isShuffled()}.
	 */
	@Test
	public void testIsShuffled() {
		
		//Make ArrayList<String> of weapons to include
		ArrayList<String> weapons = new ArrayList<String>();
		weapons.add("Knife");
		weapons.add("Rope");
		weapons.add("Pipe");
		
		//Make ArrayList<String> of players to include
		ArrayList<String> players = new ArrayList<String>();
		players.add("Mrs. White");
		players.add("Colonel Mustard");
		players.add("Miss Scarlett");
		
		//Make ArrayList<String> of rooms to include
		ArrayList<String> rooms = new ArrayList<String>();
		rooms.add("Library");
		rooms.add("Ballroom");
		rooms.add("Study");
		rooms.add("Kitchen");
		rooms.add("Dining Room");
		
		
		Deck tempDeck = new Deck();
		tempDeck.initDeckPiles(weapons, players, rooms);
		
		/*
		 *  Should return FALSE, because the deck has been created but NOT dealt to case file OR shuffled 
		 */
		assertFalse(tempDeck.isShuffled());		
		assertEquals(11, tempDeck.getCardsContained().size());		
		
		// Deal to case file
		tempDeck.dealToCaseFile();
		
		// The 3 cards that are dealt to the case file should have been removed in the previous method call
		assertEquals(8, tempDeck.getCardsContained().size());
		
		// Shuffle
		tempDeck.shuffle();
		
		for(int i = 0; i < tempDeck.getCardsContained().size(); i++){
		//	System.out.println(tempDeck.getCardsContained().get(i).toString());
		}
		
		assertTrue(tempDeck.isShuffled());
	}


	/**
	 * Test method for {@link ch_clueless.Deck#getWeaponCards()}.
	 */
	@Test
	public void testGetWeaponCards() {
		//Make ArrayList<String> of weapons to include
				ArrayList<String> weapons = new ArrayList<String>();
				weapons.add("Knife");
				weapons.add("Rope");
				weapons.add("Pipe");
				
				//Make ArrayList<String> of players to include
				ArrayList<String> players = new ArrayList<String>();
				players.add("Mrs. White");
				players.add("Colonel Mustard");
				players.add("Miss Scarlett");
				
				//Make ArrayList<String> of rooms to include
				ArrayList<String> rooms = new ArrayList<String>();
				rooms.add("Library");
				rooms.add("Ballroom");
				rooms.add("Study");
				rooms.add("Kitchen");
				rooms.add("Dining Room");
				
				
				Deck tempDeck = new Deck();
				tempDeck.initDeckPiles(weapons, players, rooms);			
				for(int i = 0; i < tempDeck.getWeaponCards().size(); i++){
					assertEquals(weapons.get(i), tempDeck.getWeaponCards().get(i).getCardValue());
				}
	}


	/**
	 * Test method for {@link ch_clueless.Deck#getPlayerCards()}.
	 */
	@Test
	public void testGetPlayerCards() {
		//Make ArrayList<String> of weapons to include
		ArrayList<String> weapons = new ArrayList<String>();
		weapons.add("Knife");
		weapons.add("Rope");
		weapons.add("Pipe");
		
		//Make ArrayList<String> of players to include
		ArrayList<String> players = new ArrayList<String>();
		players.add("Mrs. White");
		players.add("Colonel Mustard");
		players.add("Miss Scarlett");
		
		//Make ArrayList<String> of rooms to include
		ArrayList<String> rooms = new ArrayList<String>();
		rooms.add("Library");
		rooms.add("Ballroom");
		rooms.add("Study");
		rooms.add("Kitchen");
		rooms.add("Dining Room");
		
		
		Deck tempDeck = new Deck();
		tempDeck.initDeckPiles(weapons, players, rooms);			
		for(int i = 0; i < tempDeck.getPlayerCards().size(); i++){
			assertEquals(players.get(i), tempDeck.getPlayerCards().get(i).getCardValue());
		}
	}

	/**
	 * Test method for {@link ch_clueless.Deck#getRoomCards()}.
	 */
	@Test
	public void testGetRoomCards() {
		//Make ArrayList<String> of weapons to include
		ArrayList<String> weapons = new ArrayList<String>();
		weapons.add("Knife");
		weapons.add("Rope");
		weapons.add("Pipe");
		
		//Make ArrayList<String> of players to include
		ArrayList<String> players = new ArrayList<String>();
		players.add("Mrs. White");
		players.add("Colonel Mustard");
		players.add("Miss Scarlett");
		
		//Make ArrayList<String> of rooms to include
		ArrayList<String> rooms = new ArrayList<String>();
		rooms.add("Library");
		rooms.add("Ballroom");
		rooms.add("Study");
		rooms.add("Kitchen");
		rooms.add("Dining Room");
		
		
		Deck tempDeck = new Deck();
		tempDeck.initDeckPiles(weapons, players, rooms);			
		for(int i = 0; i < tempDeck.getRoomCards().size(); i++){
			assertEquals(rooms.get(i), tempDeck.getRoomCards().get(i).getCardValue());
		}
	}


	/**
	 * Test method for {@link ch_clueless.Deck#initDeckPiles(java.util.ArrayList, java.util.ArrayList, java.util.ArrayList)}.
	 */
	@Test
	public void testInitDeckPiles() {
		//Make ArrayList<String> of weapons to include
		ArrayList<String> weapons = new ArrayList<String>();
		weapons.add("Knife");
		weapons.add("Rope");
		weapons.add("Pipe");
		
		//Make ArrayList<String> of players to include
		ArrayList<String> players = new ArrayList<String>();
		players.add("Mrs. White");
		players.add("Colonel Mustard");
		players.add("Miss Scarlett");
		
		//Make ArrayList<String> of rooms to include
		ArrayList<String> rooms = new ArrayList<String>();
		rooms.add("Library");
		rooms.add("Ballroom");
		rooms.add("Study");
		rooms.add("Kitchen");
		rooms.add("Dining Room");
		
		
		Deck tempDeck = new Deck();
		tempDeck.initDeckPiles(weapons, players, rooms);
		
		for(int i = 0; i < tempDeck.getRoomCards().size(); i++){
			assertEquals(rooms.get(i), tempDeck.getRoomCards().get(i).getCardValue());
		}
		
		for(int i = 0; i < tempDeck.getPlayerCards().size(); i++){
			assertEquals(players.get(i), tempDeck.getPlayerCards().get(i).getCardValue());
		}
		
		for(int i = 0; i < tempDeck.getWeaponCards().size(); i++){
			assertEquals(weapons.get(i), tempDeck.getWeaponCards().get(i).getCardValue());
		}

		for(int i = 0; i < tempDeck.getCardsContained().size(); i++){
			//System.out.println(tempDeck.getCardsContained().get(i).toString());
			assertEquals(i, tempDeck.getCardsContained().get(i).getPositionInDeck());
		}
	}

	/**
	 * Test method for {@link ch_clueless.Deck#dealToCaseFile()}.
	 */
	@Test
	public void testDealToCaseFile() {
		
		//Make ArrayList<String> of weapons to include
		ArrayList<String> weapons = new ArrayList<String>();
		weapons.add("Knife");
		weapons.add("Rope");
		weapons.add("Pipe");
		
		//Make ArrayList<String> of players to include
		ArrayList<String> players = new ArrayList<String>();
		players.add("Mrs. White");
		players.add("Colonel Mustard");
		players.add("Miss Scarlett");
		
		//Make ArrayList<String> of rooms to include
		ArrayList<String> rooms = new ArrayList<String>();
		rooms.add("Library");
		rooms.add("Ballroom");
		rooms.add("Study");
		rooms.add("Kitchen");
		rooms.add("Dining Room");
		
		
		Deck tempDeck = new Deck();
		tempDeck.initDeckPiles(weapons, players, rooms);
		
		/*
		 *  Should return FALSE, because the deck has been created but NOT dealt to case file OR shuffled 
		 */
		assertFalse(tempDeck.isShuffled());		
		assertEquals(11, tempDeck.getCardsContained().size());		
		
		ArrayList<Card> testSolution = new ArrayList<Card>();
		
		// Deal to case file
		testSolution = tempDeck.dealToCaseFile();
		
		
		// The 3 cards that are dealt to the case file should have been removed in the previous method call
		assertEquals(8, tempDeck.getCardsContained().size());
		
		// Check to make sure solution has ONE card of each type, and EXACTLy 3 cards
		boolean containsOneWeapon = false;
		boolean containsOneRoom = false;
		boolean containsOnePlayer = false;
		boolean outputTest = false;
				
		for(int i = 0; i<testSolution.size(); i++){
			if(testSolution.size() != 3){
				fail("Solution should contain exactly 3 cards.");
			}
			if(testSolution.get(i).getCardType()=="weapon"){
				containsOneWeapon = true;
			}
			else if(testSolution.get(i).getCardType()=="room"){
				containsOneRoom = true;
			}
			else if(testSolution.get(i).getCardType()=="player"){
				containsOnePlayer = true;
			}
		}
		
		if(containsOneRoom & containsOneWeapon & containsOnePlayer){
			outputTest = true;
		}
		
		assertTrue(outputTest);
	}
	

	/**
	 * Test method for {@link ch_clueless.Deck#shuffle()}.
	 */
	@Test
	public void testShuffle() {
		//Make ArrayList<String> of weapons to include
		ArrayList<String> weapons = new ArrayList<String>();
		weapons.add("Knife");
		weapons.add("Rope");
		weapons.add("Pipe");
		
		//Make ArrayList<String> of players to include
		ArrayList<String> players = new ArrayList<String>();
		players.add("Mrs. White");
		players.add("Colonel Mustard");
		players.add("Miss Scarlett");
		
		//Make ArrayList<String> of rooms to include
		ArrayList<String> rooms = new ArrayList<String>();
		rooms.add("Library");
		rooms.add("Ballroom");
		rooms.add("Study");
		rooms.add("Kitchen");
		rooms.add("Dining Room");
		
		
		Deck tempDeck = new Deck();
		tempDeck.initDeckPiles(weapons, players, rooms);
		
		/*
		 *  Should return FALSE, because the deck has been created but NOT dealt to case file OR shuffled 
		 */
		assertFalse(tempDeck.isShuffled());		
		assertEquals(11, tempDeck.getCardsContained().size());		
		
		// Deal to case file
		tempDeck.dealToCaseFile();
		
		// The 3 cards that are dealt to the case file should have been removed in the previous method call
		assertEquals(8, tempDeck.getCardsContained().size());
		
		ArrayList<String> tempCardValues = new ArrayList<String>();
		for(int i = 0; i < tempDeck.getCardsContained().size(); i++){
			tempCardValues.add(tempDeck.getCardsContained().get(i).getCardValue());
		}
			
		// Shuffle
		tempDeck.shuffle();

		assertTrue(tempDeck.isShuffled());
	}

}
