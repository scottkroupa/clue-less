package CluelessPackage.tests;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by scott.kroupa on 11/23/2016.
 */
public class NotebookItemTest {

    @Test
    public void getItem() throws Exception {
        String item = "Knife";
        NotebookItem n = new NotebookItem(item);
        assertEquals("Knife", n.getItem());
    }

    @Test
    public void getStatus() throws Exception {
        boolean status = false;
        NotebookItem n = new NotebookItem("Knife");
        assertEquals(false, n.getStatus());
    }

    @Test
    public void setStatus() throws Exception {
        boolean status = false;
        NotebookItem n = new NotebookItem("Knife");
        n.setStatus();
        assertEquals(true, n.getStatus());
    }

}