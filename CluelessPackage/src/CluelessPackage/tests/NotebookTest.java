package CluelessPackage.tests;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by scott.kroupa on 11/23/2016.
 */
public class NotebookTest {

    @Test
    public void updateNotebookItems() throws Exception {
        final String[] notebookItems = {"Harry Potter", "Ron Weasley", "Snape", "Voldermort"};
        List<NotebookItem> itemStatus;
        itemStatus = new ArrayList<>();
        //for loop to set all to false
        for (int i = 0; i < notebookItems.length; i++)
            itemStatus.add(new NotebookItem(notebookItems[i]));
        NotebookItem n = itemStatus.get(2);
        n.setStatus();
        System.out.println("Ron Weasley should return a status of True");
        System.out.println(n.getStatus());
        assertTrue(n.getStatus());
    }
}