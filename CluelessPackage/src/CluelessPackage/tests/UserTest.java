/**
 * 
 */
package CluelessPackage.tests;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import CluelessPackage.User;


/**
 * @author christine
 *
 */
public class UserTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}


	/**
	 * Test method for {@link ch_clueless.User#getUsername()}.
	 */
	@Test
	public void testGetUsername() {
		User temp = new User("Christine", "1234", 1, "Miss Scarlett", 25);
		assertEquals("Christine", temp.getUsername());
	}
	

	/**
	 * Test method for {@link ch_clueless.User#setUsername(java.lang.String)}.
	 */
	@Test
	public void testSetUsername() {
		User temp = new User("temp", "1234", 1, "Miss Scarlett", 25);
		temp.setUsername("Amir");
		assertEquals("Amir", temp.getUsername());
	}

	/**
	 * Test method for {@link ch_clueless.User#getPassword()}.
	 */
	@Test
	public void testGetPassword() {
		User temp = new User("Christine", "1234", 1, "Miss Scarlett", 25);
		assertEquals("1234", temp.getPassword());
	}

	/**
	 * Test method for {@link ch_clueless.User#setPassword(java.lang.String)}.
	 */
	@Test
	public void testSetPassword() {
		User temp = new User("Christine", "temp", 1, "Miss Scarlett", 25);
		temp.setPassword("1234");
		assertEquals("1234", temp.getPassword());
	}

	/**
	 * Test method for {@link ch_clueless.User#getSessionID()}.
	 */
	@Test
	public void testGetSessionID() {
		User temp = new User("Christine", "temp", 1, "Miss Scarlett", 25);
		assertEquals(1, temp.getSessionID());
	}

	/**
	 * Test method for {@link ch_clueless.User#setSessionID(int)}.
	 */
	@Test
	public void testSetSessionID() {
		User temp = new User("Christine", "temp", 1, "Miss Scarlett", -99);
		temp.setSessionID(25);
		assertEquals(25, temp.getSessionID());
	}

	/**
	 * Test method for {@link ch_clueless.User#getCharName()}.
	 */
	@Test
	public void testGetCharName() {
		User temp = new User("Christine", "temp", 1, "Miss Scarlett", -99);
		assertEquals("Miss Scarlett", temp.getCharName());
	}

	/**
	 * Test method for {@link ch_clueless.User#setCharName(java.lang.String)}.
	 */
	@Test
	public void testSetCharName() {
		User temp = new User("Christine", "temp", 1, "Miss Scarlett", -99);
		temp.setCharName("Mrs. Plum");
		assertEquals("Mrs. Plum", temp.getCharName());
	}

	/**
	 * Test method for {@link ch_clueless.User#getTableKey()}.
	 */
	@Test
	public void testGetTableKey() {
		User temp = new User("Christine", "temp", 1, "Miss Scarlett", 100);
		assertEquals(100, temp.getTableKey());
	}

	/**
	 * Test method for {@link ch_clueless.User#setTableKey(int)}.
	 */
	@Test
	public void testSetTableKey() {
		User temp = new User("Christine", "temp", 1, "Miss Scarlett", 100);
		temp.setTableKey(5);
		assertEquals(5, temp.getTableKey());
	}

}
