/**
 * 
 */
package CluelessPackage.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import CluelessPackage.Card;
import CluelessPackage.CaseFile;

/**
 * @author christine
 *
 */
public class CaseFileTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link ch_clueless.CaseFile#CaseFile(java.util.ArrayList)}.
	 */
	@Test
	public void testCaseFile() {
		Card tempPlayer = new Card("player", "Miss Scarlett", 1);
		Card tempWeapon = new Card("weapon", "knife", 2);
		Card tempRoom = new Card("room", "Ballroom", 3);
		
		ArrayList<Card> tempSolution = new ArrayList<Card>();
		tempSolution.add(tempPlayer);
		tempSolution.add(tempWeapon);
		tempSolution.add(tempRoom);
		
		CaseFile tempFile = new CaseFile(tempSolution);
		assertEquals(tempSolution.toString(), tempFile.getCrime().toString());
	}

	/**
	 * Test method for {@link ch_clueless.CaseFile#getCrime()}.
	 */
	@Test
	public void testGetCrime() {
		Card tempPlayer = new Card("player", "Miss Scarlett", 1);
		Card tempWeapon = new Card("weapon", "knife", 2);
		Card tempRoom = new Card("room", "Ballroom", 3);
		
		ArrayList<Card> tempSolution = new ArrayList<Card>();
		tempSolution.add(tempPlayer);
		tempSolution.add(tempWeapon);
		tempSolution.add(tempRoom);
		
		CaseFile tempFile = new CaseFile(tempSolution);
		assertEquals(tempSolution.toString(), tempFile.getCrime().toString());
		assertTrue(tempFile.getCrime().contains(tempPlayer));
		assertTrue(tempFile.getCrime().contains(tempWeapon));
		assertTrue(tempFile.getCrime().contains(tempRoom));
	}


	/**
	 * Test method for {@link ch_clueless.CaseFile#solutionMatchesAccusation(java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testSolutionMatchesAccusation() {
		Card tempPlayer = new Card("player", "Miss Scarlett", 1);
		Card tempWeapon = new Card("weapon", "Knife", 2);
		Card tempRoom = new Card("room", "Ballroom", 3);
		
		ArrayList<Card> tempSolution = new ArrayList<Card>();
		tempSolution.add(tempPlayer);
		tempSolution.add(tempWeapon);
		tempSolution.add(tempRoom);
		
		CaseFile crimeSol = new CaseFile(tempSolution);
		
		String correctPlayer = "miss scarlett";
		String correctWeapon = "knife";
		String correctRoom = "ballroom";
		
		assertTrue(crimeSol.solutionMatchesAccusation(correctWeapon, correctPlayer, correctRoom));
		
		String falsePlayer = "Colonel Mustard";
		String falseWeapon = "rope";
		String falseRoom = "Kitchen";
		
		assertFalse(crimeSol.solutionMatchesAccusation(falseWeapon, falsePlayer, falseRoom));
		assertFalse(crimeSol.solutionMatchesAccusation(correctWeapon, falsePlayer, correctRoom));
		assertFalse(crimeSol.solutionMatchesAccusation(falseWeapon, correctPlayer, correctRoom));
		assertFalse(crimeSol.solutionMatchesAccusation(correctWeapon, correctPlayer, falseRoom));
	}

}
