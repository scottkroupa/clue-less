package CluelessPackage.tests;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import CluelessPackage.HomePanel;
import static org.junit.Assert.*;

/**
 * Created by scott.kroupa on 11/23/2016.
 */
public class HomePanelTest {

    @Test
    public void getHostname() throws Exception {
        String hostname = "Test Hostname";
        HomePanel n = new HomePanel("username", 43, hostname, "Password");
        assertEquals(n.getHostname(), "Test Hostname");
    }

    @Test
    public void getPortNum() throws Exception {
        int portNum = 23;
        HomePanel h = new HomePanel("username", portNum, "hostname", "password");
        assertEquals(23,h.getPortNum());
    }

    @Test
    public void getUsername() throws Exception {
        String username = "Test Username";
        HomePanel h = new HomePanel(username, 23, "hostname", "password");
        assertEquals("Test Username",h.getUsername());
    }

    @Test
    public void setUsername() throws Exception {
        String username = "Username";
        HomePanel n = new HomePanel(username, 43, "hostname", "Password");
        n.setUsername("Test Username");
        assertEquals("Test Username",n.getUsername());
    }

    @Test
    public void setPortNum() throws Exception {
        int portNum = 23;
        HomePanel n = new HomePanel("username", portNum, "hostname", "Password");
        n.setPortNum(443);
        assertEquals(443, n.getPortNum());
    }

    @Test
    public void setHostname() throws Exception {
        String hostname = "Hostname";
        HomePanel n = new HomePanel("username", 43, hostname, "Password");
        n.setHostname("Test Host Name");
        assertEquals("Test Host Name",n.getHostname());
    }

    @Test
    public void connect() throws Exception {
        //TBD
    }

}