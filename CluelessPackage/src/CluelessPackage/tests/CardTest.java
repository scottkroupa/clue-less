/**
 * 
 */

package CluelessPackage.tests;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import CluelessPackage.Card;
import CluelessPackage.CardHolder;
import CluelessPackage.Player;


/**
 * @author christine
 *
 */
public class CardTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link ch_clueless.Card#Card(java.lang.String, java.lang.String, int)}.
	 * Only a constructor, doesn't need a unit test.
	 */
	@Test
	public void testCard() {

	}

	/**
	 * Test method for {@link ch_clueless.Card#getCardType()}.
	 */
	@Test
	public void testGetCardType() {
		String type = "Weapon";
		String val = "Knife";
		int pos = 3;

        Card tempCard = new Card(type, val, pos);       
        assertEquals("Weapon", tempCard.getCardType());
	}

	/**
	 * Test method for {@link ch_clueless.Card#setCardType(java.lang.String)}.
	 */
	@Test
	public void testSetCardType() {
		String type = "temp";
		String val = "Knife";
		int pos = 3;

        Card tempCard = new Card(type, val, pos);  
        tempCard.setCardType("Weapon");
        assertEquals("Weapon", tempCard.getCardType());
	}

	/**
	 * Test method for {@link ch_clueless.Card#getOwner()}.
	 */
	@Test
	public void testGetOwner() {
		String type = "temp";
		String val = "Knife";
		int pos = 3;

        Card tempCard = new Card(type, val, pos);  
              
        // These should both work, since Player and CaseFile extend CardHolder
        CardHolder tempOwner = new CardHolder();
        Player tempPlayer = new Player("Miss Scarlett");

        tempCard.setOwner(tempOwner);       
        tempCard.setOwner(tempPlayer);
        
        assertEquals(tempPlayer, tempCard.getOwner());
	}

	/**
	 * Test method for {@link ch_clueless.Card#setOwner(ch_clueless.CardHolder)}.
	 */
	@Test
	public void testSetOwner() {
		String type = "temp";
		String val = "Knife";
		int pos = 3;

        Card tempCard = new Card(type, val, pos);  
              
        // These should both work, since Player and CaseFile extend CardHolder
        CardHolder tempOwner = new CardHolder();
        Player tempPlayer = new Player("Mrs. Plum");

        tempCard.setOwner(tempOwner);       
        tempCard.setOwner(tempPlayer);
        
        assertEquals(tempPlayer, tempCard.getOwner());
	}

	/**
	 * Test method for {@link ch_clueless.Card#getCardValue()}.
	 */
	@Test
	public void testGetCardValue() {
		String type = "temp";
		String val = "Knife";
		int pos = 3;

        Card tempCard = new Card(type, val, pos);  
       
        assertEquals(val, tempCard.getCardValue());
	}

	/**
	 * Test method for {@link ch_clueless.Card#setCardValue(java.lang.String)}.
	 */
	@Test
	public void testSetCardValue() {
		String type = "temp";
		String val = "temp";
		int pos = 3;

        Card tempCard = new Card(type, val, pos);  
        
        tempCard.setCardValue("Rope");
        assertEquals("Rope", tempCard.getCardValue());
	}

	/**
	 * Test method for {@link ch_clueless.Card#getPositionInDeck()}.
	 */
	@Test
	public void testGetPositionInDeck() {
		String type = "temp";
		String val = "temp";
		int pos = 3;

        Card tempCard = new Card(type, val, pos);  
        assertEquals(pos, tempCard.getPositionInDeck());
	}

	/**
	 * Test method for {@link ch_clueless.Card#setPositionInDeck(int)}.
	 * NOTE: may need to add tests here to check for int > 0 AND int <= deck size. 
	 * We will control this code on the back end though, so there is no room for user input-related error. 
	 */
	@Test
	public void testSetPositionInDeck() {
		String type = "temp";
		String val = "temp";
		int pos = -99;

        Card tempCard = new Card(type, val, pos); 
        tempCard.setPositionInDeck(5);
        assertEquals(5, tempCard.getPositionInDeck());
	}

	/**
	 * Test method for {@link ch_clueless.Card#toString()}.
	 */
	@Test
	public void testToString() {
		String type = "Person";
		String val = "Mrs. Plum";
		int pos = 3;

        Card tempCard = new Card(type, val, pos); 
        Player tempPlayer = new Player("Miss Scarlett");
        tempCard.setOwner(tempPlayer);

        assertEquals("Card [cardType=" + type + ", owner=" + tempPlayer
				+ ", cardValue=" + val + ", positionInDeck="
				+ pos + "]", tempCard.toString());
	}

}
