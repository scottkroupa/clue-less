/*
 *  The GUIHall class 
 *	@author: Ryan Bonisch
 *	last updated: 12/4/16
 *
 *	The GUIHall class is used ...
 */

package CluelessPackage;

/**
 * Import all appropriate libraries
 */
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import java.awt.Image;

/**
 * 
 */
public class GUIHall extends JPanel {
    
    // Declare global variables
    private final String name;
    private final int x;
    private int y;
    private final int width;
    private final int height;
    private boolean selected;
    private boolean possibleLocation;
    private boolean myTurn;
    private boolean vertical;
    private Image character1;
    private Image character2;
    private final int row;
    private final int col;
    
    /**
     * @method The GUIHall constructor method which takes in the name of the 
     * hallway and a custom class called a four point which is just x, y, width
     * and height
     * @param name
     * @param fp 
     * @param col
     * @param row
     */
    public GUIHall(String name, FourPoint fp, int row, int col){
        this.name = name;
        x = fp.getX();
        y = fp.getY();
        width = fp.getWidth();
        height = fp.getHeight();
        selected = false;
        character1 = null;
        character2 = null;
        this.row = row;
        this.col = col;
        myTurn = false;
        possibleLocation = false;
        if(x == 58 && y == -1 && width == 20 && height == 65) { // vertical
            vertical = true;
        } else { // horizontal
            vertical = false;
        }
        if(row == 0) {
            y = 40;
        } else if (row == 4) {
            y = 10;
        }
    }
    
    /**
     * @method The paintComponent method is an overriden method from the parent
     * JPanel object.  It will allow us to paint the hallway cells
     * @param g 
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        if (possibleLocation && myTurn) {
            g.setColor(new Color(0, 96, 64)); // Dark green
            for (int i = 1; i <= 5; i++) {
                g.drawRect(x+i, y+i, width-(i*2), height-(i*2));
            }
        } else if (!possibleLocation && myTurn) {
            g.setColor(new Color(161, 0, 0)); // Blood red
            for (int i = 1; i <= 5; i++) {
                g.drawRect(x+i, y+i, width-(i*2), height-(i*2));
            }
            g.drawLine(x, y, (x+width), (y+height));
            g.drawLine(x+1, y, (x+width), (y+(height-1)));
            g.drawLine(x-1, y+1, (x+(width-1)), (y+height));
            if(vertical) {
                g.drawLine(x, height, (x+width), y);
                g.drawLine(x, height-1, (x+(width-1)), y);
                g.drawLine(x+1, height, (x+width), y+1);
            } else {
                g.drawLine(x, (y+height), width, y);
                g.drawLine(x, (y+(height-1)), width-1, y);
                g.drawLine(x+1, (y+height), width, y+1);
            }
        }
        
        // if the cell is selected, then it will fill with green
        if(selected && myTurn && possibleLocation) {
            g.setColor(Color.green);
            g.fillRect(x, y, width, height);
        }else { // else it will just paint a square with black border
            g.setColor(Color.BLACK);
            g.drawRect(x, y, width, height);
        }
        
        //***************************
        //redo conditional, after the character is moved, the starting square disapears
        //***************************
        if (character1 != null) {
            g.setColor(Color.BLACK);
            if (row == 0 && col == 3) { // miss scarlet's starting square
                g.drawRect(3, 12, 20, 20);
                g.drawImage(character1, 5, 0, 25, 28, 0, 0, 40, 57, this);
            } else if (row == 1 && col == 0) { // Professor Plum's staring square
                g.drawRect(3, 12, 20, 20);
                g.drawImage(character1, 5, 0, 25, 28, 0, 0, 40, 57, this);
            } else if (row == 1 && col == 4) { // colonel mustard's starting square
                g.drawRect(105, 12, 20, 20);
                g.drawImage(character1, 107, 0, 127, 28, 0, 0, 40, 57, this);
            } else if (row == 3 && col == 0) { // Mrs. Peacock's starting square
                g.drawRect(3, 12, 20, 20);
                g.drawImage(character1, 5, 0, 25, 28, 0, 0, 40, 57, this);
            } else if (row == 4 && col == 1) { // Mr. Green's starting square
                g.drawRect(8, 43, 20, 20);
                g.drawImage(character1, 10, 32, 30, 60, 0, 0, 40, 57, this);
            } else if (row == 4 && col == 3) { // Mrs. White's starting square
                g.drawRect(8, 43, 20, 20);
                g.drawImage(character1, 10, 32, 30, 60, 0, 0, 40, 57, this);
            }
        }
        // if a character is in a hallway, then draw their game piece and scale
        // scale it to about 50%
        if(character2 != null) {
            if(vertical) { // vertical
                g.drawImage(character2, 60, 20, 80, 48, 0, 0, 40, 57, this); // scale to 20 x 28
            } else { // horizontal
                g.drawImage(character2, 55, y-10, 75, y+18, 0, 0, 40, 57, this); // scale to 20 x 28
            }
        }
    }
    
    /**
     * @method Returns the name of the Hallway
     * @return 
     */
    @Override
    public String getName() {
        return name;
    }
    
    /**
     * @method The setSelected method is used when the hallway needs to be selected by
     * the player
     * @param selected
     */
    public void setSelected(boolean selected) {
        if(myTurn) {
            this.selected = selected;
            repaint();
        }
    }
    
    /**
     * @method This method will toggle between selected and not selected
     */
    public void toggle() {
        if(myTurn) {
            this.selected = !selected;
            repaint();
        }
    }
    
    /**
     * @method This getter returns if the object is selected
     * @return 
     */
    public boolean isSelected() {
        return selected;
    }
    
    /**
     * @method The setCharacterImage method will set the image of whomever is in
     * the hallway
     * @param image 
     */
    public void setCharacter1Image(Image image) {
        character1 = image;
        repaint();
    }
    
    /**
     * 
     * @param image 
     */
    public void setCharacter2Image(Image image) {
        character2 = image;
        repaint();
    }
    
    /**
     * @method Returns the row number
     * @return 
     */
    public int getRow() {
        return row;
    }
    
    /**
     * @method Returns the column number
     * @return 
     */
    public int getCol() {
        return col;
    }
    
    /**
     * @method This setter method sets the myTurn variable to have whatever 
     *  value is passed to it.
     * @param myTurn 
     */
    public void setMyTurn(boolean myTurn) {
        this.myTurn = myTurn;
        repaint();
    }
    
    /**
     * @method This setter method sets the possibleLocation variable to have
     *  whatever value is passed to it.
     * @param possibleLocation 
     */
    public void setPossibleLocation(boolean possibleLocation) {
        this.possibleLocation = possibleLocation;
        repaint();
    }
    
    /**
     * @method This method returns if this hallway is a possible location
     * @return 
     */
    public boolean isPossibleLocation() {
        return possibleLocation;
    }
}
