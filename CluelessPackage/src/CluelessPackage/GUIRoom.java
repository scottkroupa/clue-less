/*
 *  The GUIRoom class 
 *	@author: Ryan Bonisch
 *	last updated: 12/1/16
 *
 *	
 */

package CluelessPackage;

/**
 * Import all necessary libraries
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 
 */
public class GUIRoom extends JPanel {
    
    // Declare global variables
    private final String name;
    private final int col;
    private final int row;
    private final int x;
    private final int y;
    private final int width;
    private final int height;
    private boolean selected;
    private boolean possibleLocation;
    private boolean myTurn;
    private Image character1;
    private Image character2;
    private Image character3;
    private Image character4;
    private Image character5;
    private Image character6;
    private final ArrayList<Image> characters = new ArrayList();
    private Image weapon;
    private final int corner;
    
    /**
     * 
     * @param name
     * @param fp
     * @param corner 
     * @param row
     * @param col
     */
    public GUIRoom(String name, FourPoint fp, int corner, int row, int col){
        this.name = name;
        x = fp.getX();
        y = fp.getY();
        width = fp.getWidth();
        height = fp.getHeight();
        selected = false;
        JLabel roomName = new JLabel(name);
        this.add(roomName);
        character1 = null;
        character2 = null;
        character3 = null;
        character4 = null;
        character5 = null;
        character6 = null;
        weapon = null;
        this.corner = corner;
        this.row = row;
        this.col = col;
        this.possibleLocation = false;
        this.myTurn = false;
        characters.add(character1);
        characters.add(character2);
        characters.add(character3);
        characters.add(character4);
        characters.add(character5);
        characters.add(character6);
    }
    
    /**
     * 
     * @param g 
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        if (possibleLocation && myTurn) {
            g.setColor(new Color(0, 96, 64));
            for (int i = 1; i <= 5; i++) {
                g.drawRect(x+i, y+i, width-(i*2), height-(i*2));
            }
        } else if (!possibleLocation && myTurn) {
            g.setColor(new Color(161, 0, 0));
            for (int i = 1; i <= 5; i++) {
                g.drawRect(x+i, y+i, width-(i*2), height-(i*2));
            }
            g.drawLine(x, y+1, width-1, height);
            g.drawLine(x, y, width, height);
            g.drawLine(x+1, y, width, height-1);
            g.drawLine(x+1, height, width, y+1);
            g.drawLine(x, height, width, y);
            g.drawLine(x, height-1, width-1, y);
        }
        
        if(selected && myTurn && possibleLocation) {
            g.setColor(Color.green);
            g.fillRect(x, y, width, height);
        }else {
            g.setColor(Color.BLACK);
            g.drawRect(x, y, width, height);
        }
        
        g.setColor(Color.black);
        
        if (corner == 1) {
            int nx = 0;
            int ny = 0;
            int nwidth = 15;
            int nheight = 15;
            g.fillRect(nx, ny, nwidth, nheight);
            g.setColor(Color.WHITE);
            nx = 4;
            ny = 4;
            nwidth = nwidth -4;
            nheight = nheight -4;
            g.drawLine(nx, ny, nwidth, nheight);
            g.drawLine(nx, ny, nwidth, ny);
            g.drawLine(nx, ny, nx, nheight);
        } else if (corner == 2) {
            int nx = width - 15;
            int ny = 0;
            int nwidth = 15;
            int nheight = 15;
            g.fillRect(nx, ny, nwidth, nheight);
            g.setColor(Color.WHITE);
            nx = nx + 4;
            ny = 4;
            nwidth = width - 4;
            nheight = nheight - 4;
            g.drawLine(nwidth, ny, nx, nheight);
            g.drawLine(nwidth, ny, nx, ny);
            g.drawLine(nwidth, ny, nwidth, nheight);
        } else if (corner == 3) {
            int nx = width - 15;
            int ny = height - 15;
            int nwidth = 15;
            int nheight = 15;
            g.fillRect(nx, ny, nwidth, nheight);
            g.setColor(Color.WHITE);
            nx = nx + 4;
            ny = ny + 4;
            nwidth = width - 4;
            nheight = height - 4;
            g.drawLine(nx, ny, nwidth, nheight);
            g.drawLine(nx, nheight, nwidth, nheight);
            g.drawLine(nwidth, ny, nwidth, nheight);
        } else if (corner == 4) {
            int nx = 0;
            int ny = height - 15;
            int nwidth = 15;
            int nheight = 15;
            g.fillRect(nx, ny, nwidth, nheight);
            g.setColor(Color.WHITE);
            nx = 4;
            ny = height - 4;
            nwidth = 11;
            nheight = height - 11;
            g.drawLine(nx, ny, nwidth, nheight);
            g.drawLine(nx, ny, nwidth, ny);
            g.drawLine(nx, ny, nx, nheight);
        }
        
        if(character1 != null) {
            g.drawImage(character1, 17,20,37,48, 0,0,40,57, this); // scale to 20 x 28
        }
        if(character2 != null) {
            g.drawImage(character2, 32,35,52,63, 0,0,40,57, this); // scale to 20 x 28
        }
        if(character3 != null) {
            g.drawImage(character3, 47,20,67,48, 0,0,40,57, this); // scale to 20 x 28
        }
        if(character4 != null) {
            g.drawImage(character4, 62,35,82,63, 0,0,40,57, this); // scale to 20 x 28
        }
        if(character5 != null) {
            g.drawImage(character5, 77,20,97,48, 0,0,40,57, this); // scale to 20 x 28
        }
        if(character6 != null) {
            g.drawImage(character6, 92,35,112,63, 0,0,40,57, this); // scale to 20 x 28
        }
    }
    
    /**
     * @method This getter returns the objects name
     * @return 
     */
    @Override
    public String getName() {
        return name;
    }
    
    /**
     * @method 
     * @param selected
     */
    public void setSelected(boolean selected) {
        if(myTurn) {
            this.selected = selected;
            repaint();
        }
    }
    
    /**
     * @method This method will toggle between selected and not selected
     */
    public void toggle() {
        if(myTurn) {
            this.selected = !selected;
            repaint();
        }
    }
    
    /**
     * @method This getter returns if the object is selected
     * @return 
     */
    public boolean isSelected() {
        return selected;
    }
    
    /**
     * 
     * @param image 
     */
    public void setCharacter1(Image image) {
        character1 = image;
        repaint();
    }
    
    /**
     * 
     * @return 
     */
    public Image getCharacter1() {
        return character1;
    }
    
    /**
     * 
     * @param image 
     */
    public void setCharacter2(Image image) {
        character2 = image;
        repaint();
    }
    
    /**
     * 
     * @return 
     */
    public Image getCharacter2(){
        return character2;
    }
    
    /**
     * 
     * @param image 
     */
    public void setCharacter3(Image image) {
        character3 = image;
        repaint();
    }
    
    /**
     * 
     * @return 
     */
    public Image getCharacter3(){
        return character3;
    }
    
    /**
     * 
     * @param image 
     */
    public void setCharacter4(Image image) {
        character4 = image;
        repaint();
    }
    
    /**
     * 
     * @return 
     */
    public Image getCharacter4()  {
        return character4;
    }
    
    /**
     * 
     * @param image 
     */
    public void setCharacter5(Image image) {
        character5 = image;
        repaint();
    }
    
    /**
     * 
     * @return 
     */
    public Image getCharacter5() {
        return character5;
    }
    
    /**
     * 
     * @param image 
     */
    public void setCharacter6(Image image) {
        character6 = image;
        repaint();
    }
    
    /**
     * 
     * @return 
     */
    public Image getCharacter6() {
        return character6;
    }
    
    /**
     * 
     * @return 
     */
    public ArrayList<Image> getCharacters() {
        return characters;
    }
    
    /**
     * 
     * @param index
     * @param image 
     */
    public void setCharacter(int index, Image image) {
        if(index == 0) {
            character1 = image;
        } else if(index == 1) {
            character2 = image;
        } else if(index == 2) {
            character3 = image;
        } else if(index == 3) {
            character4 = image;
        } else if(index == 4) {
            character5 = image;
        } else if(index == 5) {
            character6 = image;
        }
        repaint();
    }
    
    /**
     * 
     * @param image 
     */
    public void setWeapon(Image image) {
        weapon = image;
        repaint();
    }
    
    /**
     * @method Returns the row number
     * @return 
     */
    public int getRow() {
        return row;
    }
    
    /**
     * @method Returns the column number
     * @return 
     */
    public int getCol() {
        return col;
    }
    
    /**
     * @method The setter method for possibleLocation
     * @param possible 
     */
    public void setPossibleLocation(boolean possible) {
        this.possibleLocation = possible;
        repaint();
    }
    
    /**
     * @method This method returns if this is a possible move location
     * @return 
     */
    public boolean isPossibleLocation() {
        return possibleLocation;
    }
    
    /**
     * @method The setter method for myTurn
     * @param myTurn 
     */
    public void setMyTurn(boolean myTurn) {
        this.myTurn = myTurn;
        repaint();
    }
}
