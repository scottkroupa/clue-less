package CluelessPackage;
import java.util.ArrayList;
import java.util.List;

public final class Map {

    private final String STUDY = "Study";
    private final String LIBRARY = "Library";
    private final String BILLIARDROOM = "Billiard Room";
    private final String CONSERVATORY = "Conservatory";
    private final String BALLROOM = "Ballroom";
    private final String KITCHEN = "Kitchen";
    private final String DININGROOM = "Dining Room";
    private final String HALL = "Hall";
    private final String LOUNGE = "Lounge";
    private final String HALL1 = "Hall 1";
    private final String HALL2 = "Hall 2";
    private final String HALL3 = "Hall 3";
    private final String HALL4 = "Hall 4";
    private final String HALL5 = "Hall 5";
    private final String HALL6 = "Hall 6";
    private final String HALL7 = "Hall 7";
    private final String HALL8 = "Hall 8";
    private final String HALL9 = "Hall 9";
    private final String HALL10 = "Hall 10";
    private final String HALL11 = "Hall 11";
    private final String HALL12 = "Hall 12";
    private final String MS = "Miss Scarlet";
    private final String CM = "Col. Mustard";
    private final String PP = "Professor Plum";
    private final String MP = "Mrs. Peacock";
    private final String MG = "Mr. Green";
    private final String MW = "Mrs. White";
    private ArrayList<Player> players;
    private List<MapCell> location = new ArrayList<>();
    //needs reference to the PlayerLoc class

    //Constructor
    //Changed 08Dec16
    //******************************************************
    public Map() {//ArrayList<Player> players){
    //******************************************************
        location.add(new Room(STUDY,0,0));
        location.add(new Room(LIBRARY,0,2));
        location.add(new Room(BILLIARDROOM,2,2));
        location.add(new Room(CONSERVATORY,0,4));
        location.add(new Room (BALLROOM,2,4));
        location.add(new Room (KITCHEN,4,4));
        location.add(new Room(DININGROOM,4,2));
        location.add(new Room (HALL,2,0));
        location.add(new Room(LOUNGE,4,0));
        //add occupancy number
        location.add(new Hall(HALL1, 1,0, false));
        location.add(new Hall(HALL2, 3,0, false));
        location.add(new Hall(HALL3, 0,1, false));
        location.add(new Hall(HALL4, 2,1, false));
        location.add(new Hall(HALL5, 4,1, false));
        location.add(new Hall(HALL6, 1,2, false));
        location.add(new Hall(HALL7, 3,2, false));
        location.add(new Hall(HALL8, 0,3, false));
        location.add(new Hall(HALL9, 2,3, false));
        location.add(new Hall(HALL10, 4,3, false));
        location.add(new Hall(HALL11, 1,4, false));
        location.add(new Hall(HALL12, 3,4, false));
        location.add(new Door(CM));
        location.add(new Door(MS));
        location.add(new Door(PP));
        location.add(new Door(MP));
        location.add(new Door(MG));
        location.add(new Door(MW));
        association();
        //Changed 08Dec16
        //*******************************************************
        //this.players = players;
        //setStartingPoint();
        //*******************************************************

        //add in a class for starting points or count starting points in hall
        //startingPoints.add(new StartingPoint());
    }

    public void association(){
        for(MapCell cell: location){
            switch(cell.getCellName()){
                case STUDY:
                    cell.addPossibleMoves(find(HALL1));
                    cell.addPossibleMoves(find(HALL3));
                    cell.addPossibleMoves(find(KITCHEN));
                    break;
                case LIBRARY:
                    cell.addPossibleMoves(find(HALL3));
                    cell.addPossibleMoves(find(HALL6));
                    cell.addPossibleMoves(find(HALL8));
                    break;
                case CONSERVATORY:
                    cell.addPossibleMoves(find(HALL8));
                    cell.addPossibleMoves(find(HALL11));
                    cell.addPossibleMoves(find(LOUNGE));
                    break;
                case HALL:
                    cell.addPossibleMoves(find(HALL1));
                    cell.addPossibleMoves(find(HALL2));
                    cell.addPossibleMoves(find(HALL4));
                    break;
                case BILLIARDROOM:
                    cell.addPossibleMoves(find(HALL4));
                    cell.addPossibleMoves(find(HALL6));
                    cell.addPossibleMoves(find(HALL7));
                    cell.addPossibleMoves(find(HALL9));
                    break;
                case BALLROOM:
                    cell.addPossibleMoves(find(HALL9));
                    cell.addPossibleMoves(find(HALL11));
                    cell.addPossibleMoves(find(HALL12));
                    break;
        //Added 08Dec16
        //**********************************************************************
                case LOUNGE:
                    cell.addPossibleMoves(find(HALL2));
                    cell.addPossibleMoves(find(HALL5));
                    cell.addPossibleMoves(find(CONSERVATORY));
                    break;
                case DININGROOM:
                    cell.addPossibleMoves(find(HALL5));
                    cell.addPossibleMoves(find(HALL7));
                    cell.addPossibleMoves(find(HALL10));
                    break;
        //**********************************************************************
                case KITCHEN:
                    cell.addPossibleMoves(find(HALL10));
                    cell.addPossibleMoves(find(HALL12));
                    cell.addPossibleMoves(find(STUDY));
                    break;
                case HALL1:
                    cell.addPossibleMoves(find(STUDY));
                    cell.addPossibleMoves(find(HALL));
                    break;
                case HALL2:
                    cell.addPossibleMoves(find(HALL));
                    cell.addPossibleMoves(find(LOUNGE));
                    break;
                case HALL3:
                    cell.addPossibleMoves(find(STUDY));
                    cell.addPossibleMoves(find(LIBRARY));
                    break;
                case HALL4:
                    cell.addPossibleMoves(find(HALL));
                    cell.addPossibleMoves(find(BILLIARDROOM));
                    break;
                case HALL5:
                    cell.addPossibleMoves(find(LOUNGE));
                    cell.addPossibleMoves(find(DININGROOM));
                    break;
                case HALL6:
                    cell.addPossibleMoves(find(LIBRARY));
                    cell.addPossibleMoves(find(BILLIARDROOM));
                    break;
                case HALL7:
                    cell.addPossibleMoves(find(BILLIARDROOM));
                    cell.addPossibleMoves(find(DININGROOM));
                    break;
                case HALL8:
                    cell.addPossibleMoves(find(LIBRARY));
                    cell.addPossibleMoves(find(CONSERVATORY));
                    break;
                case HALL9:
                    cell.addPossibleMoves(find(BILLIARDROOM));
                    cell.addPossibleMoves(find(BALLROOM));
                    break;
                case HALL10:
                    cell.addPossibleMoves(find(DININGROOM));
                    cell.addPossibleMoves(find(KITCHEN));
                    break;
                case HALL11:
                    cell.addPossibleMoves(find(CONSERVATORY));
                    cell.addPossibleMoves(find(BALLROOM));
                    break;
                case HALL12:
                    cell.addPossibleMoves(find(BALLROOM));
                    cell.addPossibleMoves(find(KITCHEN));
                    break;
                case MS:
                    cell.addDoor(find(HALL2));
                    break;
                case PP:
                    cell.addDoor(find(HALL3));
                    break;
                case CM:
                    cell.addDoor(find(HALL5));
                    break;
                case MP:
                    cell.addDoor(find(HALL8));
                    break;
                case MG:
                    cell.addDoor(find(HALL11));
                    break;
                case MW:
                    cell.addDoor(find(HALL12));
                    break;
            }
        }
    }

    public void setStartingPoint(){

        for(int i = 0; i < players.size(); i++) {
            if(players.get(i).getCharacterName().matches(CM)|| players.get(i).getCharacterName().matches("Ms. Eden"))
                players.get(i).setCurrentRoom(location.get(21));
            else if(players.get(i).getCharacterName().matches(MS) || players.get(i).getCharacterName().matches("Miss Rosalie"))
                players.get(i).setCurrentRoom(location.get(22));
            else if(players.get(i).getCharacterName().matches(PP) || players.get(i).getCharacterName().matches("Professor Edmund"))
                players.get(i).setCurrentRoom(location.get(23));
            else if(players.get(i).getCharacterName().matches(MP) || players.get(i).getCharacterName().matches("Dr. Violet"))
                players.get(i).setCurrentRoom(location.get(24));
            else if(players.get(i).getCharacterName().matches(MG) || players.get(i).getCharacterName().matches("Mr. Maynard"))
                players.get(i).setCurrentRoom(location.get(25));
            else if(players.get(i).getCharacterName().matches(MW) || players.get(i).getCharacterName().matches("Mrs. Beatrix"))
                players.get(i).setCurrentRoom(location.get(26));
        }

    }

    public MapCell find(String name){
        MapCell cell = null;
        for(MapCell temp: location){
            if(temp.getCellName().equals(name)){
                cell = temp;
                break;
            }
        }
        return cell;
    }

    public MapCell find(int col, int row){
        MapCell cell = null;
        for(MapCell temp: location){
            if(temp.getCol()==col&&temp.getRow()==row){
                cell = temp;
                break;
            }
        }
        return cell;
    }

    public int addPlayer(Player player){
        players.add(player);
        return players.size();
    }
    
    //Added 08Dec16
    //**********************************************************
    /**
     * 
     * @param players 
     */
    public void setPlayerList(ArrayList<Player> players) {
        this.players = players;
    }
    //***********************************************************

    public void setHallOccupied(String name, boolean occupied) {
        MapCell mc = find(name);
        
    }
    
    public MapCell movePlayer(Player player, int col, int row){
        //int c;
        //int r;
        MapCell playerl = null;
        //c= player.getPlayerLoc().getCol();
        //r = player.getPlayerLoc().getRow();
        //MapCell mc = find(c,r);
        MapCell mc = find(player.getCurrentRoom().getCellName());
        System.out.println("mc.getCellName() = " + mc.getCellName());
        List <MapCell> l = mc.getPossibleMoves();
        for(MapCell cell: l) {
            if (cell.getRow() == row && cell.getCol() == col) {
                if ((!cell.isRoom() && !((Hall) cell).isOccupied()) || cell.isRoom()){
                    player.setPlayerLoc(new PlayerLoc(col,row));
                    playerl = cell;
                    break;
                }
            }
        }

        System.out.println("playerl = " + playerl);
        return playerl;
    }

    public MapCell transportPlayer(Player player, int col, int row){
        player.setPlayerLoc(new PlayerLoc(col,row));
        return player.getCurrentRoom();

    }
    /*
	public Room updateLoc(Player player, int row, int col){
		if(checkValidMove(player, row, col).equals(true))
			setPlayerLoc(player, row, col);
		return
	}
	private boolean checkValidMove(Player player, int row, int col){
		//insert logic here
		//one person per hall but any number of people per room
	}
	private void setPlayerLoc(Player player, int row, int col){
		//insert logic here
	}
	*/

}