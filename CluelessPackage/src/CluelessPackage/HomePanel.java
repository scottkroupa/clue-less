/**
 * Contributors: Ryan D Bonisch
 * Course: EN.605.401 Foundations of Software Engineering
 * Start: 04-Oct-16 11:00
 * End: 
 */
package CluelessPackage;

/**
 * Import necessary Libraries
 */
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;
import java.net.Socket;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.input.KeyCode;

/**
 * 
 * 
 */
public class HomePanel extends JPanel {
    
    //
    private ArrayList<ClientApplication> listeners = new ArrayList();
    
    //
    private Socket socket;
    
    //
    private DataInputStream fromServer;
    private DataOutputStream toServer;
    
    //
    private JTextField jtfServerIP;
    
    //
    private JTextField jtfServerPort;
    
    //
    private JTextField jtfUsername;
    
    //
    private JPasswordField jpfPassword;
    
    //
    private JLabel status;
    
    //
    private String host = "159.203.106.76";
    
    //
    private int serverPort = 8000;
    
    //
    private int playerNum = 0;
    
    private String detectiveLevel;
    
    public HomePanel(Frame frame) {
        
        //
        GridLayout gridLayout1 = new GridLayout(6, 2, 2, 10);
        
        JPanel panel1 = new JPanel(gridLayout1);
        
        JLabel jlServerIP = new JLabel("Server IP Address: ");
        JLabel jlServerPort = new JLabel("Server Port: ");
        JLabel jlUsername = new JLabel("Username: ");
        JLabel jlPassword = new JLabel("Password: ");
        jtfServerIP = new JTextField(host, 5);
        jtfServerIP.setToolTipText("ClueLess Server's IP address");
        jtfServerPort = new JTextField("" + serverPort, 5);
        jtfServerPort.setToolTipText("ClueLess Server's port number");
        jtfUsername = new JTextField("", 5);
        jtfUsername.setToolTipText("Enter a valid username for the ClueLess server");
        jpfPassword = new JPasswordField("", 5);
        jpfPassword.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if(e.getKeyCode() == 10) {
                    connectToServer();
                }
            }
        });
        jpfPassword.setToolTipText("Enter a valid password for the ClueLess server");
        status = new JLabel("");
        Font font = new Font("Arial", Font.PLAIN, 9);
        status.setFont(font);
        JButton jbConnect = new JButton("Connect");
        jbConnect.setName("jbConnect");
        jbConnect.addActionListener((ActionEvent ae) -> {
            connectToServer();
        });
        
        panel1.add(jlServerIP);
        panel1.add(jtfServerIP);
        
        panel1.add(jlServerPort);
        panel1.add(jtfServerPort);
        
        panel1.add(jlUsername);
        panel1.add(jtfUsername);
        
        panel1.add(jlPassword);
        panel1.add(jpfPassword);
        
        panel1.add(new JLabel(""));
        panel1.add(new JLabel(""));
        panel1.add(status);
        panel1.add(jbConnect);
        
        frame.add(panel1);
    }
    
    /**
     * 
     */
    private void connectToServer() {
        try {
            //Display to user
            status.setText("Attemtping connection to server");
            // Create a socket to connect to the server
            host = jtfServerIP.getText();
            serverPort = Integer.parseInt(jtfServerPort.getText());
            socket = new Socket(host, serverPort);
            
            // Create an output stream to send data to the server
            toServer = new DataOutputStream(socket.getOutputStream());
            
            // Create an input stream to receive data from the server
            fromServer = new DataInputStream(socket.getInputStream());
            
            System.out.println("Sending Username");
            toServer.writeUTF(jtfUsername.getText());
        
            System.out.println("Sending Password");
            String password = new String(jpfPassword.getPassword());
            toServer.writeUTF(password);
            
            int response = fromServer.read();
            
            if(response == 1) {
                status.setText("Correct credentials, connecting!");
                
                playerNum = fromServer.read();
                System.out.println(playerNum);
                detectiveLevel = fromServer.readUTF();
                System.out.println(detectiveLevel);
                notifyListeners();
                
            } else if(response == 0){
                status.setText("Incorrect username or password");
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
            } else if(response == 2) {
                status.setText("Username already in game");
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
            }
        }
        catch (IOException ex ) {//| InterruptedException ex) {
            Logger.getLogger(HomePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * @param client 
     */
    public void addListeners(ClientApplication client) {
        listeners.add(client);
    }
    
    /**
     * 
     */
    public void notifyListeners() {
        listeners.stream().forEach((client) -> {
            client.notified(jtfUsername.getText(), socket, playerNum, detectiveLevel);
        });
    }
}
