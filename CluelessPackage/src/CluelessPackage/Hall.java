package CluelessPackage;
/**
 * Created by scott.kroupa on 11/29/2016.
 */
public class Hall extends MapCell{

    boolean isOccupied;

    public Hall(String cellName, int col, int row, boolean isOccupied){
        super(cellName, col, row);
        this.isOccupied = isOccupied;
    }

    public boolean isOccupied(){
        return isOccupied;
    }
    
    //Added 08Dec16
    //*************************************************
    /**
     * 
     * @param isOccupied 
     */
    public void setOccupied(boolean isOccupied) {
        this.isOccupied = isOccupied;
    }
    //*************************************************
}
