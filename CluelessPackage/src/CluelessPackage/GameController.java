// GameController Class for ClueLess application
// James Quaranta
// 11/29/16:  All flows integrated, all conditional structures in place
// Need final ServerApplication and Map
//  12/3/16:  updated to use Map in constructor, and moved
// broadcasetMessage from ServerApplication to here to avoid //circular referece with Player
//  12/4/16:  Edited to use MapCell instead of Room, added a reference to HandleASession to call broadcast message
//  12/5/16:  Implemented hasSuggestedCard().  This was a hard one...
//  12/7/16: Cleanup

package CluelessPackage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;  //needed to check hasNext() for incrementPlayerTurn()
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author jrquaran
 */
public class GameController {

    
    private ArrayList<Player> turnOrder; //ArrayList<Players>
    private Player playerTurn;
    private CaseFile crime;
    private ArrayList<Player> activePlayers; //ArrayList<Player>
    private Map m1;
    //private ServerApp s1 //need a reference to serverApp to call broadcast message
    private HandleASession s1;
    
//constructor
    public GameController (Map map) {
        m1 = map;
    }//close constuctor

    
    // set and get methods
    
    
    public void setTurnOrder (ArrayList<Player> activePlayers){
        turnOrder = activePlayers;
        //Added 08Dec16
        //*******************************************
        playerTurn = turnOrder.get(0);
        //*******************************************
        System.out.println("turnOrder set to: " +turnOrder);
    }
    
    public ArrayList<Player> getTurnOrder(){
        return turnOrder;
    }
    
    //Note, NO SetPLayerTurn as this is a special setter, see incrementPLayerTurn().
    
    public Player getPlayerTurn(){
        return playerTurn;
    }
    
    
    public void setCrime(CaseFile p_crime){
        crime = p_crime;
        System.out.println("crime set to: " + p_crime );
    }
    
    public CaseFile getCrime(){
        return crime;
    }
    
    public void setActivePlayers(ArrayList<Player> p_activePlayers){
        activePlayers = p_activePlayers;
        System.out.println("activePlayers set to: " + p_activePlayers);
        
    }
    
    
    public ArrayList<Player> getActivePlayers(){
        return activePlayers;
    }
    
    public void setHandleASession(HandleASession hSession){
        s1 = hSession;
        System.out.println("handleASession set to: " +s1);
    }
    
    //Other useful methods
    
    
    //12/10/16 added to test if all players have lost
    public boolean allPlayersLost(){
        for (Player checkPlayer: activePlayers){
            if (checkPlayer.getPlayerStatus() != 2 ){ // 2 = lost
                return false;// at least 1 player has not lost
            }
        }//end for
        return true; //all players have lost! 
        
        
        //complementary code, need to find the right place to put it
        //if (allPlayersLost()== true){
          //  s1.broadcastMessage("All players have lost!  How depressing... please play again!");
           // return null;
        //}//close if
        
        
    }
    
    public Player incrementPlayerTurn(Player p_playerTurn){
        //not  simple SET method-- special action needed:
        //this method should take the CURRENT player who just ended
        //turn as input, then SET the playerTurn to the NEXT player
        //in the turnOrder Array list.
        // 11/29/16  flow finished
        //  12/4/16  implemented.
        
        
        int index = turnOrder.indexOf(p_playerTurn);
        System.out.println("index = " + index);
        int count = 0;
        System.out.println("count = " + count);
        
        
        if (index == (turnOrder.size() - 1)) {
            count = 0;
            System.out.println("count = " + count);
        } else {
            count = index+1;
            System.out.println("count = " + count);
        }
        
        while(true) {
            if(count == index) {
                System.out.println("count == index, reached player");
                return null;
            } else {
                if(turnOrder.get(count).getPlayerStatus() == 0) { 
                    System.out.println("Found the next player who hasnt lost");
                    playerTurn = turnOrder.get(count);
                    playerTurn.setMyTurn(true);
                    s1.broadcastMessage("It is " + playerTurn.getCharacterName()+"'s turn");
                    return turnOrder.get(count);
                }
            }
            
            if(count == (turnOrder.size()-1)) {
                count = 0;
                System.out.println("reached the end, reseting count to 0");
            } else {
                count++;
                System.out.println("incrementing count");
            }
        }
        /*
        
        //find the current player in the turnOrder
        int i = turnOrder.indexOf(p_playerTurn);
        
        //check of current player is last on the list
        //if (turnOrder.iterator().hasNext() == true){
        //Changed 08Dec16
        //******************************************************
        if (i < (turnOrder.size() - 1)) {
        //******************************************************
            //if current player is not last on the list
            //get the ArrayList item that is i + 1 (the next one)
            
            Player nextPlayer = turnOrder.get(i + 1);
            playerTurn = nextPlayer;
            
        } else{
            //if current player is last on the list, go back to the start of turnOrder list
            Player nextPlayer = turnOrder.get(0); // 0= first index
            playerTurn = nextPlayer;
        }
        
                
        System.out.println("playerTurn set to: " + playerTurn.getPlayerName());//debug
        
        
        //per End Turn use case, this method needs to setMyTurn(true) for the
        //next player object
        
        playerTurn.setMyTurn(true);//note, this is setting the NEXT player turn, not the current player
        
        //call ServerApplication Broadcast message
        //stub, no longer needed: ServerApplication s1 = new ServerApplication();// stub, move to Main

        */
        //NEED THIS QQQQ  remvoed to debug
        //s1.broadcastMessage("It is " + playerTurn.getCharacterName()+"'s turn");
        
        //return playerTurn;
    }
    
    public MapCell movePlayer (Player player, int row, int col){
        // 11/28/16: updated to integrate with Map
        // per Move flow, this method calls Map.updateLoc()
        // then ServerApplication.broadcastMessage()
        // All flow calls added, no conditionals needed as Valid Move and...
        // ...invalid move result in the same calls from here
        // 12/5/16: Implemented
        // 12/6/16:  Disabled call to HandleAServer.Broadcast message since I can't test this...
        //  ... after testing turn that back on.
        
        
        //call Map.updateLoc and catch the room
    
        //debug
        System.out.println("Debug:  GameConroller.movePlayer called" + player+ row + col);
        
        //Changed 08Dec16
        //swapped col and row
        //************************************************
        MapCell aRoom = m1.movePlayer(player, col, row);
        //************************************************
        
        //call GameController.announceMoved(player).   May be reduant
       
        
        //JQ: 12/10/16: Removed, redundant
        //this.announceMoved(player);
        
        //call ServerApplication Broadcast message
        //stub, no longer needed: ServerApplication s1 = new ServerApplication();// stub, move to Main
        
        //12/7/16 error. This is where we are having issues because s1 is null so a null pointer is returned
       //JQ:  12/10/16, made this conditional so if player moves in a room, 
        // tell player (and inform other players) that next action should be suggest/accuse
        
        if (aRoom.isRoom()){
            s1.broadcastMessage("Waiting for "+ player.getPlayerName() + " to Suggest or Accuse... ");
        }
        
        //s1.broadcastMessage("Message is Player "+ player.getPlayerName()
          //      + " "+player.getCurrentRoom());
        
        return aRoom;//this is a MapCell
    }
    
    //  redundant, not needed since we have s1.broadcastMessage
    public void announceMoved (Player player){
        //stub output for testing
        System.out.println ("Broadcast: Hey everybody! Player "+ 
                player.getPlayerName() +" moved");
    }
    
    
    /**
     * @method Return all possible move locations for the player
     * @param player
     * @return 
     * added per Ryan
     */
    public ArrayList<MapCell> getPossibleMoves(Player player){
        return player.getCurrentRoom().getPossibleMoves();
    }
    
    
    public boolean moveSuspect(Player suspect, MapCell room){
        //  11/28/16 was void, but made boolean for easier integratin testing
        // per flow needs to call Player.addAction("Suggest");
        //  the player here is the suggested player.
        // 12/5/16: Implemented
        
        //call Map.updateLoc
                
        //Debug
        //System.out.println("Debug moveSuspect:  row=: "+ room.getRow());
        
        //Get row and col from the MapCell room
        int row = room.getRow();
        int col = room.getCol();
        
        
        //Can't use MapCell here as it checks for normal valid moves.  Instead, just update playerLoc directly
        //Check with Scott if this is ok
        MapCell aRoom = m1.transportPlayer(suspect, row, col);
         //suspect.getPlayerLoc().setLoc(col,row);
        
        
        //debug output.  Remove when implemented.
        System.out.println("Debug:  Suspect " + suspect.getPlayerName() + " moved to "+
                room.getCellName());
        
        // call Player.addAction
        //Player playerS = new Player (this, "Player_suspect", "Character_suspect"); //stub
        suspect.addAction("Suggest");
        
        System.out.println(suspect.getCharacterName()+" actions are: " + 
                suspect.getAvailActions());//debug only, remove when implemented
       
        return true;
    }
    
    
    public Player announceSuggestion(Player playerTurn, Player suspect, String weapon, MapCell room){
        // 11/28/16  edited to integrate with ServerApplication for broadcast message
        // and Player to return a card
        // 12/5/16:  Implemented
        // 12/6/16:  Disabled s1.broadcast message since can't test. Add it back in after integration testing
        
        //Modified 09Dec16
        //***************************************************************************
        //broadcast message
        s1.broadcastMessage(playerTurn.getPlayerName()+ " suggested "+suspect.getCharacterName()+
               " with the " + weapon+ " in the "+ room.getCellName());
        //*****************************************************************************
        
        //call GameController.hasSuggestedCard()
        //hasSuggestedCard should return a player or null
        Player playerC = this.hasSuggestedCard(suspect, weapon, room);
        
        //need Conditional branch here- if player returns a card, then...
        // ...announceCardShown().  Otherwise announceNoMatchingCard()
        if (playerC != null){
            //get the card from a player who has a card
            //Card aCard = playerC.pickCard(suspect, weapon, room);
            //call GameController.announceCardShown().  May be redundant
            //this.annouceCardShown(playerTurn, playerC); 
            //s1.broadcastMessage(this.annouceCardShown(playerTurn, playerC));
            //return aCard;
            return playerC;
        }else{
            //call GameController.announceNoMatchingCard().  May be redudant
            this.annouceNoMatchingCard();
            //call broadcastMessage with no matching card message
            s1.broadcastMessage(this.annouceNoMatchingCard());
            return null;
        }//end if
    }
    
    public boolean announceAccusation(String suspect, String weapon, String room){
        //JQ 11/28/16 edited to integrate with ServerApplication to broadcast message
        //11/29/16  Conditional flows mapped
        // 12/5/16: Implemented
        
        //call ServerApplication broadcast message        
        s1.broadcastMessage("Hey everybody! An accusation has been made: " + 
             suspect+" with the "+weapon+ " in the "+room);
        
        //call GameController.checkAccusation        
        if (this.checkAccusation(suspect, weapon, room)){
            s1.broadcastMessage(this.announceAccusationResult(true));
            return true;
        }else{
            s1.broadcastMessage(this.announceAccusationResult(false));
            //call GameController.setPlayerTurn
            //this.incrementPlayerTurn(playerTurn);
            
            //call ServerApplication broadcast
            //s1.broadcastMessage(this.incrementPlayerTurn(playerTurn).getPlayerName());
           
            
            return false;
        }//end if   
    }
    
    public Player hasSuggestedCard(Player suspect, String weapon, MapCell room){
        //iterate through Active Players to find the Card
        // 12/5/16: Implemented
        //JQ: 12/10/16 added s1.broadcast message to let players know system is 
        // searching for a player with a card
        
        s1.broadcastMessage("Searching player hands for a matching card, please wait...");
        
               
        //need ListIterator variable to use in the While loop
        //somewhere need to call .next() else while loop will run forever.
        System.out.println(activePlayers);
        ArrayList<Player> order = turnOrder;
        
        Player p = playerTurn;
        System.out.println("playerTurn = " + p.getPlayerName());
        
        int index = order.indexOf(p);
        System.out.println("index = " + index);
        int count = 0;
        System.out.println("count = " + count);
        
        if (index == (order.size()-1)) {
            count = 0;
            System.out.println("count = " + count);
        } else {
            count = index+1;
            System.out.println("count = " + count);
        }
        
        while(true) {
            if(count == index) {
                System.out.println("count == index, reached player");
                break;
            } else {
                ArrayList<Card> pCards = order.get(count).getCards(); //get the player's card ArrayList
                System.out.println("checking "+order.get(count).getPlayerName()+"'s hand");
            
                for (Card aCard : pCards){// iterate thru current Player's cards looking for a matching one
                    System.out.println("Debug: " +aCard.getCardValue());//debug output

                    if( (aCard.getCardValue().equals(suspect.getCharacterName()) ||
                            (aCard.getCardValue().equals(room.getCellName() ))||
                                    (aCard.getCardValue().equals(weapon)))   ) {//combined search
                        System.out.println("Card found: " + aCard.getCardValue());// output for any card
                        System.out.println("Debug: Card Held by: " + order.get(count).getCharacterName());//debug output
                        System.out.println("Debug: Player return value is: " + order.get(count));//debug output
                        return order.get(count);
                    }
                }
            }
            
            if(count == (order.size()-1)) {
                count = 0;
                System.out.println("reached the end, reseting count to 0");
            } else {
                count++;
                System.out.println("incrementing count");
            }
        }
        /*
        ListIterator<Player> lit = activePlayers.listIterator();
        System.out.println(lit);
        while (lit.hasNext()){
            Player cPlayer = lit.next();// get the next player from the activePlayers ArrayList
            ArrayList<Card> pCards = cPlayer.getCards(); //get the player's card ArrayList
            
            for (Card aCard : pCards){// iterate thru current Player's cards looking for a matching one
                System.out.println("Debug: " +aCard.getCardValue());//debug output
              
                if( (aCard.getCardValue().equals(suspect.getCharacterName()) ||
                        (aCard.getCardValue().equals(room.getCellName() ))||
                                (aCard.getCardValue().equals(weapon)))   ) {//combined search
                    System.out.println("Card found: " + aCard.getCardValue());// output for any card
                    System.out.println("Debug: Card Held by: " + cPlayer.getCharacterName());//debug output
                    System.out.println("Debug: Player return value is: " + cPlayer);//debug output
                    return cPlayer;
                }
            }
        }*/
        System.out.println("Card not found.");
        return null; //if no player is found with a matching card
    }
    
    public String annouceCardShown(Player p_playerT, Player p_playerC){
        //12/5/16: Implemented
        Player playerT= p_playerT;
        Player playerC= p_playerC;
        return ("Hey eveybody! Player " + playerC.getPlayerName()+ " showed "+
                playerT.getPlayerName()+" a card!");
    }
    
    
    public String annouceNoMatchingCard(){
        // 12/5/16  Implemented
        return "Nobody had a matching card for the suggestion";
    }
    
    public boolean checkAccusation(String suspect, String weapon, String room){
        // compare any of the arguements with cards in the CaseFile ArrayList
        //use hasSuggestedCard as a model
        //12/5/16: verify with Christine if she wants to use...
        //...Casefile's public boolean solutionMatchesAccusation(String acWeapon, String acPlayer, String acRoom)
        // if so, convert Accusation arguments to Strings 
        
        //call Casefile.solutionMatchesAccusation(String acWeapon, String acPlayer, String acRoom)
        //Convert arguments to Strings to fit the method in CaseFile (crime is a CaseFile)
        return (crime.solutionMatchesAccusation(weapon, suspect, room));
    }
    
    public String announceAccusationResult(boolean result){
        // 12/5/16 Implemented
        return ("Hey everybody! The accusation is "+ result);
    }
    
    // 12/5/16, I believe the serverAppilcation deals cards during initialization. REM this one out
    //public boolean dealCardsToPlayer(Deck deck, ArrayList activePlayers){
      //  System.out.println ("Deck " + deck.toString()+ "was dealt to activePLayer list " 
        //        + activePlayers);
                
        //return true;
    //}
    
    //moved back to  ServerApplication, HandleASession
    //public void broadcastMessage(String message) {
        //stub output
       // System.out.println(message);
        
        //DataOutputStream toClient;
        
        //for (Player player : players) {
          //  if (player == null || !player.getPlayerInGame()) {
            //    continue;
            //} else {
              //  try {
                //    toClient = new DataOutputStream(player.getSocket().getOutputStream());
                  //  toClient.writeUTF(message);
               // }catch (IOException ex) {
                 //   Logger.getLogger(HandleASession.class.getName()).log(Level.SEVERE, null, ex);
            //    }
            //}
        //}
    //}
    
    
    
}//close GameController Class


