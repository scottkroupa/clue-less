package CluelessPackage;
/**
 * Created by scott.kroupa on 11/29/2016.
 */
public class PlayerLoc {
    private int row;
    private int col;

    public PlayerLoc(){
    }

    public PlayerLoc(int col, int row){
        this.col = col;
        this.row = row;
    }


    public int getCol() {
        return col;
    }

    public void setLoc(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public int getRow() {
        return row;
    }
}
