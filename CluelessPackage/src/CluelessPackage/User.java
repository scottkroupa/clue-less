package CluelessPackage;

/*
 *	The User class contains information about each player of the Clue-Less game.
 *	Each "user" represents one row in the lookup table used by the server
 *	@author: Christine Herlihy
 *	last updated: 11/12/16
 *
 *	NOTE: we may need to add more methods to this class, and/or constructors with a subset of input parameters
*/

public class User {
	
	private String username = "tempUserName";
	private String password = "tempPassword";
	private int sessionID = -99;
	private String charName = "tempCharName";
	private int detLevel = -99;  
        // I added this field; it wasn't in our original class diagram. I think we will need it for the lookup table
	
	/**
	 * @method: empty constructor to instantiate a new User object
	 */
	public User() {
		super();
		this.username = username;
		this.password = password;
		this.sessionID = sessionID;
		this.charName = charName;
		this.detLevel = detLevel;
	}
	
	
	/**
	 * @method: constructor; requires ALL variable fields
	 * @param username
	 * @param password
	 * @param sessionID
	 * @param charName
         * @param detLevel
	 */
	public User(String username, String password, int sessionID, String charName, int detLevel) {
		super();
		this.username = username;
		this.password = password;
		this.sessionID = sessionID;
		this.charName = charName;
		this.detLevel = detLevel;
	}


	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	
	
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the sessionID
	 */
	public int getSessionID() {
		return sessionID;
	}


	/**
	 * @param sessionID the sessionID to set
	 */
	public void setSessionID(int sessionID) {
		this.sessionID = sessionID;
	}


	/**
	 * @return the charName
	 */
	public String getCharName() {
		return charName;
	}


	/**
	 * @param charName the charName to set
	 */
	public void setCharName(String charName) {
		this.charName = charName;
	}


	/**
	 * @return the tableKey
	 */
	public int getDetLevel() {
		return detLevel;
	}


	/**
         * @param detLevel
	 */
	public void setDetLevel(int detLevel) {
		this.detLevel = detLevel;
	}
	
	
	
	
	
	
	
	


}
