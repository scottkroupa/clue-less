package CluelessPackage;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by scott on 12/6/2016.
 */
public class GUINotebook extends JPanel{

    private JTable table;
    private JTable people;
    private JTable weapons;
    private JTable rooms;
    private Notebook notebook;
    private List<NotebookItem> peopleList;
    private List<NotebookItem> weaponList;
    private List<NotebookItem> roomList;


    public GUINotebook(Notebook n, JPanel notebookPanel, ArrayList<GUIHand>  guiHandList){

        Object [] columnNames = {"Item", "Status", "Comment"};
        peopleList = new ArrayList<>();
        notebook = n;
        List<NotebookItem> items = notebook.getItems();
        for(int i = 0; i < 6; i++) {
                peopleList.add(items.get(i));
        }

        Object [][] peopledata = new Object [6][3];
        for(int i =0;i <peopleList.size();i++) {
        	peopledata[i][0] = peopleList.get(i).getItem();
			peopledata[i][1] = false;
			peopledata[i][2] = "";
        }
        
        for(int i =0;i <peopleList.size();i++) {
          	for(int j = 0; j<guiHandList.size(); j++)
        	{
        		if(peopleList.get(i).getItem().equals(guiHandList.get(j).getCard().getCardValue())){
        			System.out.println("I have this card: " + guiHandList.get(j).getCard().getCardValue());
             		peopledata[i][1] = true;
            		peopledata[i][2] = "I have this card";
        		}
        	}
        }

        NoteBookTableModel model = new NoteBookTableModel(peopledata, columnNames);
        for(int l = 0; 0 < l; l++){
            model.isCellEditable(0,l);

        }
        //DefaultTableModel model = new DefaultTableModel(peopledata, columnNames);
        people = new JTable(model);
        people.getColumnModel().getColumn(0).setPreferredWidth(150);
        people.getColumnModel().getColumn(1).setPreferredWidth(30);
        people.getColumnModel().getColumn(2).setPreferredWidth(250);

        //people = new JTable(data, columnNames);
        //TitledBorder tb = BorderFactory.createTitledBorder("Characters");
        //people.setBorder(tb);
//        notebookPanel.add(people, BorderLayout.NORTH);



        JPanel chars = new JPanel();
        chars.add(people);
        chars.setBorder(BorderFactory.createTitledBorder("Characters"));

        //weapons
        weaponList = new ArrayList<>();
        for(int j= 6; j<12;j++){
            weaponList.add(items.get(j));
        }
        
        Object [][] weapondata = new Object [6][3];
        for(int i =0;i <weaponList.size();i++) {
        	weapondata[i][0] = weaponList.get(i).getItem();
			weapondata[i][1] = false;
			weapondata[i][2] = "";
        }
        	
        	
        for(int i =0;i <weaponList.size();i++) {	
          	for(int j = 0; j<guiHandList.size(); j++)
        	{
        		if(weaponList.get(i).getItem().equals(guiHandList.get(j).getCard().getCardValue())){
        			System.out.println("I have this card: " + guiHandList.get(j).getCard().getCardValue());
             		weapondata[i][1] = true;
            		weapondata[i][2] = "I have this card";
        		}
        	}
        }

        model = new NoteBookTableModel(weapondata, columnNames);
        weapons = new JTable(model);
        weapons.getColumnModel().getColumn(0).setPreferredWidth(150);
        weapons.getColumnModel().getColumn(1).setPreferredWidth(30);
        weapons.getColumnModel().getColumn(2).setPreferredWidth(250);
        //tb = BorderFactory.createTitledBorder("Weapons");
        //weapons.setBorder(tb);
//        notebookPanel.add(weapons, BorderLayout.CENTER);

        JPanel weaps = new JPanel();
        weaps.add(weapons);
        weaps.setBorder(BorderFactory.createTitledBorder("Weapons"));




        //Rooms
        roomList = new ArrayList<>();
        for(int j= 12; j<21;j++){
            roomList.add(items.get(j));
        }
        
        Object [][] roomdata = new Object [9][3];
        
        for(int i =0;i <roomList.size();i++) {
        	roomdata[i][0] = roomList.get(i).getItem();
			roomdata[i][1] = false;
			roomdata[i][2] = "";
        }
        
       
        for(int i =0;i <roomList.size();i++) {
          	for(int j = 0; j<guiHandList.size(); j++)
        	{
        		if(roomList.get(i).getItem().equals(guiHandList.get(j).getCard().getCardValue())){
        			System.out.println("I have this card: " + guiHandList.get(j).getCard().getCardValue());
             		roomdata[i][1] = true;
            		roomdata[i][2] = "I have this card";
        		}
        	}
        }
        
        model = new NoteBookTableModel(roomdata, columnNames);
        rooms = new JTable(model);
        rooms.getColumnModel().getColumn(0).setPreferredWidth(150);
        rooms.getColumnModel().getColumn(1).setPreferredWidth(30);
        rooms.getColumnModel().getColumn(2).setPreferredWidth(250);
        //tb = BorderFactory.createTitledBorder("Rooms");
        //rooms.setBorder(tb);
//        notebookPanel.add(rooms, BorderLayout.SOUTH);


        JPanel roo = new JPanel();
        roo.add(rooms);
        roo.setBorder(BorderFactory.createTitledBorder("Rooms"));




        JPanel tables = new JPanel();
        tables.setLayout(new BoxLayout(tables, BoxLayout.PAGE_AXIS));
        tables.add(Box.createRigidArea((new Dimension(0,10))));
        tables.add(chars, BorderLayout.NORTH);
        tables.add(Box.createRigidArea((new Dimension(0,10))));
        tables.add(weaps, BorderLayout.CENTER);
        tables.add(Box.createRigidArea((new Dimension(0,10))));
        tables.add(roo, BorderLayout.SOUTH);
        JScrollPane tableContainer = new JScrollPane(tables);


        notebookPanel.add(tableContainer, BorderLayout.CENTER);

        /*this.notebook=n;
        peopleList = new ArrayList<>();
        weaponList = new ArrayList<>();
        roomList = new ArrayList<>();
        List<NotebookItem> items = notebook.getItems();
        for(int i = 0; i<items.size(); i++){
            if(i < 6)
                peopleList.add(items.get(i));
            else if(i < 12)
                weaponList.add(items.get(i));
            else
                roomList.add(items.get(i));
        }
        Object [][] object = new Object [3][6];
        for(int i =0;i <peopleList.size();i++){
            object[0][i]= new JLabel(peopleList.get(i).getItem());
            object[1][i]= new JCheckBox();//peopleList.get(i).getStatus()
            object[2][i]= "";
        }
        people = new JTable(object, columnNames);
        TitledBorder tb = BorderFactory.createTitledBorder("Characters");
        people.setBorder(tb);
        weapons = new JTable();
        tb = BorderFactory.createTitledBorder("Weapons");
        weapons.setBorder(tb);
        rooms = new JTable();
        tb = BorderFactory.createTitledBorder("Rooms");
        rooms.setBorder(tb);
        notebookPanel.add(people, BorderLayout.NORTH);
        notebookPanel.add(weapons, BorderLayout.CENTER);
        notebookPanel.add(rooms, BorderLayout.SOUTH);//this may be due to the chat functionality
*/
       /* this.add(people, BorderLayout.NORTH);
        this.add(weapons, BorderLayout.CENTER);
        this.add(rooms, BorderLayout.SOUTH);*/



        //buildChart();

    }



    public void buildChart(){
        //JFrame frame = new JFrame("Testing JFrame");
        //JPanel panel = new JPanel();
        //panel.setLayout(new BorderLayout());
        //JTable table = new JTable();
        //JScrollPane tableContainer = new JScrollPane(table);
        //panel.add(tableContainer, BorderLayout.CENTER);
        //frame.getContentPane().add(panel);
        //final String [] columnName = {"Item", "Status", "Comments"};


    }
    





}
